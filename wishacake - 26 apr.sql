-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2019 at 09:16 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wishacake`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_token` varchar(32) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `password_token`, `created_at`, `modified_at`) VALUES
(1, 'wishacake19@gmail.com', 'admin', NULL, '2019-04-14 11:08:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bakers`
--

CREATE TABLE `bakers` (
  `id` bigint(20) NOT NULL,
  `firebase_tokens` text,
  `image` varchar(512) DEFAULT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(13) NOT NULL COMMENT '+92xxxxxxxxxx',
  `cnic` varchar(15) NOT NULL,
  `pin` varchar(4) NOT NULL,
  `pin_token` varchar(32) DEFAULT NULL,
  `cnic_front` varchar(512) DEFAULT NULL,
  `cnic_back` varchar(512) DEFAULT NULL,
  `location_id` varchar(512) DEFAULT NULL,
  `location_name` varchar(512) DEFAULT NULL,
  `location_address` varchar(1024) NOT NULL,
  `location_latitude` varchar(255) NOT NULL DEFAULT '0',
  `location_longitude` varchar(255) NOT NULL DEFAULT '0',
  `slider_image1` varchar(512) DEFAULT NULL,
  `slider_image2` varchar(512) DEFAULT NULL,
  `slider_image3` varchar(512) DEFAULT NULL,
  `rating` varchar(4) DEFAULT '0',
  `account_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Active, 2 = Rejected & 3 = Blocked',
  `active_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Online & 0 = Offline',
  `notification_seen_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Seen/True & 0 = Not seen/False',
  `rejected_reason` varchar(4000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bakers`
--

INSERT INTO `bakers` (`id`, `firebase_tokens`, `image`, `first_name`, `last_name`, `email`, `mobile_number`, `cnic`, `pin`, `pin_token`, `cnic_front`, `cnic_back`, `location_id`, `location_name`, `location_address`, `location_latitude`, `location_longitude`, `slider_image1`, `slider_image2`, `slider_image3`, `rating`, `account_status`, `active_status`, `notification_seen_status`, `rejected_reason`, `created_at`, `modified_at`) VALUES
(1, NULL, NULL, 'Ahmed', 'Iqbal', 'ahmed.iqbal@outlook.com', '+923351596357', '42505-1234653-8', '2563', NULL, NULL, NULL, 'ChIJj8VnmM04sz4RHuslODx8UME', 'Nipa Chowrangi Bus Stop', 'Block 10 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', '24.917755', '67.0971768', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-19 18:33:34', '2019-04-25 19:45:34'),
(2, NULL, NULL, 'Tehreem', 'Ali', 'tehreemali21@yahoo.com', '+923361234567', '42101-1234567-8', '2200', NULL, NULL, NULL, 'ChIJ4z4Nk_4-sz4RW_xTrTpvY-k', 'PIB Colony', 'PIB Colony, Karachi, Karachi City, Sindh, Pakistan', '24.8941992', '67.0539059', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-19 18:36:54', '2019-04-25 19:45:34'),
(3, NULL, NULL, 'Emad ud din', 'Jamil', 'emad@yahoo.com', '+923361000693', '42801-1534568-9', '1995', NULL, NULL, NULL, 'ChIJCQO1tbk_sz4RhC3ujcu_hww', 'Chawla Market', 'Block 1 Nazimabad, Karachi, Karachi City, Sindh 74600, Pakistan', '24.906622199999994', '67.0257945', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-19 19:23:17', '2019-04-25 19:45:34'),
(4, NULL, NULL, 'Muhammad Ali', 'Khan', 'muhammad.ali@yahoo.com', '+923431236674', '42801-1534533-3', '1996', NULL, NULL, NULL, 'ChIJsTQ9njA_sz4RgUU1ZV3Mifw', 'Disco Bakers', 'A-495 Allama Shabbir Ahmed Usmani Rd, Block 3 Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', '24.929146400000004', '67.0973183', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-19 19:30:08', '2019-04-25 19:45:34'),
(5, NULL, NULL, 'Ahsan', 'Malik', 'ahsan456@live.com', '+923453366802', '42101-1534544-4', '8526', NULL, NULL, NULL, 'ChIJw_h0NpU-sz4RluG3tg8FK48', 'Naheed Supermarket', '56-157, Block 3, BYJCHSØŒ Shaheed-e-Millat Road, Bahadurabad Karachi, Karachi City, Sindh, Pakistan', '24.877877599999994', '67.06846170000001', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-01-19 19:33:40', '0000-00-00 00:00:00'),
(6, NULL, NULL, 'Tauqeer Ali', 'Sheikh', 'tauqeer.sheikh11@gmail.com', '+923004436852', '42101-2066973-5', '1103', NULL, NULL, NULL, 'EiNLaGF5YWJhbi1lLVJhaGF0LCBLYXJhY2hpLCBQYWtpc3RhbiIuKiwKFAoSCX1BQkn1PLM-EWonAx6gedKAEhQKEgkbtOnBYDyzPhHbqnYivHaIog', 'Khayaban-e-Rahat', 'Khayaban-e-Rahat, Phase 7 Defence Housing Authority, Karachi, Karachi City, Sindh 75500, Pakistan', '24.8107293', '67.0702816', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-01-19 19:37:04', '0000-00-00 00:00:00'),
(7, NULL, NULL, 'Ali', 'Ahsan', 'alicakes6544@gmail.com', '+923323467966', '45101-3625839-6', '5555', NULL, NULL, NULL, 'ChIJIa0l4JRAsz4RvLu0F_KPQVs', 'Nagan Chorangi', 'Sector-11-E Sector 11 E North Karachi Twp, Karachi, Karachi City, Sindh, Pakistan', '24.9662198', '67.0673176', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-20 18:09:44', '2019-04-25 19:45:34'),
(8, NULL, NULL, 'Shoaib', 'Cakes', 'shoaib.cakes011@gmail.com', '+923001269866', '42101-3066852-6', '1395', NULL, NULL, NULL, 'ChIJEdyWYMI4sz4R3VhMDoA2kVg', 'Continental Sweets & Bakers', 'A-1 New Rd Service Ln, Block 15 Gulistan-e-Johar, Karachi, Karachi City, Sindh, Pakistan', '24.9227498', '67.1166444', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-20 18:12:27', '2019-04-25 19:45:34'),
(9, NULL, NULL, 'Yasir', 'Abbas', 'syasirabbas94@gmail.com', '+923343417939', '42101-2099336-6', '6930', NULL, NULL, NULL, 'ChIJ11QyqXY-sz4RpaZKciZ0kDo', 'Madina City Mall', 'Abdullah Haroon Rd, MBL Panorama Saddar, Karachi, Karachi City, Sindh, Pakistan', '24.854570299999995', '67.02870469999999', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-01-21 14:38:14', '0000-00-00 00:00:00'),
(10, NULL, NULL, 'Afridi', 'Khan', 'a89.khan@gmail.com', '+923319685339', '48101-3895959-9', '8639', NULL, NULL, NULL, 'ChIJxbIJ2GgVsz4RZAtnHpRqMqQ', 'Labour Square', 'Labor Square Sindh Industrial Trading Estate, Karachi, Karachi City, Sindh, Pakistan', '24.9042664', '66.9842808', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-21 22:06:01', '2019-04-25 19:45:34'),
(11, NULL, NULL, 'Faiza', 'Ikram', 'faiza.cakes@gmail.com', '+923001234567', '42101-9668637-7', '3966', NULL, NULL, NULL, 'ChIJ42IBcY5Asz4RTO_pxke_TCg', 'Shadman Town', 'Shadman Town North Karachi Twp, Karachi, Karachi City, Sindh, Pakistan', '24.962396800000004', '67.0540552', NULL, NULL, NULL, '0', 2, 0, 0, 'CNIC photos are not valid.', '2019-01-21 22:42:52', '0000-00-00 00:00:00'),
(12, NULL, NULL, 'Mehwish', 'Abrar', 'mehwish778@gmail.com', '+923379636996', '45104-9666737-3', '9633', NULL, NULL, NULL, 'ChIJhdtu48U9sz4RtKytdiZQ7TQ', 'Teen Talwar Clifton', 'Kheyaban-e-Iqbal Rd, Zone A Block 7 Clifton, Karachi, Karachi City, Sindh 75600, Pakistan', '24.833845800000002', '67.0336786', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-21 22:48:42', '2019-04-25 19:45:34'),
(14, NULL, NULL, 'Talha', 'Alam', 'talha.alam@gmail.com', '+923361237676', '42101-9399467-3', '8842', NULL, NULL, NULL, 'ChIJa-rqitE-sz4RqBpFDibbC_U', 'Bahria University Karachi Campus', 'National Stadium Rd, Karsaz Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', '24.8933157', '67.0881958', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-01-23 06:49:52', '2019-04-25 19:45:34'),
(15, 'eFqLDxRnWoM:APA91bGm1j2bGWri9PAkhHvr7QWUDu4RBmbL_7fcUoMYMM70Ofbdfq77Xp4CgWpJNih5OGbaOSHCH0tSPr-Vggyl9-0-K9LftvwdUYH-aJKMDiXLg2hXF9yjLYXdjx622_PExVtZ-jno,', NULL, 'Salim', 'Khan', 'salim_cakes17@gmail.com', '+923069385122', '42101-6399853-6', '8888', NULL, NULL, NULL, 'ChIJB6cMKlE_sz4RunG6PfsRyfk', 'Urban Arts Studio', 'KB-44, Gulshan-e-Amin Plaza, Federal B Area Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', '24.9423883', '67.08520700000001', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-02-01 20:15:05', '2019-02-25 22:14:11'),
(16, 'fvnBF_0smC4:APA91bH04T9I6YuJQPxp2ncEgyLPgXRj4nEmEt-mC7bBRPjQx5KDPzIWNfqpadv10kZWeYTZk7QESxa0sd0feEo0XQ0U4hcjpwNnWuErqHw6M2hUzjNxRK1ALyKEFXXw1P1z0q38Y34h,', NULL, 'Cakes and', 'Bakes', 'cakesandbakes@gmail.com', '+923323467942', '42101-5569856-6', '5000', NULL, NULL, NULL, 'ChIJ208GleviTDkRcpQCZP9ZXGI', 'Thatta', 'Thatta, Sindh, Pakistan', '24.7475029', '67.91063179999999', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-02-02 16:07:01', '2019-04-12 17:01:01'),
(20, 'fptAtM6U0FY:APA91bEIyeLooAcBB9lF1JAuWRv5m2hGF5fu64BvMreGRkAzobsnSLlfN8hWyPh593qmccOa1FEHkgPcjXAWh3-xwRwNQKldHXyTqO7H1NizUgjpeNORmFjNo8HdN24EKT5scTl3_qj8,fkO7nd9J8mQ:APA91bEUEAZizNQLhMcz3jFl2OGHsBZSsg1WaMHkRtCtq5LdSYooy3NOou1GRsfGcSqRMcWMh7fffCuMGrN_J-LWIKVduXHPIXB1ENaJN32LcyiKotFFnp5YsR79MoSPopzwkIJg_F5O,fK_A0hAI98o:APA91bEPg_gYnGbnI3yc7fpQ8TJtrPlgGUWUhbSeCzPbp2gW3qOGZ21i6oOfythi2AI_F0qShJoVtOiAv0wQwhpafZIaf15YGpZbx8D24Ob0QcxshNVKFbwe8VNejVqXm4CJM-P_YgAu,eAAnAroiWZI:APA91bFFb2VRSaOhEZZYeMszvWrOXst7ch3JuzMrSuW2RmrmJLXdKl5Jv7A0tMZF7AkvNY5yTCaJ90MXAF4OtkqtAkLnJtq5PFI1BO8KPijmqPCW7RaDn9bgvnHoDTahW4-FHDc9wTHI,fvnBF_0smC4:APA91bH04T9I6YuJQPxp2ncEgyLPgXRj4nEmEt-mC7bBRPjQx5KDPzIWNfqpadv10kZWeYTZk7QESxa0sd0feEo0XQ0U4hcjpwNnWuErqHw6M2hUzjNxRK1ALyKEFXXw1P1z0q38Y34h,eeZuSgSVHXg:APA91bFBdBhgC1t8lKfKH_0ej-dzwgh4soVSxxHduqA91AWzYm7evxkfS1OJ4gbIQlWHkURs-dSzDeiVGQDtjTOoEnepUi4ZtSzQXRaADnIKQatEcyjoEn9LZaT6zRn3o0UnKT_iZ0au,', 'IMG_Profile_Photo_20190308_102739.jpg', 'House of', 'cakes', 'hoc@gmail.com', '+923323467940', '42515-1161515-1', '0000', NULL, 'IMG_CNIC_Front_20190209_140207.jpg', 'IMG_CNIC_Back_20190209_140207.jpg', 'ChIJxR1RLlE_sz4Ru75Vg1-89Jo', 'Gulshan E Amin Banglows', 'FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', '24.9420001', '67.0864806', 'IMG_Slider_01_20190418_125412.jpg', NULL, NULL, '0', 1, 1, 0, NULL, '2019-02-09 13:02:07', '2019-04-19 22:49:43'),
(21, 'fvydiTlUl-c:APA91bGKjkqjZGB0IZrTouTulNZgT3FPaQjjP_rrcWQ_r37KRgklXIQCj2qGv3cAmOUWD-cNAXWWPKxYbEo-3Eeiv9zDr8xsk-Tyzjns-ExmI0t3tzf-_owzMwtXrxAoMfMXuTnGpGj8,', NULL, 'Maria', 'Cakes', 'maria_cakes@gmail.com', '+923323467551', '51515-6165165-1', '3333', NULL, 'IMG_CNIC_Front_1549718220.jpg', 'IMG_CNIC_Back_1549718220.jpg', 'ChIJoSX5L4k_sz4RRWwSDh6iqD0', 'Allahwala Apartment', 'Plot CA/1 &, CA/2, FB Indus-Area Block 21 Block 21, FBA Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', '24.942899399999995', '67.0850908', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-02-09 13:17:00', '2019-02-17 18:10:17'),
(22, 'cJIjx3Vi6NM:APA91bHGNk61LUukwnq4vkZOPRmboIqhT-MZLTS_clGC5sG1IMnJfR6Nsl85fIWzJwbTuZcO6-Cs9Xw8iQ3LBH4y0oeTGCAYrlBiprMFcdfF1mgOFpUOAHIi6loRm5Lt3kr5O1DZz9-j,', 'IMG_Profile_Photo_20190209_152555.jpg', 'Hadiqa', 'Cakes', 'hadiqa.cakes@gmail.com', '+923323467999', '56545-5545655-2', '2222', NULL, 'IMG_CNIC_Front_20190209_152555.jpg', 'IMG_CNIC_Back_20190209_152555.jpg', 'ChIJB-dL9rw-sz4RsHUw4khZPlc', 'Hill Park Apartments', 'Karachi Memon Co-operative Housing Society Karachi Memon Society PECHS, Karachi, Karachi City, Sindh, Pakistan', '24.870777099999998', '67.07421990000002', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-02-09 14:25:55', '2019-02-13 19:37:52'),
(23, NULL, 'IMG_Profile_Photo_20190217_180454.jpg', 'United', 'Bakers', 'united-bakers@gmail.com', '+923323467949', '42101-6867994-6', '5366', NULL, 'IMG_CNIC_Front_20190217_180454.jpg', 'IMG_CNIC_Back_20190217_180454.jpg', 'ChIJMQrEH-dqsz4RbVar5mEdu88', 'Orangi Town', 'Orangi Town, Karachi, Karachi City, Sindh, Pakistan', '24.951728499999998', '67.00227149999999', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-02-17 17:04:54', '2019-04-12 15:34:16'),
(24, 'fvnBF_0smC4:APA91bH04T9I6YuJQPxp2ncEgyLPgXRj4nEmEt-mC7bBRPjQx5KDPzIWNfqpadv10kZWeYTZk7QESxa0sd0feEo0XQ0U4hcjpwNnWuErqHw6M2hUzjNxRK1ALyKEFXXw1P1z0q38Y34h,eeZuSgSVHXg:APA91bFBdBhgC1t8lKfKH_0ej-dzwgh4soVSxxHduqA91AWzYm7evxkfS1OJ4gbIQlWHkURs-dSzDeiVGQDtjTOoEnepUi4ZtSzQXRaADnIKQatEcyjoEn9LZaT6zRn3o0UnKT_iZ0au,', 'IMG_Profile_Photo_20190319_062440.jpg', 'Syed Mohsin', 'Raza', 'mohsinsyed1997@gmail.com', '+923323467900', '42101-6867979-7', '6622', '49e608bbf3bfdce5a60896e26b5d9485', 'IMG_CNIC_Front_20190218_004903.jpg', 'IMG_CNIC_Back_20190218_004903.jpg', 'ChIJaax0IpI-sz4RkdrFKzrsa18', 'Tariq Road Bus Stop', 'Tariq Rd, Delhi Society Block 2 PECHS, Karachi, Karachi City, Sindh, Pakistan', '24.871790499999996', '67.0599112', 'IMG_Slider_01_20190321_111332.jpg', 'IMG_Slider_02_20190321_112942.jpg', 'IMG_Slider_03_20190321_113016.jpg', '3.5', 1, 0, 0, NULL, '2019-02-17 23:49:03', '2019-04-25 19:27:45'),
(25, 'fvnBF_0smC4:APA91bH04T9I6YuJQPxp2ncEgyLPgXRj4nEmEt-mC7bBRPjQx5KDPzIWNfqpadv10kZWeYTZk7QESxa0sd0feEo0XQ0U4hcjpwNnWuErqHw6M2hUzjNxRK1ALyKEFXXw1P1z0q38Y34h,', 'aissa_blue_2_main_sq_gy_1000x1000.jpg', 'The cake', 'girl', 'cake_girl@gmail.com', '+923321234000', '42101-2321321-3', '0000', NULL, 'aissa_cognac_28_main_sq_gy_1000x1000.jpg', 'aissa_blue_2_main_sq_gy_1000x1000.jpg', 'ChIJKQ8LsrA4sz4RkdAdaQAppRs', 'Maskan Chowrangi Bus Stop', 'Allama Shabbir Ahmed Usmani Rd Block 7 Block 7', '24.9348475', '67.10524800000007', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-04-12 15:05:21', '2019-04-12 23:27:11'),
(26, NULL, 'Master Cakes.jpeg', 'Master', 'Cakes', 'master.cakes2018@gmail.com', '+923143467941', '42611-8965065-5', '7777', NULL, 'aissa_cognac_28_main_sq_gy_1000x1000.jpg', 'aissa_blue_2_main_sq_gy_1000x1000.jpg', 'ChIJc0xRp1pBsz4RFRXthYb8N4w', 'Five Star Chorangi', 'Block I Block I North Nazimabad Town', '24.9427827', '67.04757029999996', NULL, NULL, NULL, '0', 1, 0, 0, NULL, '2019-04-12 21:53:25', '0000-00-00 00:00:00'),
(27, 'eeZuSgSVHXg:APA91bFBdBhgC1t8lKfKH_0ej-dzwgh4soVSxxHduqA91AWzYm7evxkfS1OJ4gbIQlWHkURs-dSzDeiVGQDtjTOoEnepUi4ZtSzQXRaADnIKQatEcyjoEn9LZaT6zRn3o0UnKT_iZ0au,', NULL, 'Aliya', 'Iqbal', 'aliyaiqbal11@outlook.com', '+923351216545', '42501-8222222-2', '1012', NULL, 'IMG_CNIC_Front_20190422_041656.jpg', 'IMG_CNIC_Back_20190422_041656.jpg', 'ChIJa-rqitE-sz4RqBpFDibbC_U', 'Bahria University Karachi Campus', 'National Stadium Rd, Karsaz Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', '24.8933157', '67.0881958', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-04-22 14:16:56', '2019-04-25 19:45:34'),
(28, 'eeZuSgSVHXg:APA91bFBdBhgC1t8lKfKH_0ej-dzwgh4soVSxxHduqA91AWzYm7evxkfS1OJ4gbIQlWHkURs-dSzDeiVGQDtjTOoEnepUi4ZtSzQXRaADnIKQatEcyjoEn9LZaT6zRn3o0UnKT_iZ0au,', NULL, 'Furqan', 'Ali', 'furqan.ali@gmail.com', '+923360851505', '42349-7667667-6', '5555', NULL, 'IMG_CNIC_Front_20190424_024924.jpg', 'IMG_CNIC_Back_20190424_024924.jpg', 'ChIJuYs1IgVKsz4RuPLvLuPYz-k', 'Bahria Town Karachi', 'Bahria Town Karachi, Karachi, Karachi City, Sindh, Pakistan', '25.021451499999998', '67.3034311', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-04-24 12:49:24', '2019-04-25 19:45:34'),
(29, 'eeZuSgSVHXg:APA91bFBdBhgC1t8lKfKH_0ej-dzwgh4soVSxxHduqA91AWzYm7evxkfS1OJ4gbIQlWHkURs-dSzDeiVGQDtjTOoEnepUi4ZtSzQXRaADnIKQatEcyjoEn9LZaT6zRn3o0UnKT_iZ0au,', NULL, 'Test', 'Baker', 'test@gmail.com', '+923323467941', '42101-3946766-7', '5555', NULL, 'IMG_CNIC_Front_20190425_093736.jpg', 'IMG_CNIC_Back_20190425_093736.jpg', 'ChIJw_NE1PZwTDkRheJBPRi8C8A', 'Hyderabad', 'Hyderabad, Sindh, Pakistan', '25.395968699999997', '68.357776', NULL, NULL, NULL, '0', 0, 0, 1, NULL, '2019-04-25 19:37:37', '2019-04-25 19:45:34');

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `baker_id` bigint(20) NOT NULL,
  `description` varchar(4000) NOT NULL,
  `image1` varchar(512) DEFAULT NULL,
  `image2` varchar(512) DEFAULT NULL,
  `image3` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `user_id`, `baker_id`, `description`, `image1`, `image2`, `image3`, `created_at`) VALUES
(1, 4, 24, 'Bad experience', '', 'IMG_COMPLAINT_02_20190424_114404.jpg', 'IMG_COMPLAINT_03_20190424_114404.jpg', '2019-04-24 09:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_bakers`
--

CREATE TABLE `favorite_bakers` (
  `id` bigint(20) NOT NULL,
  `user_ids` text,
  `baker_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite_bakers`
--

INSERT INTO `favorite_bakers` (`id`, `user_ids`, `baker_id`, `created_at`, `modified_at`) VALUES
(1, NULL, 1, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(2, NULL, 2, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(3, NULL, 3, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(4, NULL, 4, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(5, '1,', 5, '2019-02-16 20:52:48', '2019-04-12 22:56:06'),
(6, NULL, 6, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(7, NULL, 7, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(8, NULL, 8, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(9, NULL, 9, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(10, NULL, 10, '2019-02-16 20:52:48', '0000-00-00 00:00:00'),
(11, NULL, 11, '2019-02-16 20:52:49', '0000-00-00 00:00:00'),
(12, NULL, 12, '2019-02-16 20:52:49', '0000-00-00 00:00:00'),
(13, NULL, 14, '2019-02-16 20:52:49', '0000-00-00 00:00:00'),
(14, '1,', 15, '2019-02-16 20:52:49', '2019-02-17 00:06:13'),
(15, NULL, 16, '2019-02-16 20:52:49', '2019-02-16 21:11:59'),
(16, '1,4,', 20, '2019-02-16 20:52:49', '2019-03-23 10:46:18'),
(17, '', 21, '2019-02-16 20:52:49', '2019-02-25 10:17:37'),
(18, NULL, 22, '2019-02-16 20:52:49', '2019-02-16 21:13:56'),
(19, '4,', 23, '2019-02-17 17:04:54', '0000-00-00 00:00:00'),
(20, '', 24, '2019-02-17 23:49:03', '2019-03-23 08:32:23'),
(21, NULL, 25, '2019-04-12 21:28:01', '0000-00-00 00:00:00'),
(22, NULL, 26, '2019-04-12 21:53:25', '0000-00-00 00:00:00'),
(23, NULL, 27, '2019-04-22 14:16:56', '0000-00-00 00:00:00'),
(24, NULL, 28, '2019-04-24 12:49:24', '0000-00-00 00:00:00'),
(25, NULL, 29, '2019-04-25 19:37:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `featured_cakes`
--

CREATE TABLE `featured_cakes` (
  `id` bigint(20) NOT NULL,
  `image1` varchar(512) NOT NULL,
  `image2` varchar(512) DEFAULT NULL,
  `image3` varchar(512) DEFAULT NULL,
  `image4` varchar(512) DEFAULT NULL,
  `image5` varchar(512) DEFAULT NULL,
  `image6` varchar(512) DEFAULT NULL,
  `image7` varchar(512) DEFAULT NULL,
  `image8` varchar(512) DEFAULT NULL,
  `image9` varchar(512) DEFAULT NULL,
  `image10` varchar(512) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(4000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `featured_cakes`
--

INSERT INTO `featured_cakes` (`id`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`, `image10`, `title`, `description`, `created_at`, `modified_at`) VALUES
(1, 'image1.jpg', 'image4.jpg', 'image3.jpg', 'image4.jpg', 'image5.jpg', 'image6.jpg', 'image7.jpg', 'image6.jpg', '', '', 'Pineapple', 'Good and nice cakes are available.', '2019-03-24 09:36:22', '2019-04-22 07:50:55'),
(2, 'image3.jpg', 'image7.jpg', 'image6.jpg', '', '', '', '', '', '', '', 'Stawberrys', 'Good varieties of cake for our users.', '2019-03-24 11:17:10', '2019-04-12 10:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `baker_id` bigint(20) DEFAULT NULL,
  `description` varchar(4000) NOT NULL,
  `image1` varchar(512) DEFAULT NULL,
  `image2` varchar(512) DEFAULT NULL,
  `image3` varchar(512) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `baker_id` bigint(20) NOT NULL,
  `image1` varchar(512) NOT NULL,
  `image2` varchar(512) DEFAULT NULL,
  `image3` varchar(512) DEFAULT NULL,
  `image4` varchar(512) DEFAULT NULL,
  `image5` varchar(512) DEFAULT NULL,
  `image6` varchar(512) DEFAULT NULL,
  `image7` varchar(512) DEFAULT NULL,
  `image8` varchar(512) DEFAULT NULL,
  `image9` varchar(512) DEFAULT NULL,
  `image10` varchar(512) DEFAULT NULL,
  `quantity` int(1) NOT NULL,
  `pounds` int(1) NOT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `contact_number` varchar(13) NOT NULL,
  `delivery_location` varchar(1024) NOT NULL COMMENT 'Map address',
  `delivery_address` varchar(1024) NOT NULL COMMENT 'Home address',
  `delivery_date` date NOT NULL,
  `delivery_time` time NOT NULL,
  `payment_method` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Cash & 2 = Credit/Debit Card',
  `card_number` varchar(19) DEFAULT NULL,
  `expiration_date` varchar(5) DEFAULT NULL,
  `cvv` varchar(3) DEFAULT NULL,
  `cardholder_name` varchar(70) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Pending, 2 = Accepted, 3 = Rejected, 4 = Confirmed, 5 = Cancelled, 6 = Ongoing & 7 = Completed',
  `rated_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = True & 0 = False',
  `rejected_reason` varchar(4000) DEFAULT NULL,
  `subtotal` bigint(20) NOT NULL DEFAULT '0',
  `delivery_charges` bigint(20) NOT NULL DEFAULT '0',
  `total_amount` bigint(20) AS (subtotal + delivery_charges) VIRTUAL,
  `baker_earning` bigint(20) DEFAULT '0' COMMENT '80% of the total_amount',
  `company_earning` bigint(20) DEFAULT '0' COMMENT '20% of the total_amount',
  `rating` varchar(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `baker_id`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`, `image10`, `quantity`, `pounds`, `description`, `contact_number`, `delivery_location`, `delivery_address`, `delivery_date`, `delivery_time`, `payment_method`, `card_number`, `expiration_date`, `cvv`, `cardholder_name`, `country`, `status`, `rated_status`, `rejected_reason`, `subtotal`, `delivery_charges`, `total_amount`, `baker_earning`, `company_earning`, `rating`, `created_at`, `modified_at`) VALUES
(1, 4, 24, 'IMG_ORDER_01_20190315_011618.jpg', 'IMG_ORDER_02_20190315_011618.jpg', 'IMG_ORDER_03_20190315_011618.jpg', 'IMG_ORDER_04_20190315_011618.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+923362595588', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Vv', '2019-03-15', '20:00:00', 1, NULL, NULL, NULL, NULL, NULL, 7, 1, NULL, 0, 0, 0, 0, 0, '4.0', '2019-03-15 12:16:18', '2019-04-24 04:36:36'),
(2, 4, 24, 'IMG_ORDER_01_20190315_012026.jpg', 'IMG_ORDER_02_20190315_012026.jpg', 'IMG_ORDER_03_20190315_012026.jpg', 'IMG_ORDER_04_20190315_012026.jpg', 'IMG_ORDER_05_20190315_012026.jpg', 'IMG_ORDER_06_20190315_012026.jpg', 'IMG_ORDER_07_20190315_012026.jpg', 'IMG_ORDER_08_20190315_012026.jpg', 'IMG_ORDER_09_20190315_012026.jpg', 'IMG_ORDER_10_20190315_012026.jpg', 1, 1, NULL, '+923323467942', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Flat B-67 SB-3 Noman Heights FB Area Blk 9 Karachi', '2019-03-15', '20:00:00', 1, NULL, NULL, NULL, NULL, NULL, 6, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-03-15 12:20:26', '2019-03-15 12:20:26'),
(3, 4, 24, 'IMG_ORDER_01_20190316_125548.jpg', 'IMG_ORDER_02_20190316_125548.jpg', 'IMG_ORDER_03_20190316_125548.jpg', 'IMG_ORDER_04_20190316_125548.jpg', 'IMG_ORDER_05_20190316_125548.jpg', 'IMG_ORDER_06_20190316_125548.jpg', 'IMG_ORDER_07_20190316_125548.jpg', 'IMG_ORDER_08_20190316_125548.jpg', 'IMG_ORDER_09_20190316_125548.jpg', 'IMG_ORDER_10_20190316_125548.jpg', 1, 1, NULL, '+923363256855', '13 National Stadium Rd, Karsaz Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', 'Hello World', '2019-03-16', '20:00:00', 1, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-03-16 11:55:48', '2019-03-16 11:55:48'),
(4, 4, 24, 'IMG_ORDER_01_20190319_052049.jpg', 'IMG_ORDER_02_20190319_052049.jpg', 'IMG_ORDER_03_20190319_052049.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, NULL, '+923323467941', 'Lyari Expressway', 'Bahria University', '2019-03-20', '18:00:00', 2, NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-03-19 16:20:49', '2019-03-19 16:20:49'),
(6, 4, 24, 'IMG_ORDER_01_20190325_092933.jpg', 'IMG_ORDER_02_20190325_092933.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Dont ring the bell.', '+923323467941', 'KB-44, Gulshan-e-Amin Plaza, Federal B Area Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Gulshan E Amin Banglows', '2019-03-26', '12:00:00', 2, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-03-25 20:29:33', '2019-04-24 02:02:26'),
(8, 4, 24, 'IMG_ORDER_01_20190412_113703.jpg', 'IMG_ORDER_02_20190412_113703.jpg', 'IMG_ORDER_03_20190412_113703.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Lol\nLol\nLol\nLol\nLol\nThis a sample description\nHahahahahha\nHahaahhaahhah\nHahahahahha\nHahaahhaahhah\nHahaahhaahhah', '+923361276437', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Bahria University', '2019-04-12', '17:00:00', 1, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 50, 50, 100, 0, 0, '5.0', '2019-04-12 22:37:03', '2019-04-24 04:49:49'),
(14, 4, 24, 'IMG_ORDER_01_20190424_033040.jpg', 'IMG_ORDER_02_20190424_033040.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+923302533222', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Bhhggh', '2019-04-25', '12:00:00', 1, NULL, NULL, NULL, NULL, NULL, 3, 0, 'Hahahh\nAh\n\nHhahah\n\n\nA\nA\nA\nA\n\nA\nA', 0, 0, 0, 0, 0, '0', '2019-04-24 01:30:40', '2019-04-24 02:05:04'),
(15, 4, 24, 'IMG_ORDER_01_20190424_044136.jpg', 'IMG_ORDER_02_20190424_044136.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+926399988669', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Hhh', '2019-04-26', '14:30:00', 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-04-24 02:41:36', '2019-04-24 03:30:39'),
(16, 4, 24, 'IMG_ORDER_01_20190424_034732.jpg', 'IMG_ORDER_02_20190424_034732.jpg', 'IMG_ORDER_03_20190424_034732.jpg', 'IMG_ORDER_04_20190424_034732.jpg', 'IMG_ORDER_05_20190424_034732.jpg', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+923326346764', '13 National Stadium Rd, Karsaz Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', 'Nj', '2019-04-24', '21:00:00', 2, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-04-24 13:47:32', '2019-04-24 13:50:16'),
(17, 4, 24, 'IMG_ORDER_01_20190424_101031.jpg', 'IMG_ORDER_02_20190424_101031.jpg', 'IMG_ORDER_03_20190424_101031.jpg', 'IMG_ORDER_04_20190424_101031.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+923361234667', 'C 19, Block 20 (Saadat Colony, Ancholi) Ancholi Block 20 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Bahria University', '2019-04-25', '12:00:00', 2, '4111443666431466', '12/22', '503', 'JHON DOE', 'Pakistan', 1, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-04-24 20:10:32', '2019-04-24 20:10:32'),
(18, 4, 24, 'IMG_ORDER_01_20190424_101400.jpg', 'IMG_ORDER_02_20190424_101400.jpg', 'IMG_ORDER_03_20190424_101400.jpg', 'IMG_ORDER_04_20190424_101400.jpg', 'IMG_ORDER_05_20190424_101400.jpg', 'IMG_ORDER_06_20190424_101400.jpg', 'IMG_ORDER_07_20190424_101400.jpg', 'IMG_ORDER_08_20190424_101400.jpg', 'IMG_ORDER_09_20190424_101400.jpg', NULL, 1, 1, NULL, '+923361234567', 'St 1, FB Indus-Area Block 21 Block 21 Gulberg Town, Karachi, Karachi City, Sindh, Pakistan', 'Gulshan E Iqbal', '2019-04-25', '12:00:00', 1, NULL, NULL, NULL, NULL, NULL, 7, 1, NULL, 250, 50, 300, 240, 60, '5.0', '2019-04-24 20:14:00', '2019-04-24 20:28:50'),
(19, 4, 24, 'IMG_ORDER_01_20190425_010454.jpg', 'IMG_ORDER_02_20190425_010454.jpg', 'IMG_ORDER_03_20190425_010454.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '+923365731953', '13 National Stadium Rd, Karsaz Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan', 'Bahria University', '2019-04-25', '19:00:00', 1, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 0, 0, 0, 0, 0, '0', '2019-04-25 11:04:54', '2019-04-25 11:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `baker_id` bigint(20) NOT NULL,
  `rating` varchar(4) NOT NULL DEFAULT '0',
  `review` varchar(4000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `baker_id`, `rating`, `review`, `created_at`) VALUES
(1, 4, 24, '4.0', 'Highly recommend. I had a great experience.', '2019-04-24 04:36:36'),
(2, 4, 24, '5.0', 'Excellent quality and taste', '2019-04-24 04:49:49'),
(3, 4, 24, '5.0', 'Wonderful. Highly recommend.', '2019-04-24 20:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `firebase_tokens` text,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_token` varchar(32) DEFAULT NULL,
  `email_verified_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Unverified/False & 1 = Verified/True',
  `mobile_number` varchar(13) DEFAULT NULL COMMENT '+92xxxxxxxxxx',
  `password` varchar(255) DEFAULT NULL,
  `password_token` varchar(32) DEFAULT NULL,
  `account_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Active & 0 = Blocked',
  `account_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Email & 2 = Facebook',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firebase_tokens`, `first_name`, `last_name`, `email`, `email_token`, `email_verified_status`, `mobile_number`, `password`, `password_token`, `account_status`, `account_type`, `created_at`, `modified_at`) VALUES
(1, 'dYum-k81dBI:APA91bE56p1edfCQ0mjYfus3VTWvbsP7ESB4V2KP1TWNpA49LKqOUP3h2alKC0sWI03Tb0n-vnVftQ7RLjvKNvJeRoO1L3380k8iaUaDlyz47EA2a1pwyF-1GOpz5EPDuBiBlAGrr6LU,ezRVktvE9vM:APA91bF1dZfgBHWtx-c2I09P7iRhUK3UQBphY6jLCZFgBIPA9Wqt2RCqyoMbake6nIF9oamTfPNMB2fHT12vEp5CvsWRumABJiTIZNBwCEgrHpOFwFdtWg1UoKb3lgLDtuc7AJFOHrgq,dxSG3uYrkFo:APA91bHjdvsrrxdNDL_kF5Lo4UOTbGvIzu5X3ZUdDRh-EpC2_rOn7-fpRS1zIF8mggKYmVpxrqx46Z3qPeVgJTkuAq7O1Jn4Fc_E6lyd58eCYAeCLtZpNF7aZ7OVQjD_tGnbCK97i_L3,', 'Syed Mohsin', 'Raza', 'mohsinsyed1997@gmail.com', '033803311ebe02cec3653ca7ad36bc02', 1, '+923323467941', 'Mohsin12345', 'd53dec8ada6efcbdfdf15d6dac6a0dde', 1, 1, '2018-12-04 20:07:06', '2019-04-22 13:55:28'),
(4, 'dYum-k81dBI:APA91bE56p1edfCQ0mjYfus3VTWvbsP7ESB4V2KP1TWNpA49LKqOUP3h2alKC0sWI03Tb0n-vnVftQ7RLjvKNvJeRoO1L3380k8iaUaDlyz47EA2a1pwyF-1GOpz5EPDuBiBlAGrr6LU,ezRVktvE9vM:APA91bF1dZfgBHWtx-c2I09P7iRhUK3UQBphY6jLCZFgBIPA9Wqt2RCqyoMbake6nIF9oamTfPNMB2fHT12vEp5CvsWRumABJiTIZNBwCEgrHpOFwFdtWg1UoKb3lgLDtuc7AJFOHrgq,', 'Muhammad', 'Sufyan', 'wishacake19@gmail.com', '44e7074262d90c6d3c6b347f76c21c2e', 1, NULL, NULL, NULL, 1, 2, '2018-12-09 09:45:59', '2019-03-23 09:51:19'),
(5, NULL, 'Umair Bin', 'Ali', 'umiiali4433@gmail.com', NULL, 0, '+923321234567', 'Umair12345', NULL, 1, 1, '2018-12-09 09:50:08', '2019-01-19 12:35:14'),
(6, NULL, 'Tehreem', 'Ali', 'tehreemali21@yahoo.com', NULL, 0, '+923360851505', '12345678', NULL, 1, 1, '2019-03-12 13:29:37', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `bakers`
--
ALTER TABLE `bakers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_number` (`mobile_number`),
  ADD UNIQUE KEY `cnic` (`cnic`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_complaints_user_id` (`user_id`),
  ADD KEY `fk_complaints_baker_id` (`baker_id`);

--
-- Indexes for table `favorite_bakers`
--
ALTER TABLE `favorite_bakers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_favorite_bakers_baker_id` (`baker_id`);

--
-- Indexes for table `featured_cakes`
--
ALTER TABLE `featured_cakes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_help_user_id` (`user_id`),
  ADD KEY `fk_help_baker_id` (`baker_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orders_user_id` (`user_id`),
  ADD KEY `fk_orders_baker_id` (`baker_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reviews_user_id` (`user_id`),
  ADD KEY `fk_reviews_baker_id` (`baker_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_number` (`mobile_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bakers`
--
ALTER TABLE `bakers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `favorite_bakers`
--
ALTER TABLE `favorite_bakers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `featured_cakes`
--
ALTER TABLE `featured_cakes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `complaints`
--
ALTER TABLE `complaints`
  ADD CONSTRAINT `fk_complaints_baker_id` FOREIGN KEY (`baker_id`) REFERENCES `bakers` (`id`),
  ADD CONSTRAINT `fk_complaints_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `favorite_bakers`
--
ALTER TABLE `favorite_bakers`
  ADD CONSTRAINT `fk_favorite_bakers_baker_id` FOREIGN KEY (`baker_id`) REFERENCES `bakers` (`id`);

--
-- Constraints for table `help`
--
ALTER TABLE `help`
  ADD CONSTRAINT `fk_help_baker_id` FOREIGN KEY (`baker_id`) REFERENCES `bakers` (`id`),
  ADD CONSTRAINT `fk_help_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_baker_id` FOREIGN KEY (`baker_id`) REFERENCES `bakers` (`id`),
  ADD CONSTRAINT `fk_orders_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `fk_reviews_baker_id` FOREIGN KEY (`baker_id`) REFERENCES `bakers` (`id`),
  ADD CONSTRAINT `fk_reviews_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
