<?php

class FeaturedCake
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function read()
    {
        $result = $this->con->query("SELECT * FROM featured_cakes ORDER BY created_at");
        $featured_cakes = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $featured_cakes[] = $row;
        }
        return $this->attachUploadPathToImageNames($featured_cakes);
    }

    private function attachUploadPathToImageNames($featured_cakes)
    {
        $upload_path = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/';
        foreach ($featured_cakes as $key => $value) {
            if ($value['image1'] == '' || $value['image1'] == 'null') ;
            else {
                $featured_cakes[$key]['image1'] = $upload_path . $value['image1'];
            }
            if ($value['image2'] == '' || $value['image2'] == 'null') ;
            else {
                $featured_cakes[$key]['image2'] = $upload_path . $value['image2'];
            }
            if ($value['image3'] == '' || $value['image3'] == 'null') ;
            else {
                $featured_cakes[$key]['image3'] = $upload_path . $value['image3'];
            }
            if ($value['image4'] == '' || $value['image4'] == 'null') ;
            else {
                $featured_cakes[$key]['image4'] = $upload_path . $value['image4'];
            }
            if ($value['image5'] == '' || $value['image5'] == 'null') ;
            else {
                $featured_cakes[$key]['image5'] = $upload_path . $value['image5'];
            }
            if ($value['image6'] == '' || $value['image6'] == 'null') ;
            else {
                $featured_cakes[$key]['image6'] = $upload_path . $value['image6'];
            }
            if ($value['image7'] == '' || $value['image7'] == 'null') ;
            else {
                $featured_cakes[$key]['image7'] = $upload_path . $value['image7'];
            }
            if ($value['image8'] == '' || $value['image8'] == 'null') ;
            else {
                $featured_cakes[$key]['image8'] = $upload_path . $value['image8'];
            }
            if ($value['image9'] == '' || $value['image9'] == 'null') ;
            else {
                $featured_cakes[$key]['image9'] = $upload_path . $value['image9'];
            }
            if ($value['image10'] == '' || $value['image10'] == 'null') ;
            else {
                $featured_cakes[$key]['image10'] = $upload_path . $value['image10'];
            }
        }
        return $featured_cakes;
    }
}

?>