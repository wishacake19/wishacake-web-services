<?php

class Order
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function validate($user_id, $baker_id)
    {
        include_once("user.php");
        $user = new User();
        include_once("baker.php");
        $baker = new Baker();

        $user_email = $user->getUser('id', $user_id, 'email');
        $baker_email = $baker->getBaker('id', $baker_id, 'email');
        $user_mobile_number = $user->getUser('id', $user_id, 'mobile_number');
        $baker_mobile_number = $baker->getBaker('id', $baker_id, 'mobile_number');

        if ($user_email === $baker_email) {
            return -1;
        } else if ($user_mobile_number === $baker_mobile_number) {
            return -1;
        } else {
            return 1;
        }
    }

    public function create($user_id, $baker_id, $image1_filename, $image2_filename, $image3_filename, $image4_filename, $image5_filename, $image6_filename, $image7_filename, $image8_filename, $image9_filename, $image10_filename, $quantity, $pounds, $description, $contact_number, $delivery_location, $delivery_address, $delivery_date, $delivery_time, $payment_method, $card_number, $expiration_date, $cvv, $cardholder_name, $country)
    {
        $sql = "INSERT INTO orders (user_id, baker_id, image1, image2, image3, image4, image5, image6, 
            image7, image8, image9, image10, quantity, pounds, description, contact_number, 
            delivery_location, delivery_address, delivery_date, delivery_time, payment_method, 
            card_number, expiration_date, cvv, cardholder_name, country) 
            VALUES ('$user_id', '$baker_id', '$image1_filename', '$image2_filename', '$image3_filename', 
            '$image4_filename', '$image5_filename', '$image6_filename', '$image7_filename', 
            '$image8_filename', '$image9_filename', '$image10_filename', '$quantity', '$pounds', 
            '$description', '$contact_number', '$delivery_location', '$delivery_address', '$delivery_date', 
            '$delivery_time', '$payment_method', '$card_number', '$expiration_date', '$cvv', '$cardholder_name', '$country')";

        if ($this->con->query($sql)) {
            $last_inserted_order_id = $this->con->insert_id;

            if ($image2_filename == '' || $image2_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image2');
            }
            if ($image3_filename == '' || $image3_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image3');
            }
            if ($image4_filename == '' || $image4_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image4');
            }
            if ($image5_filename == '' || $image5_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image5');
            }
            if ($image6_filename == '' || $image6_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image6');
            }
            if ($image7_filename == '' || $image7_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image7');
            }
            if ($image8_filename == '' || $image8_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image8');
            }
            if ($image9_filename == '' || $image9_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image9');
            }
            if ($image10_filename == '' || $image10_filename == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'image10');
            }
            if ($description == '' || $description == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'description');
            }
            if ($card_number == '' || $card_number == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'card_number');
            }
            if ($expiration_date == '' || $expiration_date == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'expiration_date');
            }
            if ($cvv == '' || $cvv == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'cvv');
            }
            if ($cardholder_name == '' || $cardholder_name == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'cardholder_name');
            }
            if ($country == '' || $country == 'null') {
                $this->updateNullValue($last_inserted_order_id, 'country');
            }
            return $last_inserted_order_id; // Inserted successfully
        } else {
            return -1; // Failed to insert
        }
    }

    public function updateNullValue($order_id, $attr)
    {
        if ($this->con->query("SELECT * FROM orders WHERE id = '$order_id'")->num_rows <= 0) {
            return 0; // Order not found
        } else {
            $sql = "UPDATE orders SET $attr = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function read($user_id, $baker_id, $status)
    {
        $result;
        $orders_read_for;
        if ($user_id != '' && $baker_id == '' && $status == '') {
            if ($this->con->query("SELECT * FROM users WHERE id = '$user_id'")->num_rows <= 0) {
                return -1; // user not found
            }
            $result = $this->con->query("SELECT o.id, o.image1, o.image2, o.image3, o.image4, 
                o.image5, o.image6, o.image7, o.image8, o.image9, o.image10, o.created_at, o.status, 
                o.quantity, o.pounds, o.description, o.contact_number, o.delivery_location, o.delivery_address, 
                o.delivery_date, o.delivery_time, o.subtotal, o.delivery_charges, o.total_amount, 
                o.payment_method, o.rejected_reason, o.rating AS order_rating, o.rated_status, b.id AS baker_id, b.image, b.first_name, b.last_name, b.email, b.mobile_number, 
                b.location_name, b.location_address, b.location_latitude, b.location_longitude, b.slider_image1,
                b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids 
                FROM orders o, bakers b, favorite_bakers fb 
                WHERE o.baker_id = b.id AND b.id = fb.baker_id AND o.user_id = '$user_id'
                ORDER BY o.created_at DESC");

            $orders_read_for = 'user';
        } else if ($baker_id != '' && $status != '' && $user_id == '') {
            if ($this->con->query("SELECT * FROM bakers WHERE id = '$baker_id'")->num_rows <= 0) {
                return -2; // baker not found
            }
            $result = $this->con->query("SELECT o.id, o.image1, o.image2, o.image3, o.image4, 
                o.image5, o.image6, o.image7, o.image8, o.image9, o.image10, o.created_at, o.status, 
                o.quantity, o.pounds, o.description, o.contact_number, o.delivery_location, o.delivery_address, 
                o.delivery_date, o.delivery_time, o.subtotal, o.delivery_charges, o.total_amount, o.baker_earning, o.company_earning,
                o.payment_method, o.rejected_reason, o.rating AS order_rating, u.id AS user_id, u.first_name, u.last_name, u.email
                FROM orders o, users u
                WHERE o.user_id = u.id AND o.baker_id = '$baker_id' AND o.status = '$status'
                ORDER BY o.delivery_date, o.delivery_time");

            $orders_read_for = 'baker';
        }

        $orders = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $orders[] = $row;
        }
        return $this->attachUploadPathToImageNames($orders, $orders_read_for);
    }

    public function readSingleOrder($order_id, $order_for)
    {
        $result;
        if ($order_for == 'User') {
            $result = $this->con->query("SELECT o.id, o.image1, o.image2, o.image3, o.image4, 
                o.image5, o.image6, o.image7, o.image8, o.image9, o.image10, o.created_at, o.status, 
                o.quantity, o.pounds, o.description, o.contact_number, o.delivery_location, o.delivery_address, 
                o.delivery_date, o.delivery_time, o.subtotal, o.delivery_charges, o.total_amount, 
                o.payment_method, o.rejected_reason, o.rating AS order_rating, o.rated_status, b.id AS baker_id, b.image, b.first_name, b.last_name, b.email, b.mobile_number, 
                b.location_name, b.location_address, b.location_latitude, b.location_longitude, b.slider_image1,
                b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids 
                FROM orders o, bakers b, favorite_bakers fb 
                WHERE o.baker_id = b.id AND b.id = fb.baker_id AND o.id = '$order_id'
                ORDER BY o.created_at DESC");
        } else if ($order_for == 'Baker') {
            $result = $this->con->query("SELECT o.id, o.image1, o.image2, o.image3, o.image4, 
                o.image5, o.image6, o.image7, o.image8, o.image9, o.image10, o.created_at, o.status, 
                o.quantity, o.pounds, o.description, o.contact_number, o.delivery_location, o.delivery_address, 
                o.delivery_date, o.delivery_time, o.subtotal, o.delivery_charges, o.total_amount, o.baker_earning, o.company_earning,
                o.payment_method, o.rejected_reason, o.rating AS order_rating, u.id AS user_id, u.first_name, u.last_name, u.email
                FROM orders o, users u
                WHERE o.user_id = u.id AND o.id = '$order_id'
                ORDER BY o.delivery_date, o.delivery_time");
        }

        $orders = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $orders[] = $row;
        }
        return $this->attachUploadPathToImageNames($orders, $order_for);
    }

    private function attachUploadPathToImageNames($orders, $orders_read_for)
    {
        $upload_path = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/';
        foreach ($orders as $key => $value) {
            if ($orders_read_for == 'user' || $orders_read_for == 'User') {
                if ($value['image'] == '' || $value['image'] == 'null') ;
                else {
                    $orders[$key]['image'] = $upload_path . $value['image'];
                }
                if ($value['slider_image1'] == '' || $value['slider_image1'] == 'null') ;
                else {
                    $orders[$key]['slider_image1'] = $upload_path . $value['slider_image1'];
                }
                if ($value['slider_image2'] == '' || $value['slider_image2'] == 'null') ;
                else {
                    $orders[$key]['slider_image2'] = $upload_path . $value['slider_image2'];
                }
                if ($value['slider_image3'] == '' || $value['slider_image3'] == 'null') ;
                else {
                    $orders[$key]['slider_image3'] = $upload_path . $value['slider_image3'];
                }
            }
            if ($value['image1'] == '' || $value['image1'] == 'null') ;
            else {
                $orders[$key]['image1'] = $upload_path . $value['image1'];
            }
            if ($value['image2'] == '' || $value['image2'] == 'null') ;
            else {
                $orders[$key]['image2'] = $upload_path . $value['image2'];
            }
            if ($value['image3'] == '' || $value['image3'] == 'null') ;
            else {
                $orders[$key]['image3'] = $upload_path . $value['image3'];
            }
            if ($value['image4'] == '' || $value['image4'] == 'null') ;
            else {
                $orders[$key]['image4'] = $upload_path . $value['image4'];
            }
            if ($value['image5'] == '' || $value['image5'] == 'null') ;
            else {
                $orders[$key]['image5'] = $upload_path . $value['image5'];
            }
            if ($value['image6'] == '' || $value['image6'] == 'null') ;
            else {
                $orders[$key]['image6'] = $upload_path . $value['image6'];
            }
            if ($value['image7'] == '' || $value['image7'] == 'null') ;
            else {
                $orders[$key]['image7'] = $upload_path . $value['image7'];
            }
            if ($value['image8'] == '' || $value['image8'] == 'null') ;
            else {
                $orders[$key]['image8'] = $upload_path . $value['image8'];
            }
            if ($value['image9'] == '' || $value['image9'] == 'null') ;
            else {
                $orders[$key]['image9'] = $upload_path . $value['image9'];
            }
            if ($value['image10'] == '' || $value['image10'] == 'null') ;
            else {
                $orders[$key]['image10'] = $upload_path . $value['image10'];
            }
        }
        return $orders;
    }

    public function getOrder($where, $value, $returnValue)
    {
        $result = $this->con->query("SELECT * FROM orders WHERE $where = '$value'");
        while ($row = mysqli_fetch_assoc($result)) {
            $value = $row[$returnValue];
        }
        return $value;
    }

    public function updateRatedStatusAndOrderRating($order_id, $rating)
    {
        if ($this->con->query("SELECT * FROM orders WHERE id = '$order_id'")->num_rows <= 0) {
            return 0; // The order with this id does not exist in the database
        } else {
            $sql = "UPDATE orders SET rated_status = '1', rating = '$rating', modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function getOrderStatusCodeByStatusString($status)
    {
        $status_code;
        if ($status == 'New' || $status == 'Pending') {
            $status_code = '1';
        } else if ($status == 'Accepted' || $status == 'Re-accept') {
            $status_code = '2';
        } else if ($status == 'Rejected') {
            $status_code = '3';
        } else if ($status == 'Confirmed') {
            $status_code = '4';
        } else if ($status == 'Cancelled') {
            $status_code = '5';
        } else if ($status == 'Ongoing') {
            $status_code = '6';
        } else if ($status == 'Completed') {
            $status_code = '7';
        }
        return $status_code;
    }

    public function getBakerEarning($baker_id)
    {
        $result = $this->con->query("SELECT SUM(baker_earning) AS baker_earning FROM orders WHERE baker_id = '$baker_id' GROUP BY baker_id");
        $baker_earning;
        while ($row = mysqli_fetch_assoc($result)) {
            $baker_earning = $row['baker_earning'];
        }
        return $baker_earning;
    }

    public function getOrdersCount($baker_id, $status)
    {
        if ($this->con->query("SELECT * FROM orders WHERE baker_id = '$baker_id'")->num_rows <= 0) {
            return 0; // The baker with this id does not have any order yet.
        } else {
            $status_code = $this->getOrderStatusCodeByStatusString($status);
            $result = $this->con->query("SELECT COUNT(status) AS orders_count  FROM orders WHERE baker_id = '$baker_id' AND status = '$status_code'");
            $orders_count;
            while ($row = mysqli_fetch_assoc($result)) {
                $orders_count = $row['orders_count'];
            }
            return $orders_count;
        }
    }

    public function updateStatus($order_id, $status, $subtotal, $delivery_charges, $total_amount, $rejected_reason)
    {
        $sql;
        if ($status == 'Accepted' || $status == '2' || $status == 'Re-accept') {
            $status = '2'; // Coz value '2' bh aa skti 'Re-accept' bh aaskti, but both ka status same hai i.e. 2
            $sql = "UPDATE orders SET status = '$status', subtotal = '$subtotal', 
            delivery_charges = '$delivery_charges', total_amount = '$total_amount', 
            modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
        } else if ($status == 'Rejected' || $status == '3') {
            if ($rejected_reason == 'null' || $rejected_reason == '') {
                $sql = "UPDATE orders SET status = '$status', subtotal = '0', 
                delivery_charges = '0', total_amount = '0', modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
            } else {
                $sql = "UPDATE orders SET status = '$status', subtotal = '0', 
                delivery_charges = '0', total_amount = '0', rejected_reason = '$rejected_reason', 
                modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
            }
        } else if ($status == 'Completed' || $status == '7') {
            $total_amount_val = (int)$total_amount;
            $baker_earning = (80 * $total_amount_val) / 100;
            $company_earning = (20 * $total_amount_val) / 100;

            $sql = "UPDATE orders SET status = '$status', subtotal = '$subtotal', 
            delivery_charges = '$delivery_charges', total_amount = '$total_amount', 
            baker_earning = '$baker_earning', company_earning = '$company_earning', 
            modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
        } else if ($status == 'Confirmed' || $status == '4'
            || $status == 'Cancelled' || $status == '5'
            || $status == 'Ongoing' || $status == '6'
            || $status == 'Completed' || $status == '7') {
            $sql = "UPDATE orders SET status = '$status', subtotal = '$subtotal', 
            delivery_charges = '$delivery_charges', total_amount = '$total_amount', 
            modified_at = CURRENT_TIMESTAMP() WHERE id = '$order_id'";
        }
        if ($this->con->query($sql)) {
            return 1; // Updated successfully
        } else {
            return -1; // Failed to update
        }
    }
}

?>