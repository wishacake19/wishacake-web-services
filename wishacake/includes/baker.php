<?php

class Baker
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function validate($email, $mobile_number, $cnic)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE email = '$email'")->num_rows > 0) {
            return 0; // Baker with the same email already exist
        } else if ($this->con->query("SELECT * FROM bakers WHERE mobile_number = '$mobile_number' ")->num_rows > 0) {
            return 2; // The mobile number is already in use by another account
        } else if ($this->con->query("SELECT * FROM bakers WHERE cnic = '$cnic' ")->num_rows > 0) {
            return 3; // The CNIC is already in use by another account
        } else {
            return 1;
        }
    }

    public function create($firebase_token, $image, $first_name, $last_name, $email, $mobile_number, $cnic, $pin, $cnic_front, $cnic_back, $location_id, $location_name, $location_address, $location_latitude, $location_longitude)
    {
        $firebase_token_new = $firebase_token . ',';
        if ($image == '' || $image == 'null') {
            $sql = "INSERT INTO bakers (firebase_tokens, first_name, last_name, email, mobile_number, cnic, pin, cnic_front, cnic_back, location_id, location_name, location_address, location_latitude, location_longitude)
				VALUES ('$firebase_token_new', '$first_name', '$last_name', '$email', '$mobile_number', '$cnic', '$pin', '$cnic_front', '$cnic_back', '$location_id','$location_name','$location_address','$location_latitude','$location_longitude')";
        } else {
            $sql = "INSERT INTO bakers (firebase_tokens, image, first_name, last_name, email, mobile_number, cnic, pin, cnic_front, cnic_back, location_id, location_name, location_address, location_latitude, location_longitude)
				VALUES ('$firebase_token_new', '$image', '$first_name', '$last_name', '$email', '$mobile_number', '$cnic', '$pin', '$cnic_front', '$cnic_back', '$location_id','$location_name','$location_address','$location_latitude','$location_longitude')";
        }
        if ($this->con->query($sql)) {
            $last_inserted_baker_id = $this->con->insert_id;
            if ($last_inserted_baker_id == '' || $last_inserted_baker_id == 'null') ;
            else {
                $sql_2 = "INSERT INTO favorite_bakers (baker_id) VALUES ('$last_inserted_baker_id')";
                $this->con->query($sql_2);
            }
            return 1; // Inserted successfully
        } else {
            return -1; // Failed to insert
        }
    }

    public function read($mobile_number, $order_by)
    {
        $bakers = array();
        $result;
        if ($mobile_number == '') {
            // AND b.active_status = '1'
            $result = $this->con->query("SELECT b.id, b.image, b.first_name, b.last_name, b.email, b.mobile_number, b.location_name, b.location_address, b.location_latitude, b.location_longitude, b.slider_image1, b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids FROM bakers b, favorite_bakers fb WHERE b.id = fb.baker_id AND b.account_status = '1' ORDER BY b." . $order_by . " DESC");
            while ($row = mysqli_fetch_assoc($result)) {
                $bakers[] = $row;
            }
        } else if ($mobile_number != '') {
            if ($this->con->query("SELECT * FROM bakers WHERE mobile_number = '$mobile_number'")->num_rows > 0) {
                // Baker data
                $result = $this->con->query("SELECT * FROM bakers WHERE mobile_number = '$mobile_number'");
                while ($row = mysqli_fetch_assoc($result)) {
                    $bakers[] = $row;
                }

                // Baker earning
                $baker_id = $this->getBaker('mobile_number', $mobile_number, 'id');
                $result_earning = $this->con->query("SELECT SUM(baker_earning) AS baker_earning FROM orders WHERE baker_id = '$baker_id' GROUP BY baker_id");
                $baker_earning = null;
                while ($row = mysqli_fetch_assoc($result_earning)) {
                    $baker_earning = $row['baker_earning'];
                }
                // Append the baker_earning at the end of the array object
                foreach ($bakers as $key => $value) {
                    $bakers[$key]['baker_earning'] = $baker_earning;
                }
            } else {
                return 0; // The baker with this mobile_number does not exist in the database
            }
        }
        return $this->attachUploadPathToImageNames($bakers);
    }

    public function getFavoriteBakers($user_id)
    {
        $result = $this->con->query("SELECT b.id, b.image, b.first_name, b.last_name, b.email, b.mobile_number, b.location_name, b.location_address, b.location_latitude, b.location_longitude, b.slider_image1, b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids FROM bakers b, favorite_bakers fb WHERE b.id = fb.baker_id AND b.account_status = '1' AND fb.user_ids LIKE '%" . $user_id . "%' ORDER BY fb.modified_at DESC");
        $bakers = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $bakers[] = $row;
        }
        return $this->attachUploadPathToImageNames($bakers);
    }

    public function getRelevantBakers()
    {
        // AND b.active_status = '1'
        $result = $this->con->query("SELECT b.id, b.image, b.first_name, b.last_name, b.email, 
			b.mobile_number, b.location_name, b.location_address, b.location_latitude, b.location_longitude, 
			b.slider_image1, b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids 
			FROM favorite_bakers fb, bakers b, orders o 
            WHERE b.id = fb.baker_id AND b.id = o.baker_id AND b.account_status = '1' GROUP BY o.baker_id ORDER BY COUNT(o.baker_id) DESC");
        $bakers = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $bakers[] = $row;
        }
        return $this->attachUploadPathToImageNames($bakers);
    }

    public function getRecentBakers($user_id)
    {
        $result = $this->con->query("SELECT b.id, b.image, b.first_name, b.last_name, b.email, b.mobile_number, b.location_name, b.location_address, b.location_latitude, b.location_longitude, b.slider_image1, b.slider_image2, b.slider_image3, b.rating, b.active_status, fb.id AS favorite_id, fb.user_ids FROM bakers b, favorite_bakers fb, orders o WHERE b.id = o.baker_id AND b.id = fb.baker_id AND b.account_status = '1' AND o.user_id = '$user_id' ORDER BY o.created_at DESC");
        $bakers = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $bakers[] = $row;
        }
        return $this->attachUploadPathToImageNames($bakers);
    }

    private function attachUploadPathToImageNames($bakers)
    {
        $upload_path = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/';
        foreach ($bakers as $key => $value) {
            if ($value['image'] == '' || $value['image'] == 'null') ;
            else {
                $bakers[$key]['image'] = $upload_path . $value['image'];
            }
            if ($value['slider_image1'] == '' || $value['slider_image1'] == 'null') ;
            else {
                $bakers[$key]['slider_image1'] = $upload_path . $value['slider_image1'];
            }
            if ($value['slider_image2'] == '' || $value['slider_image2'] == 'null') ;
            else {
                $bakers[$key]['slider_image2'] = $upload_path . $value['slider_image2'];
            }
            if ($value['slider_image3'] == '' || $value['slider_image3'] == 'null') ;
            else {
                $bakers[$key]['slider_image3'] = $upload_path . $value['slider_image3'];
            }
        }
        return $bakers;
    }

    public function update($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return -1; // No baker found corresponding to the provided id
        } // This is the condition when email is similar to one of the record in DB
        else if ($this->con->query("SELECT email FROM bakers WHERE email = '$email'")->num_rows > 0) {
            $bakerEmail = $this->getBaker('id', $id, 'email');
            if ($email === $bakerEmail) {
                $sql = "UPDATE bakers SET email = '$email', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                if ($this->con->query($sql)) { // If email updated successful then check for mobile number
                    if ($this->con->query("SELECT mobile_number FROM bakers WHERE mobile_number = '$mobile_number'")->num_rows > 0) {
                        $bakerMobileNumber = $this->getBaker('id', $id, 'mobile_number');
                        if ($mobile_number === $bakerMobileNumber) {
                            return $this->performUpdate($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);
                        } else {
                            return -3; // Mobile number already exist in the database
                        }
                    } else {
                        return $this->performUpdate($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);
                    }
                } else {
                    return -4; // Failed to update
                }
            } else {
                return -2; // Email already exist in the database
            }
        } // This is the condition when mobile number is similar to one of the record in DB
        else if ($this->con->query("SELECT mobile_number FROM bakers WHERE mobile_number = '$mobile_number'")->num_rows > 0) {
            $bakerMobileNumber = $this->getBaker('id', $id, 'mobile_number');
            if ($mobile_number === $bakerMobileNumber) {
                return $this->performUpdate($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);
            } else {
                return -3; // Mobile number already exist in the database
            }
        } // This is the condition when email and mobile number both will be unique
        else {
            return $this->performUpdate($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);
        }
    }

    private function performUpdate($id, $image, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude)
    {
        $sql;
        if ($image == '' || $image == 'null' || $image == 'Remove') {
            $sql = "UPDATE bakers SET image = null, ";
        } else {
            $sql = "UPDATE bakers SET image = '$image', ";
        }
        $sql .= "first_name = '$first_name', last_name = '$last_name', email = '$email', 
			mobile_number = '$mobile_number', location_id = '$location_id', 
			location_name = '$location_name', location_address = '$location_address', 
			location_latitude = '$location_latitude', location_longitude = '$location_longitude', 
			modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        if ($this->con->query($sql)) {
            return 1; // Updated successfully
        } else {
            return -4; // Failed to update
        }
    }

    public function updateFavoriteStatus($user_id, $favorite_id, $favorite_status)
    {
        $sql;
        if ($favorite_status == 'Favorite' || $favorite_status == 'true' || $favorite_status == '1') {
            $user_ids = $this->getFavoriteBaker('id', $favorite_id, 'user_ids');
            $user_ids_new = $user_ids;

            // This user has already marked requested baker as favorite.
            // works like contains function
            if (strpos($user_ids, $user_id) !== false) {
                $user_ids_new = str_replace($user_id . ',', "", $user_ids);
            }
            $user_ids_new = $user_ids_new . $user_id . ',';
            $sql = "UPDATE favorite_bakers SET user_ids = '$user_ids_new', modified_at = CURRENT_TIMESTAMP() WHERE id = '$favorite_id'";
        } else if ($favorite_status == 'Unfavorite' || $favorite_status == 'false' || $favorite_status == '0') {
            $user_ids = $this->getFavoriteBaker('id', $favorite_id, 'user_ids');
            if ($user_ids == '' || $user_ids == 'null') ;
            else {
                $user_ids_new = str_replace($user_id . ',', "", $user_ids);
            }
            $sql = "UPDATE favorite_bakers SET user_ids = '$user_ids_new', modified_at = CURRENT_TIMESTAMP() WHERE id = '$favorite_id'";
        }
        if ($this->con->query($sql)) {
            return 1; // Updated successfully
        } else {
            return -1; // Failed to update
        }
    }

    public function login($firebase_token, $mobile_number, $pin)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE mobile_number = '$mobile_number'")->num_rows <= 0) {
            return 0; // The baker with this mobile_number does not exist in the database
        } else {
            if ($this->con->query("SELECT * FROM bakers WHERE mobile_number = '$mobile_number' and pin = '$pin'")->num_rows > 0) {
                // Update firebase_token
                if ($firebase_token == '' || $firebase_token == 'null') ;
                else {
                    $firebase_tokens = $this->getBaker('mobile_number', $mobile_number, 'firebase_tokens');
                    // if already present, dont update
                    if (strpos($firebase_tokens, $firebase_token) !== false) ;
                    else {
                        $firebase_tokens_new = $firebase_tokens . $firebase_token . ',';
                        $sql = "UPDATE bakers SET firebase_tokens = '$firebase_tokens_new', modified_at = CURRENT_TIMESTAMP() WHERE mobile_number = '$mobile_number'";
                        $this->con->query($sql);
                    }
                }
                return 1; // Login successful
            } else {
                return -1; // Wrong pin
            }
        }
    }

    public function getBaker($where, $value, $returnValue)
    {
        $result = $this->con->query("SELECT * FROM bakers WHERE $where = '$value'");
        while ($row = mysqli_fetch_assoc($result)) {
            $value = $row[$returnValue];
        }
        return $value;
    }

    private function getFavoriteBaker($where, $value, $returnValue)
    {
        $result = $this->con->query("SELECT * FROM favorite_bakers WHERE $where = '$value'");
        while ($row = mysqli_fetch_assoc($result)) {
            $value = $row[$returnValue];
        }
        return $value;
    }

    public function getCurrentTimestamp()
    {
        $result = $this->con->query("SELECT CURRENT_TIMESTAMP()");
        while ($row = mysqli_fetch_assoc($result)) {
            $value = $row['CURRENT_TIMESTAMP()'];
        }
        return $value;
    }

    public function changePin($id, $pin)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return -1; // No baker found corresponding to the provided id
        } else {
            $sql = "UPDATE bakers SET pin = '$pin', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function updatePinToken($email, $pin_token)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE email = '$email'")->num_rows <= 0) {
            return 0; // The baker with this email does not exist in the database
        } else {
            $sql = "UPDATE bakers SET pin_token = '$pin_token', modified_at = CURRENT_TIMESTAMP() WHERE email = '$email'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function updateActiveStatus($id, $active_status)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return 0; // The baker with this id does not exist in the database
        } else {
            $sql = "UPDATE bakers SET active_status = '$active_status', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function updateRating($id, $rating)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return 0; // The baker with this id does not exist in the database
        } else {
            $sql = "UPDATE bakers SET rating = '$rating', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function updateSliderImage($id, $slider_image_attr, $slider_image)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return -1; // The baker with this id does not exist in the database
        } else {
            $sql;
            if ($slider_image == '' || $slider_image == 'null' || $slider_image == 'Remove') {
                $sql = "UPDATE bakers SET $slider_image_attr = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            } else {
                $sql = "UPDATE bakers SET $slider_image_attr = '$slider_image', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            }
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function clearFirebaseTokens($id)
    {
        if ($this->con->query("SELECT * FROM bakers WHERE id = '$id'")->num_rows <= 0) {
            return 0; // The baker with this id does not exist in the database
        } else {
            $sql = "UPDATE bakers SET firebase_tokens = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }
}

?>