<?php

class Complaint
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function create($user_id, $baker_id, $description, $image1, $image2, $image3)
    {
        $sql = "INSERT INTO complaints (user_id, baker_id, description, image1, image2, 
        image3) VALUES ('$user_id', '$baker_id', '$description', '$image1', '$image2', 
        '$image3')";

        if ($this->con->query($sql)) {
            $last_inserted_id = $this->con->insert_id;
            if ($image1 == '' || $image1 == 'null') {
                $this->updateNullValue($last_inserted_id, 'image1');
            }
            if ($image2 == '' || $image2 == 'null') {
                $this->updateNullValue($last_inserted_id, 'image2');
            }
            if ($image3 == '' || $image3 == 'null') {
                $this->updateNullValue($last_inserted_id, 'image3');
            }
            return 1; // Inserted successfully
        } else {
            return -1; // Failed to insert
        }
    }

    public function updateNullValue($id, $attr)
    {
        $sql = "UPDATE complaints SET $attr = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        if ($this->con->query($sql)) {
            return 1; // Updated successfully
        } else {
            return -2; // Failed to update
        }
    }
}

?>