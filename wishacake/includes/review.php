<?php

class Review
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function create($user_id, $baker_id, $rating, $review)
    {
        $sql = "INSERT INTO reviews (user_id, baker_id, rating, review) VALUES ('$user_id', '$baker_id', '$rating', '$review')";
        if ($this->con->query($sql)) {
            return 1; // Inserted successfully
        } else {
            return -1; // Failed to insert
        }
    }

    public function read($baker_id)
    {
        $sql = "SELECT u.first_name, u.last_name, r.rating, r.review, r.created_at FROM users u, reviews r WHERE u.id = r.user_id AND r.baker_id = '$baker_id' ORDER BY r.created_at DESC";
        $result = $this->con->query($sql);
        $reviews = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $reviews[] = $row;
        }
        return $reviews;
    }

    public function getReviewsCount($baker_id)
    {
        $sql = "SELECT COUNT(baker_id) AS reviews_count FROM reviews WHERE baker_id = '$baker_id'";
        $result = $this->con->query($sql);
        $reviews_count;
        if ($row = mysqli_fetch_assoc($result)) {
            $reviews_count = $row['reviews_count'];
        }
        return $reviews_count;
    }
}

?>