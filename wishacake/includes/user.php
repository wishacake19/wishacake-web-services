<?php

class User
{

    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db-connect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    public function create($firebase_token, $first_name, $last_name, $email, $password)
    {
        if ($this->con->query("SELECT * FROM users WHERE email = '$email' AND account_type = '1'")->num_rows > 0) {
            return 0; // User with the same email already exist
        } else if ($this->con->query("SELECT * FROM users WHERE email = '$email' AND account_type = '2'")->num_rows > 0) {
            return -2; // This email address is associated with a Facebook account
        } else {
            // $password = md5($password); // for password encryption
            $firebase_token_new = $firebase_token . ',';
            $sql = "INSERT INTO users (firebase_tokens, first_name, last_name, email, password) VALUES ('$firebase_token_new', '$first_name', '$last_name', '$email', '$password')";
            if ($this->con->query($sql)) {
                return 1; // Inserted successfully
            } else {
                return -1; // Failed to insert
            }
        }
    }

    public function createFbUser($firebase_token, $first_name, $last_name, $email)
    {
        if ($this->con->query("SELECT * FROM users WHERE email = '$email' AND account_type = '1'")->num_rows > 0) {
            return 0; // This email address is associated with a wishacake account
        } else if ($this->con->query("SELECT * FROM users WHERE email = '$email' AND account_type = '2'")->num_rows > 0) {
            // Update firebase_token
            if ($firebase_token == '' || $firebase_token == 'null') ;
            else {
                $firebase_tokens = $this->getUser('email', $email, 'firebase_tokens');
                // if already present, dont update
                if (strpos($firebase_tokens, $firebase_token) !== false) ;
                else {
                    $firebase_tokens_new = $firebase_tokens . $firebase_token . ',';
                    $sql = "UPDATE users SET firebase_tokens = '$firebase_tokens_new', modified_at = CURRENT_TIMESTAMP() WHERE email = '$email'";
                    $this->con->query($sql);
                }
            }
            return -2; // Fb user with the same email address already exist
        } else {
            $firebase_token_new = $firebase_token . ',';
            $sql = "INSERT INTO users (firebase_tokens, first_name, last_name, email, account_type) VALUES ('$firebase_token_new', '$first_name', '$last_name', '$email', '2')";

            if ($this->con->query($sql)) {
                return 1; // Inserted successfully
            } else {
                return -1; // Failed to insert
            }
        }
    }

    public function read($email)
    {
        $result;
        if ($email == '') {
            $result = $this->con->query("SELECT * FROM users ORDER BY created_at");
        } else {
            if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows > 0) {
                $result = $this->con->query("SELECT * FROM users WHERE email = '$email'");
            } else {
                return 0; // The user with this email does not exist in the database
            }
        }
        $arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $arr[] = $row;
        }
        return $arr;
    }

    public function update($id, $first_name, $last_name, $email, $mobile_number)
    {
        if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
            return -1; // No user found corresponding to the provided id
        } // This is the condition when email is similar to one of the record in DB
        else if ($this->con->query("SELECT email FROM users WHERE email = '$email'")->num_rows > 0) {
            $userEmail = $this->getUser('id', $id, 'email');
            if ($email === $userEmail) {
                $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                if ($this->con->query($sql)) { // If email updated successful then check for mobile number
                    if ($this->con->query("SELECT mobile_number FROM users WHERE mobile_number = '$mobile_number'")->num_rows > 0) {
                        $userMobileNumber = $this->getUser('id', $id, 'mobile_number');
                        if ($mobile_number === $userMobileNumber) {
                            return $this->performUpdate($id, $first_name, $last_name, $email, $mobile_number);
                        } else {
                            return -3; // Mobile number already exist in the database
                        }
                    } else {
                        return $this->performUpdate($id, $first_name, $last_name, $email, $mobile_number);
                    }
                } else {
                    return -4; // Failed to update
                }
            } else {
                return -2; // Email already exist in the database
            }
        } // This is the condition when mobile number is similar to one of the record in DB
        else if ($this->con->query("SELECT mobile_number FROM users WHERE mobile_number = '$mobile_number'")->num_rows > 0) {
            $userMobileNumber = $this->getUser('id', $id, 'mobile_number');
            if ($mobile_number === $userMobileNumber) {
                return $this->performUpdate($id, $first_name, $last_name, $email, $mobile_number);
            } else {
                return -3; // Mobile number already exist in the database
            }
        } // This is the condition when email and mobile number both will be unique
        else {
            return $this->performUpdate($id, $first_name, $last_name, $email, $mobile_number);
        }
    }

    private function performUpdate($id, $first_name, $last_name, $email, $mobile_number)
    {
        $sql;
        if ($mobile_number == '' || $mobile_number == 'null') {
            $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        } else if ($mobile_number == 'remove') {
            $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', mobile_number = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        } else {
            $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', mobile_number = '$mobile_number', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        }
        if ($this->con->query($sql)) {
            return 1; // Updated successfully
        } else {
            return -4; // Failed to update
        }
    }

    public function getUser($where, $value, $returnValue)
    {
        $result = $this->con->query("SELECT * FROM users WHERE $where = '$value'");
        while ($row = mysqli_fetch_assoc($result)) {
            $value = $row[$returnValue];
        }
        return $value;
    }

    public function deactivateReactivate($id, $account_status)
    {
        if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
            return -1; // No user found corresponding to the provided id
        } else {
            if ($account_status == 'Deactivate' || $account_status == '2') {
                $sql = "UPDATE users SET account_status = '2', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                if ($this->con->query($sql)) {
                    return 1; // Deactivate successfully
                } else {
                    return -2; // Failed to update
                }
            } else if ($account_status == 'Reactivate' || $account_status == '1') {
                $sql = "UPDATE users SET account_status = '1', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                if ($this->con->query($sql)) {
                    return 0; // Reactivate successfully
                } else {
                    return -2; // Failed to update
                }
            }
        }
    }

    public function changePassword($id, $password)
    {
        if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
            return -1; // No user found corresponding to the provided id
        } else {
            $sql = "UPDATE users SET password = '$password', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function login($firebase_token, $email, $password)
    {
        if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows <= 0) {
            return 0; // The user with this email does not exist in the database
        } else {
            if ($this->con->query("SELECT * FROM users WHERE email = '$email' AND account_type = '2'")->num_rows > 0) {
                return -2; // This email address is associated with a Facebook account
            }
            // Here BINARY keyword is used for exact case matching of password
            if ($this->con->query("SELECT * FROM users WHERE email = '$email' and password = BINARY '$password'")->num_rows > 0) {
                // Update firebase_token
                if ($firebase_token == '' || $firebase_token == 'null') ;
                else {
                    $firebase_tokens = $this->getUser('email', $email, 'firebase_tokens');
                    // if already present, dont update
                    if (strpos($firebase_tokens, $firebase_token) !== false) ;
                    else {
                        $firebase_tokens_new = $firebase_tokens . $firebase_token . ',';
                        $sql = "UPDATE users SET firebase_tokens = '$firebase_tokens_new', modified_at = CURRENT_TIMESTAMP() WHERE email = '$email'";
                        $this->con->query($sql);
                    }
                }
                return 1; // Login successful
            } else {
                return -1; // Incorrect password
            }
        }
    }

    public function updateToken($email, $token, $attribute)
    {
        if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows <= 0) {
            return 0; // The user with this email does not exist in the database
        } else {
            $sql = "UPDATE users SET $attribute = '$token', modified_at = CURRENT_TIMESTAMP() WHERE email = '$email'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }

    public function clearFirebaseTokens($id)
    {
        if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
            return 0; // The user with this id does not exist in the database
        } else {
            $sql = "UPDATE users SET firebase_tokens = null, modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
            if ($this->con->query($sql)) {
                return 1; // Updated successfully
            } else {
                return -2; // Failed to update
            }
        }
    }
}

?>