<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and isset($_POST['active_status'])) {

        // store the value of POST data
        $id = $_POST['id'];
        $active_status = $_POST['active_status'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        if ($active_status == 'Online' || $active_status == 'true' || $active_status == '1') {
            $active_status = '1';
        } else if ($active_status == 'Offline' || $active_status == 'false' || $active_status == '0') {
            $active_status = '0';
        }

        $res = $baker->updateActiveStatus($id, $active_status);

        if ($res == 1) {
            $response['status'] = "success";
            if ($active_status == 'Online' || $active_status == 'true' || $active_status == '1') {
                $response['response'] = "Active status updated to online.";
            } else if ($active_status == 'Offline' || $active_status == 'false' || $active_status == '0') {
                $response['response'] = "Active status updated to offline.";
            }
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id and active_status) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>