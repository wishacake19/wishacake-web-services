<?php

// array for JSON response
$response = array();

// check if the http request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    include_once("../includes/featured-cake.php");
    $featuredCake = new FeaturedCake();

    $result = $featuredCake->read();

    $response['status'] = "success";
    $response['response'] = $result;
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (GET) is missing.";
}

echo json_encode($response);

?>