<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['email'])) {

        // store the values of POST data
        $email = $_POST['email'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        // generates token string
        $token = md5(uniqid(mt_rand()));

        $res = $user->updateToken($email, $token, 'password_token');

        // check if token is successfully updated
        if ($res == 1) {
            $response['status'] = "success";
            //  $response['response'] = "Instructions for resetting your password have been emailed to you.";
            $response['response'] = "We have sent an email to " . $email . " containing instructions on how to reset your password.";

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/reset-password?t=' . $token;

            $to = $email;
            $subject = "Reset your password";

            $where = "email";
            $firstName = $user->getUser($where, $email, 'first_name');
            $lastName = $user->getUser($where, $email, 'last_name');

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
				<br/><br/>Someone recently requested to reset the password for your wishacake account. 
				<br/><br/>To reset your password, click the link below:
				<br/><br/><a href="' . $link . '">' . $link . '</a>
				<br/><br/>If you don\'t want to reset your password or didn\'t requested for it, just ignore and delete this email.
				<br/>To keep your account secure, please don\'t forward this email to anyone.
				<br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "There is no user corresponding to this email address.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (email) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>