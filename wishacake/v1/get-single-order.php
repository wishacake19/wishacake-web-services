<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include_once("../includes/order.php");
    $order = new Order();

    // check if the required field(s) are not empty
    if (isset($_POST['order_id']) and isset($_POST['order_for'])) {

        // store the value of POST data
        $order_id = $_POST['order_id'];
        $order_for = $_POST['order_for'];

        $res = $order->readSingleOrder($order_id, $order_for);

        $response['status'] = "success";
        $response['response'] = $res;
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (order_id and order_for) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>