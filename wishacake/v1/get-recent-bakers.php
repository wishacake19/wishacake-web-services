<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id'])) {

        // store the value of POST data
        $user_id = $_POST['user_id'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $recent_bakers = $baker->getRecentBakers($user_id);
        if (sizeof($recent_bakers) > 0) {
            // $favorite_bakers = $baker->getFavoriteBakers($user_id);
            // $result_array = array_unique(array_merge($recent_bakers, $favorite_bakers), SORT_REGULAR);
            $result_array = array_unique($recent_bakers, SORT_REGULAR);
        } else {
            $result_array = $recent_bakers;
        }

        $response['status'] = "success";
        $response['response'] = array_values($result_array);
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (user_id) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>