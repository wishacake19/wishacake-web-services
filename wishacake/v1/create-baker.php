<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['firebase_token']) and
        isset($_POST['image']) and
        isset($_POST['first_name']) and
        isset($_POST['last_name']) and
        isset($_POST['email']) and
        isset($_POST['mobile_number']) and
        isset($_POST['cnic']) and
        isset($_POST['pin']) and
        isset($_POST['cnic_front']) and
        isset($_POST['cnic_back']) and
        isset($_POST['location_id']) and
        isset($_POST['location_name']) and
        isset($_POST['location_address']) and
        isset($_POST['location_latitude']) and
        isset($_POST['location_longitude'])) {

        // store the values of POST data
        $firebase_token = $_POST['firebase_token'];
        $image = $_POST['image'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $mobile_number = $_POST['mobile_number'];
        $cnic = $_POST['cnic'];
        $pin = $_POST['pin'];
        $cnic_front = $_POST['cnic_front'];
        $cnic_back = $_POST['cnic_back'];
        $location_id = $_POST['location_id'];
        $location_name = $_POST['location_name'];
        $location_address = $_POST['location_address'];
        $location_latitude = $_POST['location_latitude'];
        $location_longitude = $_POST['location_longitude'];

        $image_filename = '';
        $cnic_front_filename = '';
        $cnic_back_filename = '';

        $time = date("Ymd_his");

        // set up images
        if ($image == '' || $image == 'null') ;
        else {
            $image_filename = 'IMG_Profile_Photo_' . $time . '.jpg';
        }
        $cnic_front_filename = 'IMG_CNIC_Front_' . $time . '.jpg';
        $cnic_back_filename = 'IMG_CNIC_Back_' . $time . '.jpg';

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->create($firebase_token, $image_filename, $first_name, $last_name, $email, $mobile_number, $cnic, $pin, $cnic_front_filename,
            $cnic_back_filename, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);

        $upload_path = '../uploads/images/';

        // check if the baker is successfully created
        if ($res == 1) {
            if ($image == '' || $image == 'null') ;
            else {
                file_put_contents($upload_path . $image_filename, base64_decode($image));
            }
            file_put_contents($upload_path . $cnic_front_filename, base64_decode($cnic_front));
            file_put_contents($upload_path . $cnic_back_filename, base64_decode($cnic_back));

            $response['status'] = "success";
            $response['response'] = $baker->read($mobile_number, 'created_at');
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (firebase_token, image, first_name, last_name, email, mobile_number, cnic, pin, cnic_front, cnic_back, location_id, location_name, location_address, location_latitude & location_longitude) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>