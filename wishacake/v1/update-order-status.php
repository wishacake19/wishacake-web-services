﻿<?php

// Server key from Firebase Console
define('FIREBASE_SERVER_KEY', 'AAAAwjZrkPA:APA91bFsEh4r4TIfLD2UwvfPq_Gj-coDuvjAjt8W4wokYInjibdLuZU3RlwZU32NKzBefz-hr-TyeQumK2eNWRPOTNdeB0gPZ1VJjOrP-lkIhGie-JondpYfequNJhOACJbS61TZBUWn');

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include_once("../includes/order.php");
    $order = new Order();

    // check if the required field(s) are not empty
    if (isset($_POST['order_id']) and
        isset($_POST['user_id']) and
        isset($_POST['baker_id']) and
        isset($_POST['subtotal']) and
        isset($_POST['delivery_charges']) and
        isset($_POST['total_amount']) and
        isset($_POST['rejected_reason']) and
        isset($_POST['status'])) {

        $status_msg = '';
        $response_msg = '';

        // store the values of POST data
        $order_id = $_POST['order_id'];
        $user_id = $_POST['user_id'];
        $baker_id = $_POST['baker_id'];
        $subtotal = $_POST['subtotal'];
        $delivery_charges = $_POST['delivery_charges'];
        $total_amount = $_POST['total_amount'];
        $rejected_reason = $_POST['rejected_reason'];
        $status = $order->getOrderStatusCodeByStatusString($_POST['status']);

        include_once("../includes/user.php");
        $user = new User();
        $user_name = $user->getUser('id', $user_id, 'first_name') . ' ' . $user->getUser('id', $user_id, 'last_name');

        include_once("../includes/baker.php");
        $baker = new Baker();
        $baker_name = $baker->getBaker('id', $baker_id, 'first_name') . ' ' . $baker->getBaker('id', $baker_id, 'last_name');

        $order_image1 = $order->getOrder('id', $order_id, 'image1');

        $res = $order->updateStatus($order_id, $status, $subtotal, $delivery_charges,
            $total_amount, $rejected_reason);

        $delivery_date = date_create($order->getOrder('id', $order_id, 'delivery_date'))->format('d M, Y');
        $delivery_time = date_create($order->getOrder('id', $order_id, 'delivery_time'))->format('h:i A');
        $formatted_delivery_date_time = $delivery_date . ' • ' . $delivery_time;

        $order_created_at = $order->getOrder('id', $order_id, 'created_at');
        $date = date_create($order_created_at);
        $formatted_date = $date->format('d M, Y') . ' • ' . $date->format('h:i A');

        if ($res == -1) {
            $status_msg = "error";
            $response_msg = "Sorry, something went wrong. Please try again.";
        } else {
            if ($status == 'Accepted' || $status == '2') {
                // Push notification to user
                $firebase_tokens = $user->getUser('id', $user_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $action;
                    $title_msg;
                    $message;

                    if ($_POST['status'] == 'Re-accept') {
                        $status_msg = "success";
                        $response_msg = '[' . $formatted_delivery_date_time . '] Order re-accepted successfully.';

                        $action = 're-accepted';
                        $title_msg = '[' . $formatted_date . '] Order re-accepted by ' . $baker_name;
                        $message = 'New price of your order is PKR ' . $total_amount . ' now. Tap to view the further details';
                    } else {
                        $status_msg = "success";
                        $response_msg = '[' . $formatted_delivery_date_time . '] Order accepted successfully.';

                        $action = 'accepted';
                        $title_msg = '[' . $formatted_date . '] Order accepted by ' . $baker_name;
                        $message = 'Tap to view the details';
                    }

                    $request_data = array(
                        'title' => $title_msg,
                        'message' => $message,
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                // Email notification to user
                $to = $user->getUser('id', $user_id, 'email');
                $subject = '[' . $formatted_date . '] Order ' . $action . ' by ' . $baker_name;
                $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

                $body;
                if ($_POST['status'] == 'Re-accept') {
                    $body = 'Hi ' . $user_name . ',
                    <br/><br/>Your order has been re-accepted by ' . $baker_name . '.
                    <br/>New price of your order is PKR ' . $total_amount . ' now.
                    <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                    <br/><br/>Regards,
                    <br/>The wishacake Team';
                }
                else {
                    $body = 'Hi ' . $user_name . ',
                    <br/><br/>Your order has been accepted by ' . $baker_name . '.
                    <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                    <br/><br/>Regards,
                    <br/>The wishacake Team';
                }

                mail($to, $subject, $body, $headers);
            }
            else if ($status == 'Rejected' || $status == '3') {
                // Push notification to user
                $firebase_tokens = $user->getUser('id', $user_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $request_data = array(
                        'title' => '[' . $formatted_date . '] Order rejected by ' . $baker_name,
                        'message' => 'Tap to view the details',
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                // Email notification to user
                $to = $user->getUser('id', $user_id, 'email');
                $subject = '[' . $formatted_date . '] Order rejected by ' . $baker_name;
                $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

                $body;

                if ($rejected_reason == '' || $rejected_reason == 'null') {
                    $body = 'Hi ' . $user_name . ',
                    <br/><br/>Your order has been rejected by ' . $baker_name . '.
                    <br/><br/>The baker didn\'t provided the reason for rejecting your order.
                    <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                    <br/><br/>Regards,
                    <br/>The wishacake Team';
                }
                else {
                    $body = 'Hi ' . $user_name . ',
                    <br/><br/>Your order has been rejected by ' . $baker_name . '.
                    <br/><br/>Following is the reason provided by the baker for rejecting your order:<br/><br/>' . $rejected_reason . '
                    <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                    <br/><br/>Regards,
                    <br/>The wishacake Team';
                }

                mail($to, $subject, $body, $headers);

                $status_msg = "success";
                $response_msg = '[' . $formatted_delivery_date_time . '] Order rejected successfully.';
            }
            else if ($status == 'Confirmed' || $status == '4') {
                // Push notification to baker
                $firebase_tokens = $baker->getBaker('id', $baker_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $request_data = array(
                        'title' => '[' . $formatted_delivery_date_time . '] Order confirmed by ' . $user_name,
                        'message' => 'Tap to view the details',
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                // Email notification to baker
                $to = $baker->getBaker('id', $baker_id, 'email');
                $subject = "[" . $formatted_delivery_date_time . "] Order confirmed by " . $user_name;
                $link = 'https://play.google.com/store/apps/details?id=com.android.bakerapp';

                $body = 'Hi ' . $baker_name . ',
                    <br/><br/>Your order has been confirmed by ' . $user_name . '.
                    <br/>The shipping date and time for this order is on <strong>' . $delivery_date . '</strong> at <strong>' . $delivery_time . '</strong>
                    <br/><br/>For further details, log in to your baker account.
                    <br/><br/>Here\'s a link to the application:
                    <br/><a href="' . $link . '">' . $link . '</a>
                    <br/><br/>Regards,
                    <br/>The wishacake Team';

                mail($to, $subject, $body, $headers);
                
                $status_msg = "success";
                $response_msg = '[' . $formatted_date . '] Order confirmed successfully.';
            }
            else if ($status == 'Cancelled' || $status == '5') {
                // Push notification to baker
                $firebase_tokens = $baker->getBaker('id', $baker_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $request_data = array(
                        'title' => '[' . $formatted_delivery_date_time . '] Order cancelled by ' . $user_name,
                        'message' => 'Tap to view the details',
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                // Email notification to baker
                $to = $baker->getBaker('id', $baker_id, 'email');
                $subject = "[" . $formatted_delivery_date_time . "] Order cancelled by " . $user_name;
                $link = 'https://play.google.com/store/apps/details?id=com.android.bakerapp';

                $body = 'Hi ' . $baker_name . ',
                    <br/><br/>One of your new or approved order has been cancelled by ' . $user_name . '.
                    <br/>The shipping date and time for this order was on <strong>' . $delivery_date . '</strong> at <strong>' . $delivery_time . '</strong>
                    <br/><br/>For further details, log in to your baker account.
                    <br/><br/>Here\'s a link to the application:
                    <br/><a href="' . $link . '">' . $link . '</a>
                    <br/><br/>Regards,
                    <br/>The wishacake Team';

                mail($to, $subject, $body, $headers);

                $status_msg = "success";
                $response_msg = '[' . $formatted_date . '] Order cancelled successfully.';
            } 
            else if ($status == 'Ongoing' || $status == '6') {
                // Push notification to user
                $firebase_tokens = $user->getUser('id', $user_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $request_data = array(
                        'title' => '[' . $formatted_date . '] Order marked as ongoing by ' . $baker_name,
                        'message' => $baker_name . ' is preparing your order and is marked as ongoing. Tap to view the details',
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                // Email notification to user
                $to = $user->getUser('id', $user_id, 'email');
                $subject = '[' . $formatted_date . '] Order marked as ongoing by ' . $baker_name;
                $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

                $body = 'Hi ' . $user_name . ',
                <br/><br/>Your order is being prepared by ' . $baker_name . ' and is marked as ongoing successfully.
                <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                <br/><br/>Regards,
                <br/>The wishacake Team';
                
                mail($to, $subject, $body, $headers);

                $status_msg = "success";
                $response_msg = '[' . $formatted_delivery_date_time . '] Order marked as ongoing successfully.';
            } else if ($status == 'Completed' || $status == '7') {
                // Push notification to user
                $firebase_tokens = $user->getUser('id', $user_id, 'firebase_tokens');
                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $when = $baker->getCurrentTimestamp();

                    $request_data = array(
                        'title' => '[' . $formatted_date . '] Order marked as completed by ' . $baker_name,
                        'message' => $baker_name . ' has completed your order. Tap to view the details',
                        'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order_image1,
                        'action' => 'activity',
                        'action_destination' => 'OrderDetailsActivity',
                        'order_id' => $order_id,
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $headers = array(
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    
                    curl_exec($ch);
                    curl_close($ch);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

                $payment_method = $order->getOrder('id', $order_id, 'payment_method');
                if ($payment_method == '1') {
                   $payment_method = 'Cash';
                }
                else {
                    $payment_method = 'Card';
                }

                // Email notification to user
                $to = $user->getUser('id', $user_id, 'email');
                $subject = 'Receipt for order [' . $formatted_date . ']';
                $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

                $body = 'Hi ' . $user_name . ',
                <br/><br/>Your order is completed by ' . $baker_name . ' successfully.
                <br/>The payment details for this order are as below:
                <br/><br/>Subtotal: <strong>PKR ' . $subtotal . '</strong>
                <br/>Delivery charges: <strong>PKR ' . $delivery_charges . '</strong>
                <hr style="margin-top: 6px; margin-bottom: 6px;"/>
                Amount charged: <strong>PKR ' . $total_amount . '</strong>
                <br/>Payment method: <strong>' . $payment_method . '</strong>
                <br/><br/>Reference #: <strong>' . $order_id . '</strong>
                <br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
                <br/><br/>Regards,
                <br/>The wishacake Team';
                
                mail($to, $subject, $body, $headers);

                // Email notification to baker
                $to = $baker->getBaker('id', $baker_id, 'email');
                $subject = '[' . $formatted_delivery_date_time . '] Order completed successfully';
                $link = 'https://play.google.com/store/apps/details?id=com.android.bakerapp';

                $body = 'Hi ' . $baker_name . ',
                <br/><br/>You have successfully completed the order of ' . $user_name . '.
                <br/>The payment details for this order are as below:
                <br/><br/>Subtotal: <strong>PKR ' . $subtotal . '</strong>
                <br/>Delivery charges: <strong>PKR ' . $delivery_charges . '</strong>
                <hr style="margin-top: 6px; margin-bottom: 6px;"/>
                Amount charged: <strong>PKR ' . $total_amount . '</strong>
                <br/>Payment method: <strong>' . $payment_method . '</strong>
                <hr style="margin-top: 6px; margin-bottom: 6px;"/>
                Your earning: <strong>PKR ' . $order->getOrder('id', $order_id, 'baker_earning') . '</strong>
                <br/><br/>Reference #: <strong>' . $order_id . '</strong>
                <br/><br/>For further details, log in to your baker account.
                <br/><br/>Here\'s a link to the application:
                <br/><a href="' . $link . '">' . $link . '</a>
                <br/><br/>Regards,
                <br/>The wishacake Team';

                mail($to, $subject, $body, $headers);

                $status_msg = "success";
                $response_msg = '[' . $formatted_delivery_date_time . '] Order marked as completed successfully.';
            }
        }

        $response['status'] = $status_msg;
        $response['response'] = $response_msg;

    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (order_id, user_id, baker_id, subtotal, delivery_charges, total_amount, rejected_reason and status) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>