<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['email']) and
        isset($_POST['mobile_number']) and
        isset($_POST['cnic'])) {

        // store the values of POST data
        $email = $_POST['email'];
        $m_num = $_POST['mobile_number'];
        $cnic = $_POST['cnic'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->validate($email, $m_num, $cnic);

        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = "Account is valid.";
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "The email address is already in use by another baker.";
        } else if ($res == 2) {
            $response['status'] = "error";
            $response['errorCode'] = "2";
            $response['response'] = "The mobile number is already in use by another baker.";
        } else if ($res == 3) {
            $response['status'] = "error";
            $response['errorCode'] = "3";
            $response['response'] = "The CNIC is already in use by another baker.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (email, mobile_number and cnic) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>