<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and isset($_POST['pin'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $pin = $_POST['pin'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->changePin($id, $pin);

        $mobile_number = $baker->getBaker('id', $id, 'mobile_number');

        // check if the baker's pin is successfully changed
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = $baker->read($mobile_number, 'created_at');

            $to = $baker->getBaker('id', $id, 'email');;
            $subject = "PIN changed successfully";

            $firstName = $baker->getBaker('id', $id, 'first_name');
            $lastName = $baker->getBaker('id', $id, 'last_name');

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
				<br/><br/>Your PIN has been changed successfully. 
				<br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no baker corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id & pin) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>