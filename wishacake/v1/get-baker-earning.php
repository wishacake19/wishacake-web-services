<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include_once("../includes/order.php");
    $order = new Order();

    // check if the required field(s) are not empty
    if (isset($_POST['baker_id'])) {

        // store the value of POST data
        $baker_id = $_POST['baker_id'];

        $res = $order->getBakerEarning($baker_id);

        $response['status'] = "success";
        $response['response'] = $res;
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (baker_id) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>