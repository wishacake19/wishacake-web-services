<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and
        isset($_POST['slider_image_attr']) and
        isset($_POST['slider_image'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $slider_image_attr = $_POST['slider_image_attr'];
        $slider_image = $_POST['slider_image'];

        $image_filename = '';
        $flag_create_image = false;

        // Remove
        if ($slider_image == '' || $slider_image == 'null' || $slider_image == 'Remove') {
            $image_filename = 'Remove';
        } // Update with new slider image
        else {
            $flag_create_image = true;
            $time = date("Ymd_his");
            if ($slider_image_attr == 'slider_image1') {
                $image_filename = 'IMG_Slider_01_' . $time . '.jpg';
            } else if ($slider_image_attr == 'slider_image2') {
                $image_filename = 'IMG_Slider_02_' . $time . '.jpg';
            } else if ($slider_image_attr == 'slider_image3') {
                $image_filename = 'IMG_Slider_03_' . $time . '.jpg';
            }
        }

        include_once("../includes/baker.php");
        $baker = new Baker();

        $res = $baker->updateSliderImage($id, $slider_image_attr, $image_filename);

        $upload_path = '../uploads/images/';

        if ($res == 1) {
            if ($flag_create_image == true) {
                file_put_contents($upload_path . $image_filename, base64_decode($slider_image));
            }
            $mobile_number = $baker->getBaker('id', $id, 'mobile_number');
            $response['status'] = "success";
            $response['response'] = $baker->read($mobile_number, 'created_at');
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no baker corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id, slider_image_attr and slider_image) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>