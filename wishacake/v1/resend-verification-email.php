<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['email'])) {

        // store the values of POST data
        $email = $_POST['email'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        // generates token string
        $token = md5(uniqid(mt_rand()));

        $user->updateToken($email, $token, 'email_token');

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/verify-account?t=' . $token;

        $to = $email;
        $subject = "Verify your account";

        $where = "email";
        $firstName = $user->getUser($where, $email, 'first_name');
        $lastName = $user->getUser($where, $email, 'last_name');

        $body = 'Hi ' . $firstName . ' ' . $lastName . ',
            <br/><br/>You\'re just one step away to get started to wishacake. 
            <br/><br/>To verify your account, click the link below:
            <br/><br/><a href="' . $link . '">' . $link . '</a>
            <br/><br/>Regards,
            <br/>The wishacake Team';

        // set content-type header for sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // additional headers
        $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

        // send email
        mail($to, $subject, $body, $headers);

        $response['status'] = "success";
        $response['response'] = "Verification email has been sent successfully.";
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (email) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>