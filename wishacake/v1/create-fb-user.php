<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['firebase_token']) and
        isset($_POST['first_name']) and
        isset($_POST['last_name']) and
        isset($_POST['email'])) {

        // store the values of POST data
        $firebase_token = $_POST['firebase_token'];
        $fn = $_POST['first_name'];
        $ln = $_POST['last_name'];
        $email = $_POST['email'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        $res = $user->createFbUser($firebase_token, $fn, $ln, $email);

        // check if the user is successfully created
        if ($res == 1) {
            $response['status'] = "success";
            $response['errorCode'] = "";
            $response['response'] = $user->read($email);

            // generates token string
            $token = md5(uniqid(mt_rand()));

            $user->updateToken($email, $token, 'email_token');

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/verify-account?t=' . $token;

            $to = $email;
            $subject = "Verify your account";

            $where = "email";
            $firstName = $user->getUser($where, $email, 'first_name');
            $lastName = $user->getUser($where, $email, 'last_name');

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
                <br/><br/>You\'re just one step away to get started to wishacake. 
                <br/><br/>To verify your account, click the link below:
                <br/><br/><a href="' . $link . '">' . $link . '</a>
                <br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "This email address is associated with a wishacake account.";
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "-2";
            $response['response'] = $user->read($email);
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (first_name, last_name & email) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>