<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id']) and
        isset($_POST['baker_id']) and
        isset($_POST['order_id']) and
        isset($_POST['rating']) and
        isset($_POST['review'])) {

        // store the values of POST data
        $user_id = $_POST['user_id'];
        $baker_id = $_POST['baker_id'];
        $order_id = $_POST['order_id'];
        $rating = $_POST['rating'];
        $review_post = $_POST['review'];

        // include the review.php class file
        include_once("../includes/review.php");

        // create new review object
        $review = new Review();

        $res = $review->create($user_id, $baker_id, $rating, $review_post);

        // check if the review is successfully created
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = $review->read($baker_id);

            // Update order rated status and rating
            include_once("../includes/order.php");
            $order = new Order();
            $order->updateRatedStatusAndOrderRating($order_id, $rating);

            // Update baker's rating
            include_once("../includes/baker.php");
            $baker = new Baker();
            $current_rating = $baker->getBaker('id', $baker_id, 'rating');
            $reviews_count = $review->getReviewsCount($baker_id);

            // calculate new rating and round to 1 decimal place
            $new_rating = round(((($current_rating * $reviews_count) + $rating) / ($reviews_count + 1)), 1);

            // Update rating in bakers table
            $baker->updateRating($baker_id, $new_rating);
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (user_id, baker_id, order_id, rating & review) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>