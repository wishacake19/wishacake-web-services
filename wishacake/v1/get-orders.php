<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include_once("../includes/order.php");
    $order = new Order();

    // check if the required field(s) are not empty
    if (isset($_POST['user_id'])) {

        // store the value of POST data
        $user_id = $_POST['user_id'];

        $res = $order->read($user_id, '', '');

        if ($res == -1) {
            $response['status'] = "error";
            $response['response'] = "There is no user corresponding to this id.";
        } else {
            $response['status'] = "success";
            $response['response'] = $res;
        }
    } else if (isset($_POST['baker_id']) and isset($_POST['status'])) {

        // store the value of POST data
        $baker_id = $_POST['baker_id'];
        $status = $_POST['status'];

        $status = $order->getOrderStatusCodeByStatusString($status);

        $res = $order->read('', $baker_id, $status);

        if ($res == -2) {
            $response['status'] = "error";
            $response['response'] = "There is no baker corresponding to this id.";
        } else {
            $response['status'] = "success";
            $response['response'] = $res;
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter(s) (user_id OR (baker_id and status)) is/are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>