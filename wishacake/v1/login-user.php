<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['firebase_token']) and isset($_POST['email']) and isset($_POST['password'])) {

        // store the values of POST data
        $firebase_token = $_POST['firebase_token'];
        $email = $_POST['email'];
        $pass = $_POST['password'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        $res = $user->login($firebase_token, $email, $pass);

        // check if the login is successful
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = $user->read($email);
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "There is no user corresponding to this email address.";
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "Wrong password. Try again or click Forgot password to reset it.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "-2";
            $response['response'] = "This email address is associated with a Facebook account.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (email & password) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>