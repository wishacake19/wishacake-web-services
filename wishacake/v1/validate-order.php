<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id']) and isset($_POST['baker_id'])) {

        // store the values of POST data
        $user_id = $_POST['user_id'];
        $baker_id = $_POST['baker_id'];

        // include the order.php class file
        include_once("../includes/order.php");

        // create new baker object
        $order = new Order();

        $res = $order->validate($user_id, $baker_id);

        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = "Order request is valid.";
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['response'] = "It looks like you're trying to generate a fake order for yourself. Sorry, we cannot help your proceed further.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (user_id and baker_id) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>