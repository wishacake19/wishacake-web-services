<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['firebase_token']) and
        isset($_POST['mobile_number'])
        and isset($_POST['pin'])) {

        // store the values of POST data
        $firebase_token = $_POST['firebase_token'];
        $mobile_number = $_POST['mobile_number'];
        $pin = $_POST['pin'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->login($firebase_token, $mobile_number, $pin);

        // check if the login is successful
        if ($res == 1) {
            $baker_data = $baker->read($mobile_number, 'created_at');
            // print_r($baker_datx`a);
            $response['status'] = "success";
            $response['response'] = $baker_data;
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "There is no baker corresponding to this mobile number.";
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "Wrong pin.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (firebase_token, mobile_number & pin) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>