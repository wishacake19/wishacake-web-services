<?php

// Server key from Firebase Console
define('FIREBASE_SERVER_KEY', 'AAAAwjZrkPA:APA91bFsEh4r4TIfLD2UwvfPq_Gj-coDuvjAjt8W4wokYInjibdLuZU3RlwZU32NKzBefz-hr-TyeQumK2eNWRPOTNdeB0gPZ1VJjOrP-lkIhGie-JondpYfequNJhOACJbS61TZBUWn');

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id']) and
        isset($_POST['baker_id']) and
        isset($_POST['image1']) and
        isset($_POST['image2']) and
        isset($_POST['image3']) and
        isset($_POST['image4']) and
        isset($_POST['image5']) and
        isset($_POST['image6']) and
        isset($_POST['image7']) and
        isset($_POST['image8']) and
        isset($_POST['image9']) and
        isset($_POST['image10']) and
        isset($_POST['quantity']) and
        isset($_POST['pounds']) and
        isset($_POST['description']) and
        isset($_POST['contact_number']) and
        isset($_POST['delivery_location']) and
        isset($_POST['delivery_address']) and
        isset($_POST['delivery_date']) and
        isset($_POST['delivery_time']) and
        isset($_POST['payment_method']) and
        isset($_POST['card_number']) and
        isset($_POST['expiration_date']) and
        isset($_POST['cvv']) and
        isset($_POST['cardholder_name']) and
        isset($_POST['country'])) {

        // store the values of POST data
        $user_id = $_POST['user_id'];
        $baker_id = $_POST['baker_id'];
        $image1 = $_POST['image1'];
        $image2 = $_POST['image2'];
        $image3 = $_POST['image3'];
        $image4 = $_POST['image4'];
        $image5 = $_POST['image5'];
        $image6 = $_POST['image6'];
        $image7 = $_POST['image7'];
        $image8 = $_POST['image8'];
        $image9 = $_POST['image9'];
        $image10 = $_POST['image10'];
        $quantity = $_POST['quantity'];
        $pounds = $_POST['pounds'];
        $description = $_POST['description'];
        $contact_number = $_POST['contact_number'];
        $delivery_location = $_POST['delivery_location'];
        $delivery_address = $_POST['delivery_address'];
        $delivery_date = $_POST['delivery_date'];
        $delivery_time = $_POST['delivery_time'];
        $payment_method = $_POST['payment_method'];
        $card_number = $_POST['card_number'];
        $expiration_date = $_POST['expiration_date'];
        $cvv = $_POST['cvv'];
        $cardholder_name = $_POST['cardholder_name'];
        $country = $_POST['country'];

        if ($payment_method == 'Cash') {
            $payment_method = '1';
        } else if ($payment_method == 'Card') {
            $payment_method = '2';
        }

        $time = date("Ymd_his");

        // set up images
        $image1_filename = 'IMG_ORDER_01_' . $time . '.jpg';
        $image2_filename = '';
        $image3_filename = '';
        $image4_filename = '';
        $image5_filename = '';
        $image6_filename = '';
        $image7_filename = '';
        $image8_filename = '';
        $image9_filename = '';
        $image10_filename = '';

        if ($image2 == '' || $image2 == 'null') ;
        else {
            $image2_filename = 'IMG_ORDER_02_' . $time . '.jpg';
        }
        if ($image3 == '' || $image3 == 'null') ;
        else {
            $image3_filename = 'IMG_ORDER_03_' . $time . '.jpg';
        }
        if ($image4 == '' || $image4 == 'null') ;
        else {
            $image4_filename = 'IMG_ORDER_04_' . $time . '.jpg';
        }
        if ($image5 == '' || $image5 == 'null') ;
        else {
            $image5_filename = 'IMG_ORDER_05_' . $time . '.jpg';
        }
        if ($image6 == '' || $image6 == 'null') ;
        else {
            $image6_filename = 'IMG_ORDER_06_' . $time . '.jpg';
        }
        if ($image7 == '' || $image7 == 'null') ;
        else {
            $image7_filename = 'IMG_ORDER_07_' . $time . '.jpg';
        }
        if ($image8 == '' || $image8 == 'null') ;
        else {
            $image8_filename = 'IMG_ORDER_08_' . $time . '.jpg';
        }
        if ($image9 == '' || $image9 == 'null') ;
        else {
            $image9_filename = 'IMG_ORDER_09_' . $time . '.jpg';
        }
        if ($image10 == '' || $image10 == 'null') ;
        else {
            $image10_filename = 'IMG_ORDER_10_' . $time . '.jpg';
        }

        include_once("../includes/order.php");
        $order = new Order();

        $res = $order->create($user_id, $baker_id, $image1_filename, $image2_filename, $image3_filename, $image4_filename, $image5_filename, $image6_filename, $image7_filename, $image8_filename, $image9_filename, $image10_filename, $quantity, $pounds, $description, $contact_number, $delivery_location, $delivery_address, $delivery_date, $delivery_time, $payment_method, $card_number, $expiration_date, $cvv, $cardholder_name, $country);

        if ($res == -1) {
            $response['status'] = "error";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        } else {
            $upload_path = '../uploads/images/';

            // create images in folder
            file_put_contents($upload_path . $image1_filename, base64_decode($image1));
            if ($image2 == '' || $image2 == 'null') ;
            else {
                file_put_contents($upload_path . $image2_filename, base64_decode($image2));
            }
            if ($image3 == '' || $image3 == 'null') ;
            else {
                file_put_contents($upload_path . $image3_filename, base64_decode($image3));
            }
            if ($image4 == '' || $image4 == 'null') ;
            else {
                file_put_contents($upload_path . $image4_filename, base64_decode($image4));
            }
            if ($image5 == '' || $image5 == 'null') ;
            else {
                file_put_contents($upload_path . $image5_filename, base64_decode($image5));
            }
            if ($image6 == '' || $image6 == 'null') ;
            else {
                file_put_contents($upload_path . $image6_filename, base64_decode($image6));
            }
            if ($image7 == '' || $image7 == 'null') ;
            else {
                file_put_contents($upload_path . $image7_filename, base64_decode($image7));
            }
            if ($image8 == '' || $image8 == 'null') ;
            else {
                file_put_contents($upload_path . $image8_filename, base64_decode($image8));
            }
            if ($image9 == '' || $image9 == 'null') ;
            else {
                file_put_contents($upload_path . $image9_filename, base64_decode($image9));
            }
            if ($image10 == '' || $image10 == 'null') ;
            else {
                file_put_contents($upload_path . $image10_filename, base64_decode($image10));
            }

            include_once("../includes/user.php");
            $user = new User();
            $user_name = $user->getUser('id', $user_id, 'first_name') . ' ' . $user->getUser('id', $user_id, 'last_name');

            include_once("../includes/baker.php");
            $baker = new Baker();
            $baker_name = $baker->getBaker('id', $baker_id, 'first_name') . ' ' . $baker->getBaker('id', $baker_id, 'last_name');

            $delivery_date = date_create($delivery_date)->format('d M, Y');
            $delivery_time = date_create($delivery_time)->format('h:i A');
            $formatted_delivery_date_time = $delivery_date . ' • ' . $delivery_time;

            $order_id = $res;

            // Push notification to baker
            $firebase_tokens = $baker->getBaker('id', $baker_id, 'firebase_tokens');
            if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                $order_status = $order->getOrder('id', $order_id, 'status');
                $when = $baker->getCurrentTimestamp();

                $request_data = array(
                    'title' => '[' . $formatted_delivery_date_time . '] New order from ' . $user_name,
                    'message' => 'You have received a new order. Tap to view the details',
                    'image' => 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/uploads/images/' . $order->getOrder('id', $order_id, 'image1'),
                    'action' => 'activity',
                    'action_destination' => 'OrdersActivity',
                    'order_status' => $order_status,
                    'when' => $when
                );

                $registration_ids_array = explode(",", $firebase_tokens);

                $fields = array(
                    'registration_ids' => $registration_ids_array,
                    'data' => $request_data
                );

                $headers = array(
                    'Authorization: key=' . FIREBASE_SERVER_KEY,
                    'Content-Type: application/json'
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                curl_exec($ch);
                curl_close($ch);
            }

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // Email notification to baker
            $to = $baker->getBaker('id', $baker_id, 'email');
            $subject = '[' . $formatted_delivery_date_time . '] New order from ' . $user_name;
            $link = 'https://play.google.com/store/apps/details?id=com.android.bakerapp';

            $body = 'Hi ' . $baker_name . ',
				<br/><br/>You have received a new order from ' . $user_name . '.
				<br/>The shipping details for this order are as below:
				<br/><br/>Location: <strong>' . $delivery_location . '</strong>
				<br/>Home address: <strong>' . $delivery_address . '</strong>
				<br/>Date & time: <strong>' . $delivery_date . '</strong> at <strong>' . $delivery_time . '</strong>
				<br/><br/>For further details, log in to your baker account.
				<br/><br/>Here\'s a link to the application:
				<br/><a href="' . $link . '">' . $link . '</a>
				<br/><br/>Regards,
				<br/>The wishacake Team';

            mail($to, $subject, $body, $headers);

            // sms notification to baker
            $api_key = urlencode('dwiFMGAA4hs-SUUwvSLeZ8jnQUVx3PoPmwYSMXgSjM');
            $baker_mobile_number = $baker->getBaker('id', $baker_id, 'mobile_number');
            $numbers = array($baker_mobile_number);
            $sender = urlencode('wishacake');

            $body = "Hi $baker_name,
            \r\nYou have received a new order from $user_name.\nThe shipping details for this order are as below:
            \r\nLocation: $delivery_location
            \nHome address: $delivery_address
            \nDate & time: $delivery_date at $delivery_time
            \nFor further details, log in to your baker account. Here\'s a link to the application:
            \r\n$link
            \r\nRegards,\nThe wishacake Team";

            $message = rawurlencode($body);
            $numbers = implode(',', $numbers);

            $data = array('apikey' => $api_key, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

            $ch = curl_init('https://api.txtlocal.com/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_exec($ch);
            curl_close($ch);

            // Email notification to user
            $order_created_at = $order->getOrder('id', $order_id, 'created_at');
            $date = date_create($order_created_at);
            $formatted_date = $date->format('d M, Y') . ' • ' . $date->format('h:i A');
            $to = $user->getUser('id', $user_id, 'email');
            // $subject = "[ORDER # " . $order_id . "] Order placed successfully [" . $formatted_date . "]";
            $subject = "[" . $formatted_date . "] Order placed successfully";
            $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

            $body = 'Hi ' . $user_name . ',
				<br/><br/>Your order request has been sent to ' . $baker_name . ' and is awaiting approval. You will receive a notification from us once your order request will be accepted or rejected.
				<br/><br/>To view the details of the order, log in to <a href="' . $link . '">' . "wishacake" . '</a>.
				<br/><br/>Regards,
				<br/>The wishacake Team';

            mail($to, $subject, $body, $headers);

            $response['status'] = "success";
            $response['response'] = 'Order request has been sent successfully to baker (id -> ' . $baker_id . ', name -> ' . $baker_name . ').';
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (user_id, baker_id, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, quantity, pounds, description, contact_number, delivery_location, delivery_address, delivery_date, delivery_time and payment_method) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>