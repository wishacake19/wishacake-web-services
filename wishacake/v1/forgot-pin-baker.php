<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['email'])) {

        // store the values of POST data
        $email = $_POST['email'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        // generates token string
        $token = md5(uniqid(mt_rand()));

        $res = $baker->updatePinToken($email, $token);

        // check if token is successfully updated
        if ($res == 1) {
            $response['status'] = "success";
            //  $response['response'] = "Instructions for resetting your PIN have been emailed to you.";
            $response['response'] = "We have sent an email to " . $email . " containing instructions on how to reset your PIN.";

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/wishacake/reset-pin?t=' . $token;

            $to = $email;
            $subject = "Reset your PIN";

            $where = "email";
            $firstName = $baker->getBaker($where, $email, 'first_name');
            $lastName = $baker->getBaker($where, $email, 'last_name');

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
				<br/><br/>Someone recently requested to reset the PIN for your baker account. 
				<br/><br/>To reset your PIN, click the link below:
				<br/><br/><a href="' . $link . '">' . $link . '</a>
				<br/><br/>If you don\'t want to reset your PIN or didn\'t requested for it, just ignore and delete this email.
				<br/>To keep your account secure, please don\'t forward this email to anyone.
				<br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } else if ($res == 0) {
            $response['status'] = "error";
            $response['errorCode'] = "0";
            $response['response'] = "There is no baker corresponding to this email address.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (email) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>