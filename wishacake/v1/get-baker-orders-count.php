<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include_once("../includes/order.php");
    $order = new Order();

    // check if the required field(s) are not empty
    if (isset($_POST['baker_id'])) {

        // store the value of POST data
        $baker_id = $_POST['baker_id'];

        $res_new = $order->getOrdersCount($baker_id, 'New');
        $res_accepted = $order->getOrdersCount($baker_id, 'Accepted');
        $res_rejected = $order->getOrdersCount($baker_id, 'Rejected');
        $res_confirmed = $order->getOrdersCount($baker_id, 'Confirmed');
        $res_cancelled = $order->getOrdersCount($baker_id, 'Cancelled');
        $res_ongoing = $order->getOrdersCount($baker_id, 'Ongoing');
        $res_completed = $order->getOrdersCount($baker_id, 'Completed');

        $result = array(array("New" => $res_new, "Accepted" => $res_accepted,
            "Rejected" => $res_rejected, "Confirmed" => $res_confirmed,
            "Cancelled" => $res_cancelled, "Ongoing" => $res_ongoing,
            "Completed" => $res_completed));

        // if ($res_new == 0) {
        //     $response['status'] = "error";
        //     $response['response'] = "The baker with this id does not have any order yet.";
        // } 
        // else {
        $response['status'] = "success";
        $response['response'] = $result;
        // }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (baker_id) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>