<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and
        isset($_POST['account_status'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $account_status = $_POST['account_status'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        $res = $user->deactivateReactivate($id, $account_status);

        // check if the user's account is successfully deactivated
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = 'To reactivate your account, log in using your wishacake\'s email addres and password. We hope you come back soon.';

            $to = $user->getUser('id', $id, 'email');
            $subject = "Account deactivated";

            $firstName = $user->getUser('id', $id, 'first_name');
            $lastName = $user->getUser('id', $id, 'last_name');

            $link = 'https://play.google.com/store/apps/details?id=com.android.wishacake';

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
				<br/><br/>Your account has been deactivated successfully. 
				<br/><br/>To reactivate your account, log in to <a href="' . $link . '">' . "wishacake" . '</a> using your wishacake\'s email address and password. We hope you come back soon.
				<br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } // check if the user's account is successfully reactivated
        else if ($res == 0) {
            $email = $user->getUser('id', $id, 'email');
            $response['status'] = "success";
            $response['response'] = $user->read($email);
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no user corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id & account_status) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>