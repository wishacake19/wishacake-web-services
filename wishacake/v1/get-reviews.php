<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['baker_id'])) {

        // store the value of POST data
        $baker_id = $_POST['baker_id'];

        // include the review.php class file
        include_once("../includes/review.php");

        // create new review object
        $review = new Review();

        $result = $review->read($baker_id);

        // print_r($result);

        $response['status'] = "success";
        $response['response'] = $result;
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (baker_id) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>