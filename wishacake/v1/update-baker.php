<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and
        isset($_POST['image']) and
        isset($_POST['first_name']) and
        isset($_POST['last_name']) and
        isset($_POST['email']) and
        isset($_POST['mobile_number']) and
        isset($_POST['location_id']) and
        isset($_POST['location_name']) and
        isset($_POST['location_address']) and
        isset($_POST['location_latitude']) and
        isset($_POST['location_longitude'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $image = $_POST['image'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $mobile_number = $_POST['mobile_number'];
        $location_id = $_POST['location_id'];
        $location_name = $_POST['location_name'];
        $location_address = $_POST['location_address'];
        $location_latitude = $_POST['location_latitude'];
        $location_longitude = $_POST['location_longitude'];

        $image_filename = '';
        $flag_create_image = false;

        // Update with default profile photo
        if ($image == '' || $image == 'null') {
            $image_filename = 'null';
        } // Remove photo
        else if ($image == 'Remove') {
            $image_filename = 'Remove';
        } // Update with existing profile photo
        else if (strpos($image, 'IMG_Profile_Photo') !== false) {
            $image_filename = $image;
        } // Update with new profile photo
        else {
            $flag_create_image = true;
            $time = date("Ymd_his");
            $image_filename = 'IMG_Profile_Photo_' . $time . '.jpg';
        }

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->update($id, $image_filename, $first_name, $last_name, $email, $mobile_number, $location_id, $location_name, $location_address, $location_latitude, $location_longitude);

        $upload_path = '../uploads/images/';

        // check if the baker is successfully updated
        if ($res == 1) {
            if ($flag_create_image == true) {
                file_put_contents($upload_path . $image_filename, base64_decode($image));
            }

            $response['status'] = "success";
            $response['response'] = $baker->read($mobile_number, 'created_at');
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no baker corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "-2";
            $response['response'] = "The email address is already in use by another baker.";
        } else if ($res == -3) {
            $response['status'] = "error";
            $response['errorCode'] = "-3";
            $response['response'] = "The mobile number is already in use by another baker.";
        } else if ($res == -4) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id, image, first_name, last_name, email, mobile_number, location_id, location_name, location_address, location_latitude & location_longitude) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>