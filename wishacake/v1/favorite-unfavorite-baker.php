<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id']) and isset($_POST['favorite_id']) and isset($_POST['favorite_status'])) {

        // store the value of POST data
        $user_id = $_POST['user_id'];
        $favorite_id = $_POST['favorite_id'];
        $favorite_status = $_POST['favorite_status'];

        if ($favorite_id == '' || $favorite_id == 'null') {
            $response['status'] = "error";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        } else {
            // include the baker.php class file
            include_once("../includes/baker.php");

            // create new baker object
            $baker = new Baker();

            $res = $baker->updateFavoriteStatus($user_id, $favorite_id, $favorite_status);

            if ($res == 1) {
                $response['status'] = "success";
                if ($favorite_status == 'Favorite' || $favorite_status == 'true' || $favorite_status == '1') {
                    $response['response'] = "Requested baker has been ADDED to your favorite bakers.";
                } else if ($favorite_status == 'Unfavorite' || $favorite_status == 'false' || $favorite_status == '0') {
                    $response['response'] = "Requested baker has been REMOVED from your favorite bakers.";
                }
            } else if ($res == -1) {
                $response['status'] = "error";
                $response['response'] = "Sorry, something went wrong. Please try again.";
            }
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (user_id, favorite_id and favorite_status) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>