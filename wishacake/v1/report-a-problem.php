<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id']) and
        isset($_POST['baker_id']) and
        isset($_POST['description']) and
        isset($_POST['image1']) and
        isset($_POST['image2']) and
        isset($_POST['image3'])) {

        // store the values of POST data
        $user_id = $_POST['user_id'];
        $baker_id = $_POST['baker_id'];
        $description = $_POST['description'];
        $image1 = $_POST['image1'];
        $image2 = $_POST['image2'];
        $image3 = $_POST['image3'];

        $image1_filename = '';
        $image2_filename = '';
        $image3_filename = '';

        $time = date("Ymd_his");

        if ($image1 == '' || $image1 == 'null') ;
        else {
            $image1_filename = 'IMG_COMPLAINT_01_' . $time . '.jpg';
        }
        if ($image2 == '' || $image2 == 'null') ;
        else {
            $image2_filename = 'IMG_COMPLAINT_02_' . $time . '.jpg';
        }
        if ($image3 == '' || $image3 == 'null') ;
        else {
            $image3_filename = 'IMG_COMPLAINT_03_' . $time . '.jpg';
        }

        include_once("../includes/complaint.php");
        $complaint = new Complaint();

        $res = $complaint->create($user_id, $baker_id, $description, $image1_filename, $image2_filename, $image3_filename);

        if ($res == 1) {
            $upload_path = '../uploads/images/';

            // create images in folder
            if ($image1 == '' || $image1 == 'null') ;
            else {
                file_put_contents($upload_path . $image1_filename, base64_decode($image1));
            }
            if ($image2 == '' || $image2 == 'null') ;
            else {
                file_put_contents($upload_path . $image2_filename, base64_decode($image2));
            }
            if ($image3 == '' || $image3 == 'null') ;
            else {
                file_put_contents($upload_path . $image3_filename, base64_decode($image3));
            }

            $response['status'] = "success";
            $response['response'] = "Complaint submitted successfully.";
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (user_id, baker_id, description, image1, image2 and image3) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>