<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and
        isset($_POST['first_name']) and
        isset($_POST['last_name']) and
        isset($_POST['email']) and
        isset($_POST['mobile_number'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $fn = $_POST['first_name'];
        $ln = $_POST['last_name'];
        $email = $_POST['email'];
        $mobile_number = $_POST['mobile_number'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        $res = $user->update($id, $fn, $ln, $email, $mobile_number);

        // check if the user is successfully updated
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = $user->read($email);
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no user corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "-2";
            $response['response'] = "The email address is already in use by another account.";
        } else if ($res == -3) {
            $response['status'] = "error";
            $response['errorCode'] = "-3";
            $response['response'] = "The mobile number is already in use by another account.";
        } else if ($res == -4) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id, first_name, last_name, email & mobile_number) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>