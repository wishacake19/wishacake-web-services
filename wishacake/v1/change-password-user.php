<?php

// array for JSON response
$response = array();

// check if the http request method is correct i.e. POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['id']) and
        isset($_POST['password'])) {

        // store the values of POST data
        $id = $_POST['id'];
        $password = $_POST['password'];

        // include the user.php class file
        include_once("../includes/user.php");

        // create new user object
        $user = new User();

        $res = $user->changePassword($id, $password);

        $email = $user->getUser('id', $id, 'email');

        // check if the user's password is successfully changed
        if ($res == 1) {
            $response['status'] = "success";
            $response['response'] = $user->read($email);;

            $to = $email;
            $subject = "Password changed successfully";

            $firstName = $user->getUser('id', $id, 'first_name');
            $lastName = $user->getUser('id', $id, 'last_name');

            $body = 'Hi ' . $firstName . ' ' . $lastName . ',
				<br/><br/>Your password has been changed successfully. 
				<br/><br/>Regards,
                <br/>The wishacake Team';

            // set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // additional headers
            $headers .= 'From: wishacake<wishacake@feedpakistan.com>' . "\r\n";

            // send email
            mail($to, $subject, $body, $headers);
        } else if ($res == -1) {
            $response['status'] = "error";
            $response['errorCode'] = "-1";
            $response['response'] = "There is no user corresponding to the provided id.";
        } else if ($res == -2) {
            $response['status'] = "error";
            $response['errorCode'] = "";
            $response['response'] = "Sorry, something went wrong. Please try again.";
        }
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameters (id & password) are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>