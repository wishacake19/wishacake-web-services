<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // check if the required field(s) are not empty
    if (isset($_POST['user_id'])) {

        // store the value of POST data
        $user_id = $_POST['user_id'];

        // include the baker.php class file
        include_once("../includes/baker.php");

        // create new baker object
        $baker = new Baker();

        $res = $baker->getFavoriteBakers($user_id);

        $response['status'] = "success";
        $response['response'] = $res;
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter (user_id) is missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>