<?php

// array for JSON response
$response = array();

// check if the http request method is POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // include the baker.php class file
    include_once("../includes/baker.php");

    // create new baker object
    $baker = new Baker();

    // check if the required field(s) are not empty
    if (isset($_POST['mobile_number'])) {

        // store the value of POST data
        $mobile_number = $_POST['mobile_number'];

        $res = $baker->read($mobile_number, 'created_at');

        if ($res == 0) {
            $response['status'] = "error";
            $response['response'] = "There is no baker corresponding to this mobile number.";
        } else {
            $response['status'] = "success";
            $response['response'] = $res;
        }
    } else if (isset($_POST['user_id']) and isset($_POST['order_by'])) {

        // store the value of POST data
        $user_id = $_POST['user_id'];
        $order_by = $_POST['order_by'];

        $bakers = $baker->read('', $order_by);

        // $favorite_bakers = $baker->getFavoriteBakers($user_id);
        // $result_array = array_values(array_unique(array_merge($bakers, $favorite_bakers), SORT_REGULAR));

        $result_array = array_values($bakers);

        $response['status'] = "success";
        $response['response'] = $result_array;
    } else {
        $response['status'] = "error";
        $response['response'] = "Required parameter(s) (mobile_number OR (user_id and order_by)) is/are missing.";
    }
} else {
    $response['status'] = "error";
    $response['response'] = "HTTP request method (POST) is missing.";
}

echo json_encode($response);

?>