<?php include "includes/header.php"; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <img class="logo-mini" src="dist/img/ic_launcher.png"
                 style="height: 50px; width: 50px; padding: 2px 2px 2px 2px;">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>wishacake</b></span>
        </a>

        <!-- Header Navbar -->
        <?php include "includes/navigation.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include "includes/sidenav.php"; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Featured cakes
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="col-lg-12">

                    <?php
                    if (isset($_GET['source'])) {
                        $source = $_GET['source'];
                    } else {
                        $source = '';
                    }
                    switch ($source) {
                        case 'add_featured_cakes':
                            include "includes/add_featured_cakes.php";
                            break;
                        case 'edit_featured_cake':
                            include "includes/edit_featured_cakes.php";
                            break;
                        default:
                            include "includes/view_all_featured_cakes.php";
                            break;
                    }
                    ?>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
</body>
</html>