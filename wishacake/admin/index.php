<?php include "includes/header.php"; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <img class="logo-mini" src="dist/img/ic_launcher.png"
                 style="height: 50px; width: 50px; padding: 2px 2px 2px 2px;">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>wishacake</b></span>
        </a>

        <!-- Header Navbar -->
        <?php include "includes/navigation.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include "includes/sidenav.php"; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="font-weight: 500;">
                Users
            </h1><br>
            <?php include "includes/user_dashboard_section.php"; ?>
            <hr style="background-color: #9b9b9b; height: 1px;">
            <h1 style="font-weight: 500;">
                Bakers
            </h1><br>
            <?php include "includes/baker_dashboard_section.php"; ?>
            <hr style="background-color: #9b9b9b; height: 1px;">
            <h1 style="font-weight: 500;">
                Orders
            </h1><br>
            <?php include "includes/order_dashboard_section.php"; ?>
        </section>


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
</body>
</html>