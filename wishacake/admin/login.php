<?php ob_start(); ?>
<?php session_start(); ?>
<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>
<?php include "functions.php"; ?>
<?php
checkIfUserIsLoggedInAndRedirect('/wishacake/admin');

if (ifItISMethod('post')) {
    $error = [
        'email' => '',
        'password' => '',
        'message' => ''
    ];
    $email = $_POST['admin_email'];
    $password = $_POST['admin_password'];
    if ($email == '') {
        $error['email'] = "<span style='color: #FF0000; padding-right: 60px;'>* Email address is required</span>";
    }
    if ($password == '') {
        $error['password'] = "<span style='color: #FF0000; padding-right: 85px;'>* Password is required</span>";
    }
    if ($email != '' && $password != '') {
        if (login_user($email, $password)) {

        } else {
            $error['message'] = "<h4 class='text-center' style='color: #FF0000;'>Incorrect email address or password</h4>";
        }
    }
}

?>

<!-- Page Content -->
<body style="background-color: #3c8dbc;">
<div id="responsive" class="container">

    <div class="form-gap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="text-center">


                            <img class="logo-mini" src="dist/img/ic_launcher.png"
                                 style="height: 100px; width: 100px; padding: 2px 2px 2px 2px;">
                            <h3 class="text-center">Login to dashboard</h3>
                            <br>
                            <?php echo isset($error['message']) ? $error['message'] : '' ?>
                            <div class="panel-body">

                                <form action="" id="login-form" role="form" autocomplete="off" class="form"
                                      method="post">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-envelope color-blue"></i></span>

                                            <input name="admin_email" type="email" class="form-control"
                                                   placeholder="Enter your email address">
                                        </div>
                                        <?php echo isset($error['email']) ? $error['email'] : '' ?>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-lock color-blue"></i></span>
                                            <input name="admin_password" type="password" class="form-control"
                                                   placeholder="Enter your password">
                                        </div>
                                        <?php echo isset($error['password']) ? $error['password'] : '' ?>
                                    </div>


                                    <input name="login" style="margin-top: 32px;" class="btn btn-lg btn-primary btn-block" value="Login"
                                           type="submit">

                                </form>

                            </div><!-- Body-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- /.container -->
</body>