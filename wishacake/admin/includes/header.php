<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>wishacake | Admin Dashboard</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="dist/img/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Theme style -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        #load-screen {
            background: url(dist/img/header-back.png);
            position: fixed;
            z-index: 10000;
            top: 0px;
            width: 100%;
            height: 1600px;

        }

        @media only screen and (max-width: 450px)  {
            #loading {
                width: 350px;
                height: 500px;
                margin: 10% auto;
                background: url(dist/img/loader.gif);
                background-size: 40%;
                background-repeat: no-repeat;
                background-position: center;

            }
        }
        @media only screen and (min-width: 451px)  {
            #loading {
                width: 400px;
                height: 400px;
                margin: 10% auto;
                background: url(dist/img/loader.gif);
                background-size: 40%;
                background-repeat: no-repeat;
                background-position: center;

            }
        }

        @media only screen and (max-width: 450px)  {
            #responsive {
                margin-top: 120px;
            }
        }
        @media only screen and (min-width: 451px)  {
            #responsive {
                margin-top: 170px;
            }
        }
    </style>
</head>

