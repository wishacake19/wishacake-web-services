<?php
$status = '';
$user = '';
$msg = 'All users';
if (isset($_GET['active'])) {
    $status = $_GET['active'];
    $msg = 'Active users';
}
if (isset($_GET['blocked'])) {
    $status = $_GET['blocked'];
    $msg = 'Blocked users';
}
if (isset($_GET['user_id'])) {
    $user = $_GET['user_id'];
    $msg = 'User';
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="box-title"><?php echo $msg; ?></h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email address</th>
                    <th>Mobile number</th>
                    <th>Account status</th>
                    <th>Created at</th>
                    <th>Last modified</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($status == '1' || $status == '0') {
                    $query = "SELECT * FROM users WHERE account_status = $status ORDER BY created_at DESC";
                } else if (isset($_GET['user_id'])) {
                    $query = "SELECT * FROM users WHERE id = $user";
                } else {
                    $query = "SELECT * FROM users ORDER BY created_at DESC";
                }

                $select_users = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_users)) {
                    $user_id = $row['id'];
                    $first_name = $row['first_name'];
                    $last_name = $row['last_name'];
                    $email = $row['email'];
                    $no = $row['mobile_number'];
                    $status = $row['account_status'];
                    $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');
                    $last_modified = $row['modified_at'];
                    if ($last_modified != '0000-00-00 00:00:00') {
                        $last_modified = date_create($row['modified_at'])->format('d M, Y') . ' • ' . date_create($row['modified_at'])->format('h:i A');;
                    } else {
                        $last_modified = "";
                    }

                    if ($no == '' || $no == 'null') {
                        $no = "";
                    }

                    if ($status == '1') {
                        $status = 'Active';
                    } else if ($status == '0') {
                        $status = 'Blocked';
                    }

                    echo "<tr>";
                    echo "<td>$user_id</td>";
                    echo "<td>$first_name</td>";
                    echo "<td>$last_name</td>";
                    echo "<td>$email</td>";
                    echo "<td>$no</td>";
                    echo "<td>$status</td>";
                    echo "<td>$created_at</td>";
                    echo "<td>$last_modified</td>";
                    ?>
                    <td>
                        <?php
                        echo "<a style='margin-right: 5px;' class='btn btn-info' href = 'users.php?source=edit_user&edit_user=$user_id'>Edit</a>";
                        if ($status == 'Active') {
                            echo "<a style='width: 80px;' class='btn btn-danger' href = 'users.php?block=$user_id'>Block</a>";
                        } else if ($status == 'Blocked') {
                            echo "<a style='width: 80px;' class='btn btn-success' href = 'users.php?unblock=$user_id'>Unblock</a>";
                        }
                        ?>
                    </td>
                    <?php
                    echo "</tr>";
                }
                ?>

                </tbody>

            </table>
            <?php
            if (isset($_GET['block'])) {
                $the_user_id = $_GET['block'];
                $query = "UPDATE users SET account_status = '0', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_user_id}";
                $change_status_to_block = mysqli_query($connection, $query);
                header("Location: users.php");
            }
            if (isset($_GET['unblock'])) {
                $the_user_id = $_GET['unblock'];
                $query = "UPDATE users SET account_status = '1', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_user_id}";
                $change_status_to_unblock = mysqli_query($connection, $query);
                header("Location: users.php");
            }
            ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->