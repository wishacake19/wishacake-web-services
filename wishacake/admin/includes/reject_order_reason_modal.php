<div id="reject<?php echo $order_id; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Rejected reason</h3>
            </div>
            <div class="modal-body">
                <?php
                $reason = '';
                $id = $order_id;
                $query = "SELECT rejected_reason FROM orders WHERE id = $id";
                $select_rejected_reason = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_rejected_reason)) {
                    $reason = $row['rejected_reason'];
                }
                echo $reason;
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
