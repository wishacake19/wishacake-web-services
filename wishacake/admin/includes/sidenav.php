<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <?php
        $index_class = '';
        $index = 'index.php';
        $user_class = '';
        $user = "users.php";
        $baker_class = '';
        $baker = "bakers.php";
        $featured_cake_class = '';
        $featured_cake = "featured_cakes.php";
        $order_class = '';
        $order = "orders.php";
        $complaint_class = '';
        $complaint = "complaints.php";
        $pageName = basename($_SERVER['PHP_SELF']);

        if ($pageName == $index) {
            $index_class = 'active';
        } else if ($pageName == $user) {
            $user_class = 'active';
        } else if ($pageName == $baker) {
            $baker_class = 'active';
        } else if ($pageName == $featured_cake) {
            $featured_cake_class = 'active';
        } else if ($pageName == $order) {
            $order_class = 'active';
        } else if ($pageName == $complaint) {
            $complaint_class = 'active';
        }

        ?>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php echo $index_class; ?>"><a href="index.php"><i class="fa fa-home"></i><span>Dashboard</span></a>
            </li>
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview <?php echo $user_class; ?>">
                <a href="#"><i class="fa fa-user-circle"></i> <span>Users</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="users.php?source=add_user"><span style="padding-left: 20px;"
                                                                  class="fa fa-circle-o"></span><span
                                    style="padding-left: 20px;">Add new user</span></a></li>
                    <li><a href="./users.php"><span style="padding-left: 20px;" class="fa fa-circle-o"></span><span
                                    style="padding-left: 20px;">View all users</span></a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $baker_class; ?>">
                <a href="#"><i class="fa fa-user"></i> <span>Bakers</span><span class="pull-right-container"><i
                                class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="bakers.php?source=add_baker"><span style="padding-left: 20px;"
                                                                    class="fa fa-circle-o"></span><span
                                    style="padding-left: 20px;">Add new baker</span></a></li>
                    <li><a href="./bakers.php"><span style="padding-left: 20px;" class="fa fa-circle-o"></span><span
                                    style="padding-left: 20px;">View all bakers</span></a></li>
                    <li class="treeview">
                        <a href="#"><span style="padding-left: 20px;" class="fa fa-circle-o"></span><span
                                    style="padding-left: 20px;">Requests</span>
                            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li style="margin-left: 20px;"><a href="bakers.php?source=new_bakers"><i
                                            class="fa fa-circle-o"></i> New / Pending</a></li>
                            <li style="margin-left: 20px;"><a href="bakers.php?source=approve_bakers"><i
                                            class="fa fa-circle-o"></i> Active / Approved</a></li>
                            <li style="margin-left: 20px;"><a href="bakers.php?source=block_bakers"><i
                                            class="fa fa-circle-o"></i> Blocked</a></li>
                            <li style="margin-left: 20px;"><a href="bakers.php?source=reject_bakers"><i
                                            class="fa fa-circle-o"></i> Rejected</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo $featured_cake_class; ?>">
                <a href="#"><i class="fa fa-birthday-cake"></i> <span>Featured cakes</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="featured_cakes.php?source=add_featured_cakes"><span style="padding-left: 15px;"
                                                                                     class="fa fa-circle-o"></span><span
                                    style="padding-left: 15px;">Add new cake</span></a></li>
                    <li><a href="./featured_cakes.php"><span style="padding-left: 15px;"
                                                             class="fa fa-circle-o"></span><span
                                    style="padding-left: 15px;">View all cakes</span></a></li>
                </ul>
            </li>
            <li class="<?php echo $order_class; ?>"><a href="orders.php"><i class="fa fa-shopping-cart"
                                                                            style="font-size: 15px;"></i><span
                            style="padding-left: 4px;">Orders</span></a></li>
            <li class="<?php echo $complaint_class; ?>"><a href="complaints.php"><i
                            class="fa fa-exclamation-triangle"></i><span
                            style="padding-left: 4px;">Complaints</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>