<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #34b7f1;">
            <div class="inner">
                <h3><?php echo totalUsers(); ?></h3>

                <p class="info-box-text">Total Users</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-circle"></i>
            </div>
            <a href="users.php" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->

</div>
<!-- /.row -->

<!-- =========================================================== -->
<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #4aae20;">
            <div class="inner">
                <h3><?php echo totalActiveUsers(); ?></h3>

                <p class="info-box-text">Active Users</p>
            </div>
            <div class="icon">
                <i class="fa fa-check-circle-o"></i>
            </div>
            <a href="users.php?active=1" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #FF0000;">
            <div class="inner">
                <h3><?php echo totalBlockedUsers(); ?></h3>

                <p class="info-box-text">Blocked Users</p>
            </div>
            <div class="icon">
                <i class="fa fa-ban"></i>
            </div>
            <a href="users.php?blocked=0" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
      
    