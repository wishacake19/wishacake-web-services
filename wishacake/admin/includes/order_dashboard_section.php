<!-- DONUT CHART -->
<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div class="box-body">
            <canvas id="pieChart"></canvas>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="col-lg-6 col-xs-12">
        <br><br><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #34b7f1; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Total</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #f8cc07; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">New / Pending</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #4aae20; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Accepted</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #d53732; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Rejected</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #f06292; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Confirmed</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #797979; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Cancelled</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #ff9300; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Ongoing</span><br>
        <i class="fa fa-circle" style="padding-right: 25px; color: #0076ba; font-size: 20px;"></i><span
                style="color: #000000; font-size: 18px">Completed</span>


    </div>
</div>
<br><br><br>

