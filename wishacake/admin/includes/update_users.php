<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "wishacake";

foreach ($db as $key => $value) {
    define(strtoupper($key), $value);
}
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$connection) {
    die("Database connection Failed" . mysqli_error($connection));
}
$the_user_id = $_GET['id'];
$user_firstname = $_GET['user_firstname'];
$user_lastname = $_GET['user_lastname'];
$user_email = $_GET['user_email'];
$user_password = $_GET['user_password'];
$user_number = $_GET['code'] . $_GET['number'];
$error = [
    'firstname' => '',
    'lastname' => '',
    'email' => '',
    'email_already' => '',
    'no' => '',
    'no_already' => '',
    'no_length' => '',
    'password' => '',
    'password_length' => '',
    'message' => ''
];
if ($user_firstname == "") {
    $error['firstname'] = "true";

}
if ($user_lastname == "") {
    $error['lastname'] = "true";

}
if ($user_email == "") {
    $error['email'] = "true";
} else if (usermail_exists_update($user_email, $the_user_id)) {
    $error['email_already'] = "true";
}
if ($_GET['number'] == "") {
    $error['no'] = "true";
} else if (strlen($_GET['number']) < 10) {
    $error['no_length'] = "true";
} else if (userphone_exists_update($user_number, $the_user_id)) {
    $error['no_already'] = "true";
}
if ($user_password == "") {
    $error['password'] = "true";
} else if (strlen($user_password) < 8) {
    $error['password_length'] = "true";
}
if ($user_firstname != '' && $user_lastname != '' && $user_email != '' && usermail_exists_update($user_email, $the_user_id) == false && $_GET['number'] != '' && userphone_exists_update($user_number, $the_user_id) == false && $user_password != '' && strlen($user_password) >= 8 && strlen($_GET['number']) == 10) {
    $query = "UPDATE users SET first_name = '{$user_firstname}', last_name = '{$user_lastname}', mobile_number = '{$user_number}', email = '{$user_email}', password = '{$user_password}', modified_at = CURRENT_TIMESTAMP() WHERE id = $the_user_id";;
    $update_user_query = mysqli_query($connection, $query);
    //confirmQuery($update_user_query);
    $error['message'] = "true";
}
function usermail_exists_update($email, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT email from users WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($email == $row['email']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT email from users WHERE email = '$email'";
        $result = mysqli_query($connection, $query);
        //confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function userphone_exists_update($phone, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT mobile_number from users WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($phone == $row['mobile_number']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT mobile_number from users WHERE mobile_number = '$phone'";
        $result = mysqli_query($connection, $query);
        //confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

echo json_encode($error);

?>