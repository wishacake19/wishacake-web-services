<div class="box">
    <div class="box-header">
        <h2 class="box-title">All complaints</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Complaint #</th>
                    <th>Submitted by (User)</th>
                    <th>Submitted against (Baker)</th>
                    <th>Description</th>
                    <th>Image 1</th>
                    <th>Image 2</th>
                    <th>Image 3</th>
                    <th>Created at</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $query = "SELECT c.id, u.id AS user_id, b.id AS baker_id, CONCAT(u.first_name, ' ', u.last_name) AS user_name, CONCAT(b.first_name, ' ', b.last_name) AS baker_name, c.image1, c.image2, c.image3, c.description, c.created_at FROM complaints c, users u, bakers b WHERE c.baker_id = b.id AND c.user_id = u.id ORDER BY c.created_at DESC";
                $select_complaints = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_complaints)) {
                    $complaint_id = $row['id'];
                    $user_id = $row['user_id'];
                    $baker_id = $row['baker_id'];
                    $user_name = $row['user_name'];
                    $baker_name = $row['baker_name'];
                    $description = $row['description'];
                    $image1 = $row['image1'];
                    $image2 = $row['image2'];
                    $image3 = $row['image3'];
                    if ($image1 == '') {
                        $img1Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image1 != '') {
                        $img1Src = '../uploads/images/' . $image2;
                    }
                    if ($image2 == '') {
                        $img2Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image2 != '') {
                        $img2Src = '../uploads/images/' . $image2;
                    }

                    if ($image3 == '') {
                        $img3Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image3 != '') {
                        $img3Src = '../uploads/images/' . $image3;
                    }
                    $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');


                    echo "<tr>";
                    echo "<td>$complaint_id</td>";
                    echo "<td><a href='users.php?user_id=$user_id'>$user_name</a></td>";
                    echo "<td><a href='bakers.php?baker_id=$baker_id'>$baker_name</a></td>";
                    echo "<td>$description</td>";
                    ?>
                    <td>
                        <img width="100" height="100" src="<?php echo $img1Src; ?>">
                    </td>
                    <td>
                        <img width="100" height="100" src="<?php echo $img2Src; ?>">
                    </td>
                    <td>
                        <img width="100" height="100" src="<?php echo $img3Src; ?>">
                    </td>
                    <?php
                    echo "<td>$created_at</td>";

                    ?>

                    <?php
                    echo "</tr>";
                }
                ?>

                </tbody>

            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->