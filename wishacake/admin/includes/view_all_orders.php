<section>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #dd5080;"><i class="fa fa-shopping-cart"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total orders</span>
                    <span class="info-box-number"><?php echo totalOrders(); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #34b7f1;"><i class="fa fa-dollar"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total company earning</span>
                    <span class="info-box-number"><?php echo totalCompanyEarning(); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->

    <!-- =========================================================== -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #f8cc07;"><i class="fa fa-question"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">New/Pending orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(1); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #4aae20;"><i class="fa fa-check"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Accepted orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(2); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #d53732;"><i class="fa fa-close"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Rejected orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(3); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #f06292;"><i class="fa fa-check-square-o"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Confirmed orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(4); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>

    <!-- /.row -->

    <!-- =========================================================== -->

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #797979;"><i class="fa fa-window-close-o"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Cancelled orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(5); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #ff9300;"><i class="fa fa-arrow-right"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Ongoing orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(6); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box" style="box-shadow: 5px 10px 8px #888888;">
                <span class="info-box-icon" style="background-color: #0076ba;"><i class="fa fa-check-circle"
                                                                                  style="color: #ffffff"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Completed orders</span>
                    <span class="info-box-number"><?php echo orderStatusCount(7); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->

    <!-- =========================================================== -->
</section>
<br>
<div class="box">
    <div class="box-header">
        <h2 class="box-title">All orders</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Order #</th>
                    <th>User</th>
                    <th>Baker</th>
                    <th>Quantity</th>
                    <th>Pounds</th>
                    <th>Description</th>
                    <th>Delivery location</th>
                    <th>Delivery address</th>
                    <th>Delivery date & time</th>
                    <th>Payment method</th>
                    <th>Card number</th>
                    <th>Expiration date</th>
                    <th>CVV</th>
                    <th>Cardholder name</th>
                    <th>Country</th>
                    <th>Status</th>
                    <th>Rating</th>
                    <th>Subtotal</th>
                    <th>Delivery charges</th>
                    <th>Total amount</th>
                    <th>Baker earning</th>
                    <th>Company earning</th>
                    <th>Created at</th>
                    <th>Last modified</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $query = "SELECT o.id, u.id AS user_id, b.id AS baker_id, CONCAT(u.first_name, ' ', u.last_name) AS user_name, CONCAT(b.first_name, ' ', b.last_name) AS baker_name, o.image1, o.image2, o.image3, o.image4, o.image5, o.image6, o.image7, o.image8, o.image9, o.image10, o.quantity, o.pounds, o.description, o.delivery_location, o.delivery_address, o.delivery_date, o.delivery_time, o.payment_method, o.card_number, o.expiration_date, o.cvv, o.cardholder_name, o.country, o.status, o.rating, o.subtotal, o.delivery_charges, o.total_amount, o.baker_earning, o.company_earning, o.created_at, o.modified_at FROM orders o, users u, bakers b WHERE o.baker_id = b.id AND o.user_id = u.id ORDER BY o.created_at DESC";
                $select_bakers = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_bakers)) {
                    $order_id = $row['id'];
                    $user_id = $row['user_id'];
                    $baker_id = $row['baker_id'];
                    $user_name = $row['user_name'];
                    $baker_name = $row['baker_name'];
                    $quantity = $row['quantity'];
                    $pounds = $row['pounds'];
                    $description = $row['description'];
                    $delivery_location = $row['delivery_location'];
                    $delivery_address = $row['delivery_address'];
                    $delivery_date_time = date_create($row['delivery_date'])->format('d M, Y') . ' • ' . date_create($row['delivery_time'])->format('h:i A');
                    $payment_method = $row['payment_method'];
                    $card_number = $row['card_number'];
                    $expiration_date = $row['expiration_date'];
                    $cvv = $row['cvv'];
                    $cardholder_name = $row['cardholder_name'];
                    $country = $row['country'];
                    $status = $row['status'];
                    $rating = $row['rating'];
                    $subtotal = $row['subtotal'];
                    $delivery_charges = $row['delivery_charges'];
                    $total_amount = $row['total_amount'];
                    $baker_earning = $row['baker_earning'];
                    $company_earning = $row['company_earning'];
                    $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');
                    $last_modified = $row['modified_at'];
                    if ($last_modified != '0000-00-00 00:00:00') {
                        $last_modified = date_create($row['modified_at'])->format('d M, Y') . ' • ' . date_create($row['modified_at'])->format('h:i A');;
                    } else {
                        $last_modified = "";
                    }

                    if ($status == '1') {
                        $status = 'Pending';
                    } else if ($status == '2') {
                        $status = 'Accepted';
                    } else if ($status == '3') {
                        $status = 'Rejected';
                    } else if ($status == '4') {
                        $status = 'Confirmed';
                    } else if ($status == '5') {
                        $status = 'Cancelled';
                    } else if ($status == '6') {
                        $status = 'Ongoing';
                    } else if ($status == '7') {
                        $status = 'Completed';
                    }
                    if ($payment_method == '1') {
                        $payment_method = '../uploads/images/cash.svg';
                    } else if ($payment_method == '2') {
                        $payment_method = '../uploads/images/card.svg';
                    }

                    echo "<tr>";
                    echo "<td>$order_id</td>";
                    echo "<td><a href='orders.php?source=user&user_id=$user_id'>$user_name</a></td>";
                    echo "<td><a href='orders.php?source=baker&baker_id=$baker_id'>$baker_name</a></td>";
                    echo "<td>$quantity</td>";
                    echo "<td>$pounds</td>";
                    echo "<td>$description</td>";
                    echo "<td>$delivery_location</td>";
                    echo "<td>$delivery_address</td>";
                    echo "<td>$delivery_date_time</td>";
                    echo "<td><img src='$payment_method' width = '50' height = '50'></td>";
                    echo "<td>$card_number</td>";
                    echo "<td>$expiration_date</td>";
                    echo "<td>$cvv</td>";
                    echo "<td>$cardholder_name</td>";
                    echo "<td>$country</td>";
                    echo "<td>$status</td>";
                    echo "<td>$rating</td>";
                    echo "<td>$subtotal</td>";
                    echo "<td>$delivery_charges</td>";
                    echo "<td>$total_amount</td>";
                    echo "<td>$baker_earning</td>";
                    echo "<td>$company_earning</td>";
                    echo "<td>$created_at</td>";
                    echo "<td>$last_modified</td>";
                    ?>
                    <td>
                        <?php
                        if ($status != 'Rejected') {
                            ?>
                            <button style="margin-bottom: 10px;" type='button' class='btn btn-primary'
                                    data-toggle='modal' data-target='#message<?php echo $row['id']; ?>'>View Images
                            </button>
                            <?php include("view_image_modal.php"); ?>
                            <?php
                        } else if ($status == 'Rejected') {
                            ?>
                            <button style="margin-bottom: 10px;" type='button' class='btn btn-primary'
                                    data-toggle='modal' data-target='#message<?php echo $row['id']; ?>'>View Images
                            </button>
                            <?php include("view_image_modal.php"); ?>
                            <button style="width: 100%" type='button' class='btn btn-danger' data-toggle='modal'
                                    data-target='#reject<?php echo $order_id; ?>'>View Reason
                            </button>
                            <?php include("reject_order_reason_modal.php"); ?>
                            <?php
                        }
                        ?>
                    </td>
                    <?php
                    echo "</tr>";
                }
                ?>

                </tbody>

            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->