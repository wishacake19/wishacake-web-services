<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #34b7f1;">
            <div class="inner">
                <h3><?php echo totalBakers(); ?></h3>

                <p class="info-box-text">Total Bakers</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a href="bakers.php" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #f8cc07;">
            <div class="inner">
                <h3><?php echo bakerAccountStatus(0); ?></h3>

                <p class="info-box-text">New / Pending Bakers</p>
            </div>
            <div class="icon">
                <i class="fa fa-question-circle-o"></i>
            </div>
            <a href="bakers.php?source=new_bakers" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #4aae20;">
            <div class="inner">
                <h3><?php echo bakerAccountStatus(1); ?></h3>

                <p class="info-box-text">Active / Approved Bakers</p>
            </div>
            <div class="icon">
                <i class="fa fa-check-circle-o"></i>
            </div>
            <a href="bakers.php?source=approve_bakers" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #ff5656;">
            <div class="inner">
                <h3><?php echo bakerAccountStatus(2); ?></h3>

                <p class="info-box-text">Rejected Bakers</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-times"></i>
            </div>
            <a href="bakers.php?source=reject_bakers" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box" style="box-shadow: 5px 10px 8px #888888; background-color: #FF0000;">
            <div class="inner">
                <h3><?php echo bakerAccountStatus(3); ?></h3>

                <p class="info-box-text">Blocked Bakers</p>
            </div>
            <div class="icon">
                <i class="fa fa-ban"></i>
            </div>
            <a href="bakers.php?source=block_bakers" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->

<!-- =========================================================== -->