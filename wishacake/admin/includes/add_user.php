<?php
if (isset($_POST['create_user'])) {
    $user_firstname = $_POST['user_firstname'];
    $user_lastname = $_POST['user_lastname'];
    $user_email = $_POST['user_email'];
    $user_password = $_POST['user_password'];
    $user_number = $_POST['code'] . $_POST['number'];
    $error = [
        'firstname' => '',
        'lastname' => '',
        'email' => '',
        'password' => '',
        'no' => '',
        'message' => ''
    ];
    if ($user_firstname == "") {
        $error['firstname'] = "<span style='color: #FF0000;'>* First name is required</span>";
    }
    if ($user_lastname == "") {
        $error['lastname'] = "<span style='color: #FF0000;'>* Last name is required</span>";
    }
    if ($user_email == "") {
        $error['email'] = "<span style='color: #FF0000;'>* Email address is required</span>";
    } else if (usermail_exists($user_email)) {
        $error['email'] = "<span style='color: #FF0000;'>* Email address already exists</span>";
    }
    if ($_POST['number'] == "") {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number is required</span>";
    } else if (strlen($_POST['number']) < 10) {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number must be 10 digits long</span>";
    } else if (userphone_exists($user_number)) {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number already exists</span>";
    }
    if ($user_password == "") {
        $error['password'] = "<span style='color: #FF0000;'>* Password is required</span>";
    } else if (strlen($user_password) < 8) {
        $error['password'] = "<span style='color: #FF0000;'>* Password must be at least 8 characters long</span>";
    }
    if ($user_firstname != '' && $user_lastname != '' && $user_email != '' && usermail_exists($user_email) == false && $_POST['number'] != '' && userphone_exists($user_number) == false && $user_password != '' && strlen($user_password) >= 8 && strlen($_POST['number']) == 10) {
        $query = "INSERT INTO users(first_name, last_name, email, mobile_number, password) VALUES ('{$user_firstname}','{$user_lastname}', '{$user_email}', '{$user_number}', '{$user_password}')";
        $create_user_query = mysqli_query($connection, $query);
        confirmQuery($create_user_query);
        $error['message'] = "<div class='alert alert-success alert-dismissible' style='background-color: #dff0d8 !important; border-color: #d6e9c6;'>
        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
        <strong style='color: #000000;'>Success!</strong> <span style='color: #000000;'>User added successfully — </span><a href='users.php' class='alert-link'>View Users</a>
      </div>";
    }

}
?>

<h4 class="box-title">Add new user</h4>
<hr style="background-color: #9b9b9b; height: 1px;">
<form action="users.php?source=add_user" method="post" enctype="multipart/form-data">
    <?php echo isset($error['message']) ? $error['message'] : '' ?>
    <div class="form-group">
        <label for="fname">First name</label>
        <input maxlength="35" type="text" class="form-control" name="user_firstname"
               value="<?php echo isset($_POST['user_firstname']) ? $_POST['user_firstname'] : '' ?>">
        <?php echo isset($error['firstname']) ? $error['firstname'] : '' ?>
    </div>
    <div class="form-group">
        <label for="lname">Last name</label>
        <input maxlength="35" type="text" class="form-control" name="user_lastname"
               value="<?php echo isset($_POST['user_lastname']) ? $_POST['user_lastname'] : '' ?>">
        <?php echo isset($error['lastname']) ? $error['lastname'] : '' ?>
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input maxlength="255" type="email" class="form-control" name="user_email"
               value="<?php echo isset($_POST['user_email']) ? $_POST['user_email'] : '' ?>">
        <?php echo isset($error['email']) ? $error['email'] : '' ?>
    </div>
    <div class="form-group">
        <label for="lname">Mobile number</label>
        <div class="row">
            <div class="col-xs-4 col-md-1">
                <input type="hidden" class="form-control" name="code" value="+92">
                <input type="text" class="form-control" name="code" value="+92" disabled>
            </div>
            <div class="col-xs-8 col-md-11">
                <input style="border-left: none;" maxlength="10" type="text" class="form-control" name="number"
                       value="<?php echo isset($_POST['number']) ? $_POST['number'] : '' ?>">
                <?php echo isset($error['no']) ? $error['no'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input maxlength="255" type="password" class="form-control" name="user_password">
        <?php echo isset($error['password']) ? $error['password'] : '' ?>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_user" value="Add user">
    </div>
</form>