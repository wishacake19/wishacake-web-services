<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        height: 400px;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }
</style>
<?php
if (isset($_POST['create_baker'])) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $number = $_POST['code'] . $_POST['number'];
    $cnic = $_POST['cnic'];
    $pin = $_POST['pin'];

    $time = date("Ymd_his");

    $profile = 'IMG_Profile_Photo_' . $time . '.';
    $cnic_front = 'IMG_CNIC_Front_' . $time . '.';
    $cnic_back = 'IMG_CNIC_Back_' . $time . '.';

    $profile_tmp = $_FILES['profile']['tmp_name'];
    $cnic_front_tmp = $_FILES['cnicfront']['tmp_name'];
    $cnic_back_tmp = $_FILES['cnicback']['tmp_name'];

    $location = $_POST['location'];

    // Hidden location vars
    $location_id = $_POST['location_id'];
    $location_name = $_POST['location_name'];
    $location_address = $_POST['location_address'];
    $location_latitude = $_POST['location_latitude'];
    $location_longitude = $_POST['location_longitude'];

    $error = [
        'firstname' => '',
        'lastname' => '',
        'email' => '',
        'cnic' => '',
        'pin' => '',
        'profile' => '',
        'cnicfront' => '',
        'cnicback' => '',
        'location' => '',
        'message' => ''
    ];

    $allowed_image_extension = array(
        "png",
        "jpg",
        "jpeg",
        '',
    );

    $time = date("Ymd_his");

    if ($firstname == "") {
        $error['firstname'] = "<span style='color: #FF0000;'>* First name is required</span>";
    }
    if ($lastname == "") {
        $error['lastname'] = "<span style='color: #FF0000;'>* Last name is required</span>";
    }
    if ($email == "") {
        $error['email'] = "<span style='color: #FF0000;'>* Email address is required</span>";
    } else if (bakermail_exists($email)) {
        $error['email'] = "<span style='color: #FF0000;'>* Email address already exists</span>";
    }
    if ($_POST['number'] == "") {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number is required</span>";
    } else if (strlen($_POST['number']) < 10) {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number must be 10 digits long</span>";
    } else if (bakerphone_exists($number)) {
        $error['no'] = "<span style='color: #FF0000;'>* Mobile number already exists</span>";
    }
    if ($cnic == "") {
        $error['cnic'] = "<span style='color: #FF0000;'>* CNIC is required</span>";
    } else if (bakercnic_exists($cnic)) {
        $error['cnic'] = "<span style='color: #FF0000;'>* Cnic already exists</span>";
    }
    if ($pin == "") {
        $error['pin'] = "<span style='color: #FF0000;'>* PIN is required</span>";
    } else if (strlen($pin) != 4) {
        $error['pin'] = "<span style='color: #FF0000;'>* PIN must be 4 digits long</span>";
    }

    $file_extension_profile = pathinfo($_FILES["profile"]["name"], PATHINFO_EXTENSION);
    if ($file_extension_profile != '') {
        if (!in_array($file_extension_profile, $allowed_image_extension)) {
            $error['profile'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["profile"]["size"] > 30000000)) {
            $error['profile'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $profile .= $file_extension_profile;
        }
    } else {
        $profile = '';
    }

    $file_extension_cnic_front = pathinfo($_FILES["cnicfront"]["name"], PATHINFO_EXTENSION);
    $cnic_front .= $file_extension_cnic_front;
    if (!in_array($file_extension_cnic_front, $allowed_image_extension)) {
        $error['cnicfront'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
    } else if (($_FILES["cnicfront"]["size"] > 30000000)) {
        $error['cnicfront'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
    }

    $file_extension_cnic_back = pathinfo($_FILES["cnicback"]["name"], PATHINFO_EXTENSION);
    $cnic_back .= $file_extension_cnic_back;
    if (!in_array($file_extension_cnic_back, $allowed_image_extension)) {
        $error['cnicback'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
    } else if (($_FILES["cnicback"]["size"] > 30000000)) {
        $error['cnicback'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
    }

    if ($location == "") {
        $error['location'] = "<span style='color: #FF0000;'>* Location is required</span>";
    } else if ($location_id == "") {
        $error['location'] = "<span style='color: #FF0000;'>* Please select location from map</span>";
    }
    if ($firstname != '' && $lastname != '' && $email != '' && bakermail_exists($email) == false
        && $_POST['number'] != '' && bakerphone_exists($number) == false && bakercnic_exists($cnic) == false &&
        in_array($file_extension_cnic_front, $allowed_image_extension) &&
        in_array($file_extension_cnic_back, $allowed_image_extension) &&
        $location != '' && $location_id != '' && $pin != "" && strlen($pin) == 4 && strlen($_POST['number']) == 10) {

        if ($profile != '') {
            move_uploaded_file($profile_tmp, "../uploads/images/" . $profile);
        }
        move_uploaded_file($cnic_front_tmp, "../uploads/images/" . $cnic_front);
        move_uploaded_file($cnic_back_tmp, "../uploads/images/" . $cnic_back);

        $query = "INSERT INTO bakers(first_name, last_name, email, mobile_number, cnic, pin, image, cnic_front, cnic_back, location_id, location_name, location_address, location_latitude, location_longitude, account_status) VALUES ('{$firstname}','{$lastname}', '{$email}', '{$number}', '{$cnic}', '{$pin}', '{$profile}', '{$cnic_front}', '{$cnic_back}', '{$location_id}', '{$location_name}', '{$location_address}', '{$location_latitude}', '{$location_longitude}', '1')";
        $create_baker_query = mysqli_query($connection, $query);
        confirmQuery($create_baker_query);

        $last_inserted_baker_id = mysqli_insert_id($connection);
        $sql = "INSERT INTO favorite_bakers (baker_id) VALUES ('$last_inserted_baker_id')";
        $create_baker_copy_in_favorites = mysqli_query($connection, $sql);
        confirmQuery($create_baker_copy_in_favorites);

        $error['message'] = "<div class='alert alert-success alert-dismissible' style='background-color: #dff0d8 !important; border-color: #d6e9c6;'>
        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
        <strong style='color: #000000;'>Success!</strong> <span style='color: #000000;'>Baker added successfully — </span><a href='bakers.php' class='alert-link'>View Bakers</a>
      </div>";

    }

}
?>
<h4 class="box-title">Add new baker</h4>
<hr style="background-color: #9b9b9b; height: 1px;">
<form action="bakers.php?source=add_baker" method="post" enctype="multipart/form-data">
    <?php echo isset($error['message']) ? $error['message'] : '' ?>
    <div class="form-group">
        <label for="fname">First name</label>
        <input maxlength="35" type="text" class="form-control" name="firstname"
               value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : '' ?>">
        <?php echo isset($error['firstname']) ? $error['firstname'] : '' ?>
    </div>
    <div class="form-group">
        <label for="lname">Last name</label>
        <input maxlength="35" type="text" class="form-control" name="lastname"
               value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : '' ?>">
        <?php echo isset($error['lastname']) ? $error['lastname'] : '' ?>
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input maxlength="255" type="email" class="form-control" name="email"
               value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
        <?php echo isset($error['email']) ? $error['email'] : '' ?>
    </div>
    <div class="form-group">
        <label for="no">Mobile number</label>
        <div class="row">
            <div class="col-xs-4 col-md-1">
                <input type="hidden" class="form-control" name="code" value="+92">
                <input type="text" class="form-control" name="code" value="+92" disabled>
            </div>
            <div class="col-xs-8 col-md-11">
                <input maxlength="10" type="text" class="form-control" name="number"
                       value="<?php echo isset($_POST['number']) ? $_POST['number'] : '' ?>">
                <?php echo isset($error['no']) ? $error['no'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="cnic">CNIC</label>
        <input type="text" class="form-control" name="cnic" id="cnic"
               value="<?php echo isset($_POST['cnic']) ? $_POST['cnic'] : '' ?>">
        <?php echo isset($error['cnic']) ? $error['cnic'] : '' ?>
    </div>

    <div class="form-group">
        <label for="cnic">PIN</label>
        <input type="text" class="form-control" maxlength="4" name="pin" id="pin"
               value="<?php echo isset($_POST['pin']) ? $_POST['pin'] : '' ?>">
        <?php echo isset($error['pin']) ? $error['pin'] : '' ?>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="profile">Profile photo (optional)</label>
                <input type="file" name="profile" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['profile']) ? $error['profile'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image2">CNIC Front photo</label>
                <input type="file" name="cnicfront" accept="image/png, image/jpg, image/jpeg" required>
                <?php echo isset($error['cnicfront']) ? $error['cnicfront'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="cnicback">CNIC Back photo</label>
                <input type="file" name="cnicback" accept="image/png, image/jpg, image/jpeg" required>
                <?php echo isset($error['cnicback']) ? $error['cnicback'] : '' ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="location">Location</label>
        <input type="text" class="form-control" name="location" id="location"
               value="<?php echo isset($_POST['location']) ? $_POST['location'] : '' ?>">
        <input type="hidden" class="form-control" name="location_id" id="location_id">
        <input type="hidden" class="form-control" name="location_name" id="location_name">
        <input type="hidden" class="form-control" name="location_address" id="location_address">
        <input type="hidden" class="form-control" name="location_latitude" id="location_latitude">
        <input type="hidden" class="form-control" name="location_longitude" id="location_longitude">
        <?php echo isset($error['location']) ? $error['location'] : '' ?>
    </div>

    <div class="form-group">
        <div id="map"></div>
    </div>
    <div id="infowindow-content">
        <img src="" width="24" height="24" id="place-icon" hidden>
        <span id="place-name" class="title"></span><br>
        <span id="place-address"></span>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_baker" value="Add baker">
    </div>
</form>
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 24.8933157, lng: 67.0881958},
            zoom: 18
        });

        var input = document.getElementById('location');
        var options = {
            type: ['(cities)'],
            componentRestrictions: {country: 'pk'}
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'place_id', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function () {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(18);  // Why 18? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);

            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();

            console.log(place.place_id);
            console.log(place.name);
            console.log(address);
            console.log(latitude);
            console.log(longitude);

            document.getElementById('location_id').value = place.place_id;
            document.getElementById('location_name').value = place.name;
            document.getElementById('location_address').value = address;
            document.getElementById('location_latitude').value = latitude;
            document.getElementById('location_longitude').value = longitude;
        });
    }

</script>
