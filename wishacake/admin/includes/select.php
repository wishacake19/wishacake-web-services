<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "wishacake";

foreach ($db as $key => $value) {
    define(strtoupper($key), $value);
}
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$connection) {
    die("Database connection Failed" . mysqli_error($connection));
}
$query = "SELECT * FROM bakers WHERE account_status = 0 AND notification_seen_status = 0 ORDER BY created_at DESC";
$select = mysqli_query($connection, $query);
$result = mysqli_num_rows($select);
if ($result > 0) {
    $count = $result;
} else {
    $count = '';
}
echo json_encode($count);
?>