<?php
$check = false;
if (!isset($_GET['baker_id'])) {
    $query = "UPDATE bakers SET notification_seen_status = 1, modified_at = CURRENT_TIMESTAMP() WHERE account_status = 0";
    $update_user_query = mysqli_query($connection, $query);
    $query = '';
}

if (isset($_GET['baker_id'])) {
    if ($_GET['baker_id']) {
        $id = $_GET['baker_id'];
        $query = "UPDATE bakers SET notification_seen_status = 1, modified_at = CURRENT_TIMESTAMP() WHERE account_status = 0 AND id = $id";
        $update_user_query = mysqli_query($connection, $query);
        $check = true;
        $query = '';
    }
}

?>

<div class="box">
    <div class="box-header">
        <h2 class="box-title">New / Pending bakers</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email address</th>
                    <th>Mobile number</th>
                    <th>CNIC</th>
                    <th>Profile photo</th>
                    <th>CNIC Front photo</th>
                    <th>CNIC Back photo</th>
                    <th>Location</th>
                    <th>Account status</th>
                    <th>Created at</th>
                    <th>Last modified</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($check) {
                    $query = "SELECT * FROM bakers WHERE account_status = 0 AND notification_seen_status = 1 AND id = $id ORDER BY created_at DESC";
                } else {
                    $query = "SELECT * FROM bakers WHERE account_status = 0 AND notification_seen_status = 1 ORDER BY created_at DESC";
                }

                $select_bakers = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_bakers)) {

                    $baker_id = $row['id'];
                    $first_name = $row['first_name'];
                    $last_name = $row['last_name'];
                    $email = $row['email'];
                    $no = $row['mobile_number'];
                    $cnic = $row['cnic'];
                    $profile_photo = $row['image'];
                    $cnic_front = $row['cnic_front'];
                    $cnic_back = $row['cnic_back'];
                    $location = $row['location_name'] . '<br>' . $row['location_address'];
                    $status = $row['account_status'];
                    $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');
                    $last_modified = $row['modified_at'];
                    if ($last_modified != '0000-00-00 00:00:00') {
                        $last_modified = date_create($row['modified_at'])->format('d M, Y') . ' • ' . date_create($row['modified_at'])->format('h:i A');;
                    } else {
                        $last_modified = "";
                    }

                    if ($status == '1') {
                        $status = 'Active';
                    } else if ($status == '0') {
                        $status = 'Pending';
                    } else if ($status == '2') {
                        $status = 'Rejected';
                    } else if ($status == '3') {
                        $status = 'Blocked';
                    }
                    if ($profile_photo == '') {
                        $imgProfileSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($profile_photo != '') {
                        $imgProfileSrc = '../uploads/images/' . $profile_photo;
                    }
                    if ($cnic_front == '') {
                        $imgCnicFrontSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($cnic_front != '') {
                        $imgCnicFrontSrc = '../uploads/images/' . $cnic_front;
                    }
                    if ($cnic_back == '') {
                        $imgCnicBackSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($cnic_back != '') {
                        $imgCnicBackSrc = '../uploads/images/' . $cnic_back;
                    }
                    echo "<tr>";
                    echo "<td>$baker_id</td>";
                    echo "<td>$first_name</td>";
                    echo "<td>$last_name</td>";
                    echo "<td>$email</td>";
                    echo "<td>$no</td>";
                    echo "<td>$cnic</td>";
                    ?>
                    <td><img width='100' height='100' src="<?php echo $imgProfileSrc; ?>"></td>

                    <td><img width='100' height='100' src="<?php echo $imgCnicFrontSrc; ?>"></td>
                    <td><img width='100' height='100' src="<?php echo $imgCnicBackSrc; ?>"></td>
                    <?php
                    echo "<td>$location</td>";
                    echo "<td>$status</td>";
                    echo "<td>$created_at</td>";
                    echo "<td>$last_modified</td>";
                    ?>
                    <td>
                        <?php
                        if ($status == 'Active') {
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-info' href = 'bakers.php?source=edit_baker&edit_baker=$baker_id'>Edit</a>";
                            echo "<a style='width: 100%;' class='btn btn-danger' href = 'bakers.php?block=$baker_id'>Block</a>";
                        } else if ($status == 'Blocked') {
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-info' href = 'bakers.php?source=edit_baker&edit_baker=$baker_id'>Edit</a>";
                            echo "<a style='width: 100%;' class='btn btn-success' href = 'bakers.php?unblock=$baker_id'>Unblock</a>";
                        } else if ($status == 'Pending') {
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-info' href = 'bakers.php?source=edit_baker&edit_baker=$baker_id'>Edit</a>";
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-success' href = 'bakers.php?approve=$baker_id'>Approve</a>";
                            ?>
                            <button style="width: 100%" type='button' class='btn btn-danger' data-toggle='modal'
                                    data-target='#reject<?php echo $row['id']; ?>'>Reject
                            </button>
                            <?php include("reject_modal.php"); ?>
                            <?php
                        } else if ($status == 'Rejected') {
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-info' href = 'bakers.php?source=edit_baker&edit_baker=$baker_id'>Edit</a>";
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-success' href = 'bakers.php?approve=$baker_id'>Approve</a>";
                            ?>
                            <button type='button' class='btn btn-primary' data-toggle='modal'
                                    data-target='#message<?php echo $row['id']; ?>'>View Reason
                            </button>
                            <?php include("rejected_reason_modal.php"); ?>

                            <?php
                        }
                        ?>
                    </td>
                    <?php
                    echo "</tr>";
                }
                ?>

                </tbody>

            </table>

            <?php
            if (isset($_GET['block'])) {
                $the_baker_id = $_GET['block'];
                $query = "UPDATE bakers SET account_status = '3', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_baker_id}";
                $change_status_to_block = mysqli_query($connection, $query);
                header("Location: bakers.php");
            }
            if (isset($_GET['unblock'])) {
                $the_baker_id = $_GET['unblock'];
                $query = "UPDATE bakers SET account_status = '1', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_baker_id}";
                $change_status_to_unblock = mysqli_query($connection, $query);
                header("Location: bakers.php");
            }
            if (isset($_GET['approve'])) {
                $the_baker_id = $_GET['approve'];
                $query = "UPDATE bakers SET account_status = '1', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_baker_id}";
                $change_status_to_approve = mysqli_query($connection, $query);

                // Push notification
                $query = "SELECT firebase_tokens FROM bakers WHERE id = $the_baker_id";
                $res_firebase_tokens = mysqli_query($connection, $query);
                $firebase_tokens;
                while ($row = mysqli_fetch_assoc($res_firebase_tokens)) {
                    $firebase_tokens = $row['firebase_tokens'];
                }

                if ($firebase_tokens != '' || $firebase_tokens != 'null') {
                    $query_when = "SELECT CURRENT_TIMESTAMP() AS NOW_TIME";
                    $res_when = mysqli_query($connection, $query_when);
                    $when;
                    while ($row = mysqli_fetch_assoc($res_when)) {
                        $when = $row['NOW_TIME'];
                    }

                    $request_data = array(
                        'title' => 'Account activated',
                        'message' => 'Congratulations! Your account has been activated. To start using the application, log in with your mobile number and PIN.',
                        'action' => 'activity',
                        'action_destination' => 'LoginActivity',
                        'when' => $when
                    );

                    $registration_ids_array = explode(",", $firebase_tokens);

                    $fields = array(
                        'registration_ids' => $registration_ids_array,
                        'data' => $request_data
                    );

                    $server_key = 'AAAAwjZrkPA:APA91bFsEh4r4TIfLD2UwvfPq_Gj-coDuvjAjt8W4wokYInjibdLuZU3RlwZU32NKzBefz-hr-TyeQumK2eNWRPOTNdeB0gPZ1VJjOrP-lkIhGie-JondpYfequNJhOACJbS61TZBUWn';

                    $headers = array(
                        'Authorization: key=' . $server_key,
                        'Content-Type: application/json'
                    );

                    // open connection
                    $ch = curl_init();

                    // set the url, number of POST variables, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                    curl_exec($ch);

                    curl_close($ch);
                }
                header("Location: bakers.php");
            }

            ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

