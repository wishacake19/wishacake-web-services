<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "wishacake";

foreach ($db as $key => $value) {
    define(strtoupper($key), $value);
}
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$connection) {
    die("Database connection Failed" . mysqli_error($connection));
}
$query = "SELECT id, first_name, last_name FROM bakers WHERE account_status = 0 AND notification_seen_status = 0 ORDER BY created_at DESC";
$select = mysqli_query($connection, $query);
$results = array();
while ($row = mysqli_fetch_array($select)) {
    $id = $row['id'];
    $fname = $row['first_name'];
    $lname = $row['last_name'];
    $results[] = array("id" => $id, "fname" => $fname, "lname" => $lname);
}

echo json_encode($results);
?>