<?php
if (isset($_POST['create_featured_cakes'])) {
    $error = [
        'title' => '',
        'description' => '',
        'image1' => '',
        'image2' => '',
        'image3' => '',
        'image4' => '',
        'image5' => '',
        'image6' => '',
        'image7' => '',
        'image8' => '',
        'image9' => '',
        'image10' => ''
    ];

    $time = date("Ymd_his");

    $image1 = 'IMG_Cake_01_' . $time . '.';
    $image2 = 'IMG_Cake_02_' . $time . '.';
    $image3 = 'IMG_Cake_03_' . $time . '.';
    $image4 = 'IMG_Cake_04_' . $time . '.';
    $image5 = 'IMG_Cake_05_' . $time . '.';
    $image6 = 'IMG_Cake_06_' . $time . '.';
    $image7 = 'IMG_Cake_07_' . $time . '.';
    $image8 = 'IMG_Cake_08_' . $time . '.';
    $image9 = 'IMG_Cake_09_' . $time . '.';
    $image10 = 'IMG_Cake_10_' . $time . '.';

    $image1_tmp = $_FILES['image1']['tmp_name'];
    $image2_tmp = $_FILES['image2']['tmp_name'];
    $image3_tmp = $_FILES['image3']['tmp_name'];
    $image4_tmp = $_FILES['image4']['tmp_name'];
    $image5_tmp = $_FILES['image5']['tmp_name'];
    $image6_tmp = $_FILES['image6']['tmp_name'];
    $image7_tmp = $_FILES['image7']['tmp_name'];
    $image8_tmp = $_FILES['image8']['tmp_name'];
    $image9_tmp = $_FILES['image9']['tmp_name'];
    $image10_tmp = $_FILES['image10']['tmp_name'];

    $title = $_POST['title'];
    $description = trim($_POST['description']);

    $allowed_image_extension = array(
        "png",
        "jpg",
        "jpeg",
        ''
    );

    if ($title == "") {
        $error['title'] = "<span style='color: #FF0000;'>* Title is required</span>";
    }
    if ($description == "") {
        $error['description'] = "<span style='color: #FF0000;'>* Description is required</span>";
    }

    $image1_ex = pathinfo($_FILES["image1"]["name"], PATHINFO_EXTENSION);
    if (!in_array($image1_ex, $allowed_image_extension)) {
        $error['image1'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
    } else if (($_FILES["image1"]["size"] > 30000000)) {
        $error['image1'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
    } else {
        $image1 .= $image1_ex;
    }

    $image2_ex = pathinfo($_FILES["image2"]["name"], PATHINFO_EXTENSION);
    if ($image2_ex != "") {
        if (!in_array($image2_ex, $allowed_image_extension)) {
            $error['image2'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image2"]["size"] > 30000000)) {
            $error['image2'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image2 .= $image2_ex;
        }
    } else {
        $image2 = "";
    }

    $image3_ex = pathinfo($_FILES["image3"]["name"], PATHINFO_EXTENSION);
    if ($image3_ex != "") {
        if (!in_array($image3_ex, $allowed_image_extension)) {
            $error['image3'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image3"]["size"] > 30000000)) {
            $error['image3'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image3 .= $image3_ex;
        }
    } else {
        $image3 = "";
    }

    $image4_ex = pathinfo($_FILES["image4"]["name"], PATHINFO_EXTENSION);
    if ($image4_ex != "") {
        if (!in_array($image4_ex, $allowed_image_extension)) {
            $error['image4'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image4"]["size"] > 30000000)) {
            $error['image4'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image4 .= $image4_ex;
        }
    } else {
        $image4 = "";
    }

    $image5_ex = pathinfo($_FILES["image5"]["name"], PATHINFO_EXTENSION);
    if ($image5_ex != "") {
        if (!in_array($image5_ex, $allowed_image_extension)) {
            $error['image5'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image5"]["size"] > 30000000)) {
            $error['image5'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image5 .= $image5_ex;
        }
    } else {
        $image5 = "";
    }

    $image6_ex = pathinfo($_FILES["image6"]["name"], PATHINFO_EXTENSION);
    if ($image6_ex != "") {
        if (!in_array($image6_ex, $allowed_image_extension)) {
            $error['image6'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image6"]["size"] > 30000000)) {
            $error['image6'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image6 .= $image6_ex;
        }
    } else {
        $image6 = "";
    }

    $image7_ex = pathinfo($_FILES["image7"]["name"], PATHINFO_EXTENSION);
    if ($image7_ex != "") {
        if (!in_array($image7_ex, $allowed_image_extension)) {
            $error['image7'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image7"]["size"] > 30000000)) {
            $error['image7'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image7 .= $image7_ex;
        }
    } else {
        $image7 = "";
    }

    $image8_ex = pathinfo($_FILES["image8"]["name"], PATHINFO_EXTENSION);
    if ($image8_ex != "") {
        if (!in_array($image8_ex, $allowed_image_extension)) {
            $error['image8'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image8"]["size"] > 30000000)) {
            $error['image8'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image8 .= $image8_ex;
        }
    } else {
        $image8 = "";
    }

    $image9_ex = pathinfo($_FILES["image9"]["name"], PATHINFO_EXTENSION);
    if ($image9_ex != "") {
        if (!in_array($image9_ex, $allowed_image_extension)) {
            $error['image9'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image9"]["size"] > 30000000)) {
            $error['image9'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image9 .= $image9_ex;
        }
    } else {
        $image9 = "";
    }

    $image10_ex = pathinfo($_FILES["image10"]["name"], PATHINFO_EXTENSION);
    if ($image10_ex != "") {
        if (!in_array($image10_ex, $allowed_image_extension)) {
            $error['image10'] = "<span style='color: #FF0000;'>* Only PNG, JPG and JPEG are allowed</span>";
        } else if (($_FILES["image10"]["size"] > 30000000)) {
            $error['image10'] = "<span style='color: #FF0000;'>* Image size exceeds 3MB</span>";
        } else {
            $image10 .= $image10_ex;
        }
    } else {
        $image10 = "";
    }

    if ($description != "" && $title != "" && in_array($image1_ex, $allowed_image_extension)) {
        move_uploaded_file($image1_tmp, "../uploads/images/$image1");
        move_uploaded_file($image2_tmp, "../uploads/images/$image2");
        move_uploaded_file($image3_tmp, "../uploads/images/$image3");
        move_uploaded_file($image4_tmp, "../uploads/images/$image4");
        move_uploaded_file($image5_tmp, "../uploads/images/$image5");
        move_uploaded_file($image6_tmp, "../uploads/images/$image6");
        move_uploaded_file($image7_tmp, "../uploads/images/$image7");
        move_uploaded_file($image8_tmp, "../uploads/images/$image8");
        move_uploaded_file($image9_tmp, "../uploads/images/$image9");
        move_uploaded_file($image10_tmp, "../uploads/images/$image10");

        $query = "INSERT INTO featured_cakes(image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, title, description) VALUES ('{$image1}','{$image2}', '{$image3}', '{$image4}', '{$image5}', '{$image6}', '{$image7}', '{$image8}', '{$image9}', '{$image10}', '{$title}', '{$description}')";
        $create_featured_cake_query = mysqli_query($connection, $query);
        confirmQuery($create_featured_cake_query);

        $message = "<div class='alert alert-success alert-dismissible' style='background-color: #dff0d8 !important; border-color: #d6e9c6;'>
        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
        <strong style='color: #000000;'>Success!</strong> <span style='color: #000000;'>Featured cake added successfully — </span><a href='featured_cakes.php' class='alert-link'>View Featured Cakes</a>
      </div>";
    }

}
?>

<h4 class="box-title">Add featured cake</h4>
<hr style="background-color: #9b9b9b; height: 1px;">
<form action="featured_cakes.php?source=add_featured_cakes" method="post" enctype="multipart/form-data">
    <?php echo isset($message) ? $message : '' ?>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="image1">Image 1</label>
                <input type="file" name="image1" required accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image1']) ? $error['image1'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image2">Image 2 (optional)</label>
                <input type="file" name="image2" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image2']) ? $error['image2'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image3">Image 3 (optional)</label>
                <input type="file" name="image3" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image3']) ? $error['image3'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="image4">Image 4 (optional)</label>
                <input type="file" name="image4" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image4']) ? $error['image4'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image5">Image 5 (optional)</label>
                <input type="file" name="image5" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image5']) ? $error['image5'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image6">Image 6 (optional)</label>
                <input type="file" name="image6" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image6']) ? $error['image6'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="image7">Image 7 (optional)</label>
                <input type="file" name="image7" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image7']) ? $error['image7'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image8">Image 8 (optional)</label>
                <input type="file" name="image8" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image8']) ? $error['image8'] : '' ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <label for="image9">Image 9 (optional)</label>
                <input type="file" name="image9" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image9']) ? $error['image9'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label for="image10">Image 10 (optional)</label>
                <input type="file" name="image10" accept="image/png, image/jpg, image/jpeg">
                <?php echo isset($error['image10']) ? $error['image10'] : '' ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title">
        <?php echo isset($error['title']) ? $error['title'] : '' ?>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" cols="190" rows="5"></textarea>
        <?php echo isset($error['description']) ? $error['description'] : '' ?>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_featured_cakes" value="Add featured cake">
    </div>
</form>