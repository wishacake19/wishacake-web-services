<div id="reject<?php echo $row['id']; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Reject reason</h3>
            </div>
            <div class="modal-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="rejected_reason">Reject reason</label><br>
                        <textarea class="form-control" name="rejected_reason" cols="80" rows="5"></textarea>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <input type="hidden" value="<?php echo $row['id']; ?>" name="id">
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" name="rejected" value="Reject">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            </form>
        </div>

    </div>
</div>

<?php
if (isset($_POST['rejected'])) {
    $reason = $_POST['rejected_reason'];
    $id = $_POST['id'];
    $query = "UPDATE bakers SET account_status = '2', rejected_reason = '{$reason}', modified_at = CURRENT_TIMESTAMP() WHERE id = $id";
    $reject_reason = mysqli_query($connection, $query);

    // Push notification
    $query = "SELECT firebase_tokens FROM bakers WHERE id = $id";
    $res_firebase_tokens = mysqli_query($connection, $query);
    $firebase_tokens;
    while ($row = mysqli_fetch_assoc($res_firebase_tokens)) {
        $firebase_tokens = $row['firebase_tokens'];
    }

    if ($firebase_tokens != '' || $firebase_tokens != 'null') {
        $query_when = "SELECT CURRENT_TIMESTAMP() AS NOW_TIME";
        $res_when = mysqli_query($connection, $query_when);
        $when;
        while ($row = mysqli_fetch_assoc($res_when)) {
            $when = $row['NOW_TIME'];
        }

        $request_data = array(
            'title' => 'Account request rejected',
            'message' => 'We\'re sorry, the information you provided isn\'t correct. For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.',
            'action' => 'activity',
            'action_destination' => 'LoginActivity',
            'when' => $when
        );

        $registration_ids_array = explode(",", $firebase_tokens);

        $fields = array(
            'registration_ids' => $registration_ids_array,
            'data' => $request_data
        );

        $server_key = 'AAAAwjZrkPA:APA91bFsEh4r4TIfLD2UwvfPq_Gj-coDuvjAjt8W4wokYInjibdLuZU3RlwZU32NKzBefz-hr-TyeQumK2eNWRPOTNdeB0gPZ1VJjOrP-lkIhGie-JondpYfequNJhOACJbS61TZBUWn';

        $headers = array(
            'Authorization: key=' . $server_key,
            'Content-Type: application/json'
        );

        // open connection
        $ch = curl_init();

        // set the url, number of POST variables, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_exec($ch);

        curl_close($ch);
    }

    header("Location: bakers.php");
    exit;
}
?>