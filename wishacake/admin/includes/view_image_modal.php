<div id="message<?php echo $row['id']; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Images for order # <?php echo $row['id']; ?></h3>
            </div>
            <div class="modal-body">
                <?php
                $reason = '';
                $id = $row['id'];
                $query = "SELECT * FROM orders WHERE id = $id";
                $select_images = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_images)) {
                    $image1 = $row['image1'];
                    $image2 = $row['image2'];
                    $image3 = $row['image3'];
                    $image4 = $row['image4'];
                    $image5 = $row['image5'];
                    $image6 = $row['image6'];
                    $image7 = $row['image7'];
                    $image8 = $row['image8'];
                    $image9 = $row['image9'];
                    $image10 = $row['image10'];
                    if ($image1 == '') {
                        $img1Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image1 != '') {
                        $img1Src = '../uploads/images/' . $image1;
                    }
                    if ($image2 == '') {
                        $img2Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image2 != '') {
                        $img2Src = '../uploads/images/' . $image2;
                    }

                    if ($image3 == '') {
                        $img3Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image3 != '') {
                        $img3Src = '../uploads/images/' . $image3;
                    }

                    if ($image4 == '') {
                        $img4Src = '../uploads/images/not_available.jpg';
                    }

                    if ($image4 != '') {
                        $img4Src = '../uploads/images/' . $image4;
                    }

                    if ($image5 == '') {
                        $img5Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image5 != '') {
                        $img5Src = '../uploads/images/' . $image5;
                    }

                    if ($image6 == '') {
                        $img6Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image6 != '') {
                        $img6Src = '../uploads/images/' . $image6;
                    }

                    if ($image7 == '') {
                        $img7Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image7 != '') {
                        $img7Src = '../uploads/images/' . $image7;
                    }

                    if ($image8 == '') {
                        $img8Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image8 != '') {
                        $img8Src = '../uploads/images/' . $image8;
                    }

                    if ($image9 == '') {
                        $img9Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image9 != '') {
                        $img9Src = '../uploads/images/' . $image9;
                    }

                    if ($image10 == '') {
                        $img10Src = '../uploads/images/not_available.jpg';
                    }
                    if ($image10 != '') {
                        $img10Src = '../uploads/images/' . $image10;
                    }
                    ?>

                    <div class="row">
                        <div class="col-xs-4">
                            <label for="image1">Image 1</label><br>
                            <img width="150" height="150" src="<?php echo $img1Src; ?>" alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image2">Image 2</label><br>
                            <img width="150" height="150" src=<?php echo $img2Src; ?> alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image3">Image 3</label><br>
                            <img width="150" height="150" src=<?php echo $img3Src; ?> alt="">
                        </div>
                    </div>
                    <hr style="background-color: #9b9b9b; height: 1px;">
                    <div class="row">
                        <div class="col-xs-4">
                            <label for="image4">Image 4</label><br>
                            <img width="150" height="150" src=<?php echo $img4Src; ?> alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image5">Image 5</label><br>
                            <img width="150" height="150" src=<?php echo $img5Src; ?> alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image6">Image 6</label><br>
                            <img width="150" height="150" src=<?php echo $img6Src; ?> alt="">
                        </div>
                    </div>
                    <hr style="background-color: #9b9b9b; height: 1px;">
                    <div class="row">
                        <div class="col-xs-4">
                            <label for="image7">Image 7</label><br>
                            <img width="150" height="150" src=<?php echo $img7Src; ?> alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image8">Image 8</label><br>
                            <img width="150" height="150" src=<?php echo $img8Src; ?> alt="">
                        </div>
                        <div class="col-xs-4">
                            <label for="image9">Image 9</label><br>
                            <img width="150" height="150" src=<?php echo $img9Src; ?> alt="">
                        </div>
                    </div>
                    <hr style="background-color: #9b9b9b; height: 1px;">
                    <label for="image10">Image 10</label><br>
                    <img width="150" height="150" src=<?php echo $img10Src; ?> alt="">
                    <?php
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>