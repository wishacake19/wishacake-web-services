<?php include("delete_modal.php"); ?>
    <div class="box">
        <div class="box-header">
            <h2 class="box-title">All featured cakes</h2>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Image1</th>
                        <th>Image2</th>
                        <th>Image3</th>
                        <th>Image4</th>
                        <th>Image5</th>
                        <th>Image6</th>
                        <th>Image7</th>
                        <th>Image8</th>
                        <th>Image9</th>
                        <th>Image10</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Created at</th>
                        <th>Modified at</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $query = "SELECT * FROM featured_cakes ORDER BY id DESC";
                    $select_featured_cakes = mysqli_query($connection, $query);
                    confirmQuery($select_featured_cakes);
                    while ($row = mysqli_fetch_assoc($select_featured_cakes)) {
                        $id = $row['id'];
                        $image1 = $row['image1'];
                        $image2 = $row['image2'];
                        $image3 = $row['image3'];
                        $image4 = $row['image4'];
                        $image5 = $row['image5'];
                        $image6 = $row['image6'];
                        $image7 = $row['image7'];
                        $image8 = $row['image8'];
                        $image9 = $row['image9'];
                        $image10 = $row['image10'];
                        $title = $row['title'];
                        if ($image2 == '') {
                            $img2Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image2 != '') {
                            $img2Src = '../uploads/images/' . $image2;
                        }

                        if ($image3 == '') {
                            $img3Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image3 != '') {
                            $img3Src = '../uploads/images/' . $image3;
                        }

                        if ($image4 == '') {
                            $img4Src = '../uploads/images/not_available.jpg';
                        }

                        if ($image4 != '') {
                            $img4Src = '../uploads/images/' . $image4;
                        }

                        if ($image5 == '') {
                            $img5Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image5 != '') {
                            $img5Src = '../uploads/images/' . $image5;
                        }

                        if ($image6 == '') {
                            $img6Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image6 != '') {
                            $img6Src = '../uploads/images/' . $image6;
                        }

                        if ($image7 == '') {
                            $img7Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image7 != '') {
                            $img7Src = '../uploads/images/' . $image7;
                        }

                        if ($image8 == '') {
                            $img8Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image8 != '') {
                            $img8Src = '../uploads/images/' . $image8;
                        }

                        if ($image9 == '') {
                            $img9Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image9 != '') {
                            $img9Src = '../uploads/images/' . $image9;
                        }

                        if ($image10 == '') {
                            $img10Src = '../uploads/images/not_available.jpg';
                        }
                        if ($image10 != '') {
                            $img10Src = '../uploads/images/' . $image10;
                        }
                        $description = $row['description'];
                        $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');
                        $last_modified = $row['modified_at'];
                        if ($last_modified != '0000-00-00 00:00:00') {
                            $last_modified = date_create($row['modified_at'])->format('d M, Y') . ' • ' . date_create($row['modified_at'])->format('h:i A');;
                        } else {
                            $last_modified = "";
                        }
                        ?>
                        <tr>
                            <td><?php echo $id; ?></td>
                            <td><img width="100" height="100" src="../uploads/images/<?php echo $image1; ?>"></td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img2Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img3Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img4Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img5Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img6Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img7Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img8Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img9Src; ?>">
                            </td>
                            <td>
                                <img width="100" height="100" src="<?php echo $img10Src; ?>">
                            </td>
                            <td><?php echo $title; ?></td>
                            <td><?php echo $description; ?></td>
                            <td><?php echo $created_at; ?></td>
                            <td><?php echo $last_modified; ?></td>
                            <td><a style='margin-bottom: 10px; width: 100%;' class='btn btn-info'
                                   href='featured_cakes.php?source=edit_featured_cake&id=<?php echo $id; ?>'>Edit</a><a
                                        style="width: 100%" class='btn btn-danger delete'
                                        rel=<?php echo $id; ?> href='javascript: void(0)'>Delete</a></td>
                        </tr>
                    <?php }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
<?php
if (isset($_GET['delete'])) {
    $featured_cakes_id = $_GET['delete'];
    $query = "DELETE FROM featured_cakes WHERE id = {$featured_cakes_id}";
    $delete_query = mysqli_query($connection, $query);
    header("Location: featured_cakes.php");
}
?>