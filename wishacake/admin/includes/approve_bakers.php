<div class="box">
    <div class="box-header">
        <h2 class="box-title">Active / Approved bakers</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email address</th>
                    <th>Mobile number</th>
                    <th>CNIC</th>
                    <th>Profile photo</th>
                    <th>CNIC Front photo</th>
                    <th>CNIC Back photo</th>
                    <th>Location</th>
                    <th>Created at</th>
                    <th>Last modified</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $query = "SELECT * FROM bakers WHERE account_status = 1 ORDER BY created_at DESC";
                $select_bakers = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_bakers)) {

                    $baker_id = $row['id'];
                    $first_name = $row['first_name'];
                    $last_name = $row['last_name'];
                    $email = $row['email'];
                    $no = $row['mobile_number'];
                    $cnic = $row['cnic'];
                    $profile_photo = $row['image'];
                    $cnic_front = $row['cnic_front'];
                    $cnic_back = $row['cnic_back'];
                    $location = $row['location_name'] . '<br>' . $row['location_address'];
                    $status = $row['account_status'];
                    $created_at = date_create($row['created_at'])->format('d M, Y') . ' • ' . date_create($row['created_at'])->format('h:i A');
                    $last_modified = $row['modified_at'];
                    if ($last_modified != '0000-00-00 00:00:00') {
                        $last_modified = date_create($row['modified_at'])->format('d M, Y') . ' • ' . date_create($row['modified_at'])->format('h:i A');;
                    } else {
                        $last_modified = "";
                    }

                    if ($status == '1') {
                        $status = 'Active';
                    }
                    if ($profile_photo == '') {
                        $imgProfileSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($profile_photo != '') {
                        $imgProfileSrc = '../uploads/images/' . $profile_photo;
                    }
                    if ($cnic_front == '') {
                        $imgCnicFrontSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($cnic_front != '') {
                        $imgCnicFrontSrc = '../uploads/images/' . $cnic_front;
                    }
                    if ($cnic_back == '') {
                        $imgCnicBackSrc = '../uploads/images/not_available.jpg';
                    }
                    if ($cnic_back != '') {
                        $imgCnicBackSrc = '../uploads/images/' . $cnic_back;
                    }
                    echo "<tr>";
                    echo "<td>$baker_id</td>";
                    echo "<td>$first_name</td>";
                    echo "<td>$last_name</td>";
                    echo "<td>$email</td>";
                    echo "<td>$no</td>";
                    echo "<td>$cnic</td>";
                    ?>
                    <td><img width='100' height='100' src="<?php echo $imgProfileSrc; ?>"></td>

                    <td><img width='100' height='100' src="<?php echo $imgCnicFrontSrc; ?>"></td>
                    <td><img width='100' height='100' src="<?php echo $imgCnicBackSrc; ?>"></td>
                    <?php
                    echo "<td>$location</td>";
                    echo "<td>$created_at</td>";
                    echo "<td>$last_modified</td>";
                    ?>
                    <td>
                        <?php
                        if ($status == 'Active') {
                            echo "<a style='margin-bottom: 10px; width: 100%;' class='btn btn-info' href = 'bakers.php?source=edit_baker&edit_baker=$baker_id'>Edit</a>";
                            echo "<a style='width: 100%;' class='btn btn-danger' href = 'bakers.php?block=$baker_id'>Block</a>";
                        }

                        ?>
                    </td>
                    <?php
                    echo "</tr>";
                }
                ?>

                </tbody>

            </table>

            <?php
            if (isset($_GET['block'])) {
                $the_baker_id = $_GET['block'];
                $query = "UPDATE bakers SET account_status = '3', modified_at = CURRENT_TIMESTAMP() WHERE id = {$the_baker_id}";
                $change_status_to_block = mysqli_query($connection, $query);
                header("Location: bakers.php");
            }


            ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
    