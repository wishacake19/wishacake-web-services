<?php
if (isset($_GET['edit_user'])) {
    $the_user_id = $_GET['edit_user'];
    $query = "SELECT * FROM users WHERE id = $the_user_id";
    $select_user = mysqli_query($connection, $query);
    confirmQuery($select_user);
    while ($row = mysqli_fetch_assoc($select_user)) {
        $user_id = $row['id'];
        $user_password = $row['password'];
        $user_firstname = $row['first_name'];
        $user_lastname = $row['last_name'];
        $user_email = $row['email'];
        $user_mobile = $row['mobile_number'];
    }
}
?>
<h4 class="box-title">Update user</h4>
<hr style="background-color: #9b9b9b; height: 1px;">
<form action="" id="update_form" method="post" enctype="multipart/form-data">
    <div hidden id="success" class='alert alert-success alert-dismissible'
         style='background-color: #dff0d8 !important; border-color: #d6e9c6;'>
        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
        <strong style='color: #000000;'>Success!</strong> <span
                style='color: #000000;'>User updated successfully — </span><a href='users.php' class='alert-link'>View
            Users</a>
    </div>
    <div class="form-group">
        <label for="fname">First name</label>
        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $user_id; ?>">
        <input maxlength="35" type="text" class="form-control" id="user_firstname" name="user_firstname"
               value="<?php echo $user_firstname; ?>">
        <span id="firstname" hidden style='color: #FF0000;'>* First name is required</span>
    </div>
    <div class="form-group">
        <label for="lname">Last name</label>
        <input maxlength="35" type="text" class="form-control" id="user_lastname" name="user_lastname"
               value="<?php echo $user_lastname; ?>">
        <span id="lastname" hidden style='color: #FF0000;'>* Last name is required</span>
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input maxlength="255" type="email" class="form-control" id="user_email" name="user_email"
               value="<?php echo $user_email; ?>">
        <span id="email" hidden style='color: #FF0000;'>* Email address is required</span>
        <span id="email_already" hidden style='color: #FF0000;'>* Email address already exists</span>
    </div>
    <div class="form-group">
        <label for="number">Mobile number</label>
        <div class="row">
            <div class="col-xs-1">
                <input type="hidden" class="form-control" id="code" name="code" value="+92">
                <input type="text" class="form-control" value="+92" disabled>
            </div>
            <div class="col-xs-11">
                <input maxlength="10" type="text" class="form-control" id="number" name="number"
                       value="<?php echo substr($user_mobile, 3) ?>">
                <span id="no" hidden style='color: #FF0000;'>* Mobile number is required</span>
                <span id="no_length" hidden style='color: #FF0000;'>* Mobile number must be 10 digits long</span>
                <span id="no_already" hidden style='color: #FF0000;'>* Mobile number already exists</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input maxlength="255" type="password" class="form-control" id="user_password" name="user_password"
               value="<?php echo $user_password; ?>">
        <span id="password" hidden style='color: #FF0000;'>* Password is required</span>
        <span id="password_length" hidden style='color: #FF0000;'>* Password must be at least 8 characters long</span>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" id="update_user" name="update_user" value="Update user">
    </div>
</form>
<script>

    $(document).ready(function () {
        $('#update_form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: "includes/update_users.php",
                type: "GET",
                data: $('#update_form').serialize(),
                dataType: 'json',
                success: function (data) {

                    if (data.firstname === "true") {
                        $("#success").hide();

                        $("#firstname").show();
                    }
                    if (data.lastname === "true") {
                        $("#success").hide();

                        $("#lastname").show();
                    }
                    if (data.email === "true") {
                        $("#success").hide();

                        $("#email").show();
                    }
                    if (data.email_already === "true") {
                        $("#success").hide();

                        $("#email_already").show();
                    }
                    if (data.no === "true") {
                        $("#success").hide();

                        $("#no").show();
                    }
                    if (data.no_length === "true") {
                        $("#success").hide();

                        $("#no_length").show();
                    }
                    if (data.no_already === "true") {
                        $("#success").hide();

                        $("#no_already").show();
                    }
                    if (data.password === "true") {
                        $("#success").hide();

                        $("#password").show();
                    }
                    if (data.password_length === "true") {
                        $("#success").hide();

                        $("#password_length").show();
                    }
                    if (data.message === "true") {
                        $("#firstname").hide();
                        $("#lastname").hide();
                        $("#email").hide();
                        $("#email_already").hide();
                        $("#no").hide();
                        $("#no_length").hide();
                        $("#no_already").hide();
                        $("#password").hide();
                        $("#password_length").hide();

                        $("#success").show();
                    }
                    //alert("Record successfully updated ");
                },
                error: function (ts) {
                    alert(ts.responseText);
                }
            });
        });
    });
</script>