<?php include "includes/db.php"; ?>
<?php include "functions.php"; ?>
<?php ob_start(); ?>
<?php session_start(); ?>
<?php
if (!isset($_SESSION['email'])) {
    header("Location: ../admin/login.php");
}
?>

<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">


            <!-- Notifications Menu -->
            <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning" id="dataCount"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header" id="dataNotification" style="margin-bottom: 6px;"></li>
                    <li>

                        <ul class="menu">
                            </li><!-- start notification -->

                            <div id="user_notification"></div>

                            </li>
                            <!-- end notification -->
                        </ul>
                    </li>
                    <li class="footer" id="view_all"><a style="font-size: 14px;"
                                                        href="bakers.php?source=pending_notified_bakers">View All</a>
                    </li>
                </ul>
            </li>

            <!-- User Account Menu -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-circle"></i>
                    <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a style="color: #333333;" href="../admin/change_password.php"><i class="fa fa-fw fa-lock"></i>
                            Change password</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a style="color: #333333;" href="../admin/logout.php"><i class="fa fa-fw fa-power-off"></i> Log
                            out</a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
<script>
    function reloadDiv() {
        $.ajax({
            url: "includes/select.php",
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $('#dataCount').html(result);
                if (result == '') {
                    $('#dataNotification').html("You have no new baker requests.");
                    $('#dataNotification').css('margin-bottom', '0px');
                    $('#view_all').hide();
                    $('#user_notification').hide();
                } else {
                    $('#dataNotification').html("You have" + " " + result + " " + "new baker request(s).");
                    $('#dataNotification').css('margin-bottom', '6px');
                    $('#view_all').show();
                    $('#user_notification').show();
                }

            }
        });
    }

    setInterval("reloadDiv()", 1000);

    function reloadUser() {
        $.ajax
        ({
            url: 'includes/select_baker.php',
            type: 'GET',
            data: '',
            dataType: "json",

            success: function (data) {

                var html = '', baker = '';
                for (var i = 0; i < data.length; i++) {
                    baker = data[i];
                    url = "bakers.php?source=pending_notified_bakers&baker_id=" + baker.id;
                    html += '<a style="padding-left: 12px; color: #3c8dbc;" href=' + url + '>' + baker.fname + " " + baker.lname + '</a><hr style="margin-top: 6px; margin-bottom: 6px;">';
                }
                $('#user_notification').html(html);
            }
        });
    }

    setInterval("reloadUser()", 1000);

</script>