<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="index.php">wishacake</a></strong> All rights reserved.
</footer>


</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>

<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcSK_dp9iNn7WBaOzLTMbVkFxKmZj_k0E&libraries=places&callback=initMap"
        async defer></script>
<script>

    $(document).ready(function () {
        $(".delete").on("click", function () {
            var id = $(this).attr("rel");
            var delete_url = "featured_cakes.php?delete=" + id + "";
            $(".modal_delete_link").attr("href", delete_url);
            $("#myModal").modal('show');
        });
    });
    $(document).ready(function () {
        $('#example1').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false
        });

    });
    $(":input").inputmask();
    $("#cnic").inputmask({"mask": "99999-9999999-9"});
    // $("#pin").inputmask({"mask": "9999"});
    $(document).ready(function () {

//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
            {
                value: <?php echo totalOrders(); ?>,
                color: '#34b7f1',
                highlight: '#34b7f1',
                label: 'Total'
            },
            {
                value: <?php echo orderStatusCount(1); ?>,
                color: '#f8cc07',
                highlight: '#f8cc07',
                label: 'New / Pending'
            },
            {
                value: <?php echo orderStatusCount(2); ?>,
                color: '#4aae20',
                highlight: '#4aae20',
                label: 'Accepted'
            },
            {
                value: <?php echo orderStatusCount(3); ?>,
                color: '#d53732',
                highlight: '#d53732',
                label: 'Rejected'
            },
            {
                value: <?php echo orderStatusCount(4); ?>,
                color: '#f06292',
                highlight: '#f06292',
                label: 'Confirmed'
            },
            {
                value: <?php echo orderStatusCount(5); ?>,
                color: '#797979',
                highlight: '#797979',
                label: 'Cancelled'
            },
            {
                value: <?php echo orderStatusCount(6); ?>,
                color: '#ff9300',
                highlight: '#ff9300',
                label: 'Ongoing'
            },
            {
                value: <?php echo orderStatusCount(7); ?>,
                color: '#0076ba',
                highlight: '#0076ba',
                label: 'Completed'
            }
        ];
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true
        };
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);


    });
    $(document).ready(function () {
        var div_box = "<div id='load-screen'><div id='loading'></div></div>";
        $("body").prepend(div_box);
        $('#load-screen').delay(150).fadeOut(600, function () {
            $(this).remove();
        });
    });
</script>