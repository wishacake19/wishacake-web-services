<?php
function confirmQuery($result)
{
    global $connection;
    if (!$result) {
        die("Query Failed." . mysqli_error($connection));
    }
}

function usermail_exists($email)
{
    global $connection;
    $query = "SELECT email from users WHERE email = '$email'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function userphone_exists($phone)
{
    global $connection;
    $query = "SELECT mobile_number from users WHERE mobile_number = '$phone'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function usermail_exists_update($email, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT email from users WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($email == $row['email']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT email from users WHERE email = '$email'";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function userphone_exists_update($phone, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT mobile_number from users WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($phone == $row['mobile_number']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT mobile_number from users WHERE mobile_number = '$phone'";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function bakermail_exists($email)
{
    global $connection;
    $query = "SELECT email from bakers WHERE email = '$email'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function bakerphone_exists($phone)
{
    global $connection;
    $query = "SELECT mobile_number from bakers WHERE mobile_number = '$phone'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function bakercnic_exists($cnic)
{
    global $connection;
    $query = "SELECT cnic from bakers WHERE cnic = '$cnic'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function bakermail_exists_update($email, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT email from bakers WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($email == $row['email']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT email from bakers WHERE email = '$email'";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function bakerphone_exists_update($phone, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT mobile_number from bakers WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($phone == $row['mobile_number']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT mobile_number from bakers WHERE mobile_number = '$phone'";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function bakercnic_exists_update($cnic, $id)
{
    global $connection;
    $check = true;
    $query = "SELECT cnic from bakers WHERE id = $id";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_assoc($result)) {
        if ($cnic == $row['cnic']) {
            $check = false;
        } else {
            $check = true;
        }
    }
    if ($check == true) {
        $query = "SELECT cnic from bakers WHERE cnic = '$cnic'";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        if (mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }
    return $check;
}

function redirect($location)
{
    header("Location: " . $location);
    exit;
}

function ifItIsMethod($method = null)
{
    if ($_SERVER['REQUEST_METHOD'] == strtoupper($method)) {
        return true;
    }
    return false;
}

function isLoggedIn()
{
    if (isset($_SESSION['email'])) {
        return true;
    }
    return false;
}

function checkIfUserIsLoggedInAndRedirect($redirectLocation = null)
{
    if (isLoggedIn()) {
        redirect($redirectLocation);
    }
}

function login_user($email, $password)
{

    global $connection;

    $query = "SELECT * FROM admin WHERE email = '{$email}'";
    $select_user_query = mysqli_query($connection, $query);
    if (!$select_user_query) {
        die("Query Failed " . mysqli_error($connection));
    }
    while ($row = mysqli_fetch_assoc($select_user_query)) {
        $db_user_email = $row['email'];
        $db_user_password = $row['password'];
        if ($password == $db_user_password) {
            $_SESSION['email'] = $db_user_email;
            redirect("/wishacake/admin/index.php");
        } else {
            return false;
        }
    }
    return false;
}

function checkforoldpassword($password)
{
    global $connection;
    $query = "SELECT password FROM admin WHERE password = '$password'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    if (mysqli_num_rows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function totalOrders()
{
    global $connection;
    $query = "SELECT * FROM orders";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    confirmQuery($result);
    return $result;
}

function totalCompanyEarning()
{
    global $connection;
    $query = "SELECT SUM(company_earning) AS total_company_earning FROM orders";
    $result = mysqli_query($connection, $query);
    $row = mysqli_fetch_assoc($result);
    $sum = $row['total_company_earning'];
    return $sum;
}

function orderStatusCount($status_code)
{
    global $connection;
    $query = "SELECT * FROM orders WHERE status = $status_code";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

function totalBakerOrders($baker_id)
{
    global $connection;
    $query = "SELECT * FROM orders WHERE baker_id = $baker_id";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    confirmQuery($result);
    return $result;
}

function totalBakerEarning($baker_id)
{
    global $connection;
    $query = "SELECT SUM(baker_earning) AS total_baker_earning FROM orders WHERE baker_id = $baker_id";
    $result = mysqli_query($connection, $query);
    $row = mysqli_fetch_assoc($result);
    $sum = $row['total_baker_earning'];
    return $sum;
}

function orderBakerStatusCount($baker_id, $status_code)
{
    global $connection;
    $query = "SELECT * FROM orders WHERE status = $status_code AND baker_id = $baker_id";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

function totalUserOrders($user_id)
{
    global $connection;
    $query = "SELECT * FROM orders WHERE user_id = $user_id";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    confirmQuery($result);
    return $result;
}

function orderUserStatusCount($user_id, $status_code)
{
    global $connection;
    $query = "SELECT * FROM orders WHERE status = $status_code AND user_id = $user_id";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

function totalUsers()
{
    global $connection;
    $query = "SELECT * FROM users";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    confirmQuery($result);
    return $result;
}

function totalActiveUsers()
{
    global $connection;
    $query = "SELECT * FROM users WHERE account_status=1";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

function totalBlockedUsers()
{
    global $connection;
    $query = "SELECT * FROM users WHERE account_status=0";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

function totalBakers()
{
    global $connection;
    $query = "SELECT * FROM bakers";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    confirmQuery($result);
    return $result;
}

function bakerAccountStatus($status)
{
    global $connection;
    $query = "SELECT * FROM bakers WHERE account_status=$status";
    $select = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select);
    if ($result > 0) {
        return $result;
    } else {
        return 0;
    }
    confirmQuery($result);
}

?>