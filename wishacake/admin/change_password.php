<?php include "includes/header.php"; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <img class="logo-mini" src="dist/img/ic_launcher.png"
                 style="height: 50px; width: 50px; padding: 2px 2px 2px 2px;">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>wishacake</b></span>
        </a>

        <!-- Header Navbar -->
        <?php include "includes/navigation.php"; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include "includes/sidenav.php"; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Change password
            </h1>
            <br>
        </section>
        <?php

        if (isset($_POST['submit'])) {
            $oldpassword = $_POST['old_password'];
            $newpassword = $_POST['new_password'];
            $confirmpassword = $_POST['confirm_password'];

            $error = [
                'oldpassword' => '',
                'newpassword' => '',
                'confirmpassword' => ''
            ];
            if ($oldpassword == "") {
                $error['oldpassword'] = "<span style='color: #FF0000;'>* Old password is required</span>";
            } else if (checkforoldpassword($oldpassword) == false) {
                $error['oldpassword'] = "<span style='color: #FF0000;'>* Old password is incorrect</span>";
            }
            if ($newpassword == "") {
                $error['newpassword'] = "<span style='color: #FF0000;'>* New password is required</span>";
            } else if (strlen($newpassword) < 5) {
                $error['newpassword'] = "<span style='color: #FF0000;'>* New password must be greater or equal to 5 characters</span>";
            }
            // if(strlen($confirmpassword) < 5){
            //     $error['confirmpassword'] = "<span style='color: #FF0000;'>* Confirm password must be greater or equal to 5 characters</span>";
            // }
            if ($confirmpassword == "") {
                $error['confirmpassword'] = "<span style='color: #FF0000;'>* Confirm password is required</span>";
            } else if ($confirmpassword != $newpassword) {
                $error['confirmpassword'] = "<span style='color: #FF0000;'>* Confirm password does not match new password</span>";
            }
            if ($oldpassword != "" && checkforoldpassword($oldpassword) == true && $newpassword != "" && $confirmpassword != "" && $confirmpassword == $newpassword && strlen($newpassword) >= 5) {
                // else{
                $query = "UPDATE admin SET password = '{$newpassword}' WHERE email = '{$_SESSION['email']}'";
                $update_admin_query = mysqli_query($connection, $query);
                confirmQuery($update_admin_query);
                echo "<h4 style='color: #33cc33; padding-left: 15px;'>Admin password has been updated successfully</h4>";
                // }
            }

        }

        ?>
        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="col-lg-12">

                    <form action="" method="post" enctype="multipart/form-data">
                        <?php echo isset($error['message']) ? $error['message'] : '' ?>

                        <div class="form-group">
                            <label for="password">Old password</label>
                            <input maxlength="255" type="password" class="form-control" name="old_password"
                                   value="<?php echo isset($_POST['old_password']) ? $_POST['old_password'] : '' ?>">
                            <?php echo isset($error['oldpassword']) ? $error['oldpassword'] : '' ?>
                        </div>
                        <div class="form-group">
                            <label for="password">New password</label>
                            <input maxlength="255" type="password" class="form-control" name="new_password"
                                   value="<?php echo isset($_POST['new_password']) ? $_POST['new_password'] : '' ?>">
                            <?php echo isset($error['newpassword']) ? $error['newpassword'] : '' ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Confirm password</label>
                            <input maxlength="255" type="password" class="form-control" name="confirm_password"
                                   value="<?php echo isset($_POST['confirm_password']) ? $_POST['confirm_password'] : '' ?>">
                            <?php echo isset($error['confirmpassword']) ? $error['confirmpassword'] : '' ?>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                        </div>
                    </form>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. -->
</body>
</html>