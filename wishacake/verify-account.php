<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verify account | wishacake</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        body {
            margin: 0 auto;
            max-width: 1024px;
            padding: 2em 2em 4em;
        }
    </style>
</head>

<body>
<?php
if (isset($_GET['t'])) {

    // Get value of token from URL param
    $token = $_GET['t'];

    // Establish DB connection
    require_once dirname(__FILE__) . '/includes/db-connect.php';
    $db = new DbConnect();
    $con = $db->connect();

    // Get user auto ID from token
    include_once("./includes/user.php");
    $user = new User();
    $id = $user->getUser("email_token", $token, 'id');

    $emailVerifiedStatus = $user->getUser("id", $id, 'email_verified_status');

    if ($emailVerifiedStatus == '0') {
        // Update verification status in DB
        $sql = "UPDATE users SET email_verified_status = '1', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
        if ($con->query($sql)) {
            echo "<html><body><div class='alert alert-success'><strong>Success!</strong> Your account has been verified.</div></html></body>";
        } else {
            echo "<html><body><div class='alert alert-danger'><strong>Error!</strong> Sorry, something went wrong. Please try again.</div></html></body>";
        }
    } else if ($emailVerifiedStatus == '1') {
        echo "<html><body><div class='alert alert-info'><strong>Info!</strong> Your account has already been verified.</div></html></body>";
    } else {
        echo "<html><body><div class='alert alert-danger'><strong>Error!</strong> Oops! Sorry, something went wrong. Please request a new verification email.</div></html></body>";
    }
} else {
    echo "<html><body><div class='alert alert-danger'><strong>404!</strong> Oops! Sorry, something went wrong. Please try again.</div></html></body>";
}
?>
</body>

</html>