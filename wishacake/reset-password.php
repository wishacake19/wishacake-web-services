<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>wishacake | Reset password</title>

    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        body {
            margin: 0 auto;
            max-width: 1024px;
            padding: 2em 2em 4em;
        }
    </style>
</head>

<body>
<h1 style="text-align: center;">Reset your password</h1>
<br/>
<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "wishacake";

foreach ($db as $key => $value) {
    define(strtoupper($key), $value);
}
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$connection) {
    die("Database connection Failed" . mysqli_error($connection));
}
if (isset($_GET['t'])) {
    $password_token = $_GET['t'];
    if (isset($_POST['submit'])) {
        $newpassword = $_POST['new_pass'];
        $confirmpassword = $_POST['confirm_pass'];

        $error = [
            'newpassword' => '',
            'confirmpassword' => ''
        ];
        if ($newpassword == "") {
            $error['newpassword'] = "<span style='color: #FF0000;'>* New password is required</span>";
        } else if (strlen($newpassword) < 8) {
            $error['newpassword'] = "<span style='color: #FF0000;'>* New password must be at least 8 characters long</span>";
        }

        if ($confirmpassword == "") {
            $error['confirmpassword'] = "<span style='color: #FF0000;'>* Confirm password is required</span>";
        } else if ($confirmpassword != $newpassword) {
            $error['confirmpassword'] = "<span style='color: #FF0000;'>* Confirm password does not match new password</span>";
        }
        if ($newpassword != "" && $confirmpassword != "" && $confirmpassword == $newpassword && strlen($newpassword) >= 8) {
            $query = "UPDATE users SET password = '{$confirmpassword}', password_token=null WHERE password_token = '{$password_token}'";
            $update_admin_query = mysqli_query($connection, $query);
            echo "<div class='alert alert-success'><strong>Success!</strong> Password updated successfully</div>";
        }
    }
}

?>
<form action="" method="post">
    <div class="form-group">
        <div class="input-group">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                </span>
            <input type="password" placeholder="Type new password" name="new_pass" maxlength="255"
                   class="form-control"/>

        </div>
        <?php echo isset($error['newpassword']) ? $error['newpassword'] : '' ?>
    </div>
    <div class="form-group">
        <div class="input-group">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                </span>
            <input type="password" placeholder="Re-type new password" name="confirm_pass" maxlength="255"
                   class="form-control"/>

        </div>
        <?php echo isset($error['confirmpassword']) ? $error['confirmpassword'] : '' ?>
    </div>
    <input type="submit" name="submit" class="btn btn-success" value="Reset password">
</form>
</body>

</html>