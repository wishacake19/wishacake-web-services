<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>wishacake | Reset PIN</title>

    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        body {
            margin: 0 auto;
            max-width: 1024px;
            padding: 2em 2em 4em;
        }
    </style>
</head>

<body>
<h1 style="text-align: center;">Reset your PIN</h1>
<br/>
<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "";
$db['db_name'] = "wishacake";

foreach ($db as $key => $value) {
    define(strtoupper($key), $value);
}
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$connection) {
    die("Database connection Failed" . mysqli_error($connection));
}
if (isset($_GET['t'])) {
    $token_pin = $_GET['t'];
    if (isset($_POST['submit'])) {
        $newpin = $_POST['new_pin'];
        $confirmpin = $_POST['confirm_pin'];

        $error = [
            'newpin' => '',
            'confirmpin' => ''
        ];
        if ($newpin == "") {
            $error['newpin'] = "<span style='color: #FF0000;'>* New PIN is required</span>";
        } else if (strlen($newpin) != 4) {
            $error['newpin'] = "<span style='color: #FF0000;'>* New PIN must be equal to 4</span>";
        }

        if ($confirmpin == "") {
            $error['confirmpin'] = "<span style='color: #FF0000;'>* Confirm PIN is required</span>";
        } else if ($confirmpin != $newpin) {
            $error['confirmpin'] = "<span style='color: #FF0000;'>* Confirm PIN does not match new PIN</span>";
        }
        if ($newpin != "" && $confirmpin != "" && $confirmpin == $newpin && strlen($newpin) == 4) {
            $query = "UPDATE bakers SET pin = '{$confirmpin}', pin_token=null WHERE pin_token = '{$token_pin}'";
            $update_admin_query = mysqli_query($connection, $query);
            echo "<div class='alert alert-success'><strong>Success!</strong> PIN updated successfully</div>";
        }
    }
}
?>
<form action="" method="post">
    <div class="form-group">
        <div class="input-group">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                </span>
            <input type="password" placeholder="Type new PIN" name="new_pin" maxlength="4" class="form-control"/>
        </div>
        <?php echo isset($error['newpin']) ? $error['newpin'] : '' ?>
    </div>
    <div class="form-group">
        <div class="input-group">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                </span>
            <input type="password" placeholder="Re-type new PIN" name="confirm_pin" maxlength="4" class="form-control"/>
        </div>
        <?php echo isset($error['confirmpin']) ? $error['confirmpin'] : '' ?>
    </div>
    <button type="submit" name="submit" class="btn btn-success">Reset PIN</button>
</form>
</body>

</html>